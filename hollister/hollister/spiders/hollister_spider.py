from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from hollister.items import HollisterItem
from cStringIO import StringIO
from scrapy.exceptions import DropItem
import requests

class HollisterSpider(CrawlSpider):
	name = "hollister"
	allowed_domains = ["hollisterco.com"]
	start_urls = [
		"http://www.hollisterco.com/shop/wd/dudes-shirts",
		"http://www.hollisterco.com/shop/wd/dudes-hoodies-and-sweatshirts",
		"http://www.hollisterco.com/shop/wd/dudes-sweaters",
		"http://www.hollisterco.com/shop/wd/dudes-graphic-t-shirts",
		# "http://www.hollisterco.com/webapp/wcs/stores/servlet/CategoryDisplay?storeId=11205&urlRequestType=Base&topCategoryId=12551&categoryId=16398&parentCategoryId=16398&langId=-1&catalogId=10201",
		"http://www.hollisterco.com/shop/wd/dudes-short-sleeve-t-shirts",
		"http://www.hollisterco.com/shop/wd/dudes-long-sleeve-t-shirts",
		"http://www.hollisterco.com/shop/wd/dudes-tanks-t-shirts",
		"http://www.hollisterco.com/shop/wd/dudes-polos",
		"http://www.hollisterco.com/shop/wd/dudes-outerwear",
		"http://www.hollisterco.com/shop/wd/dudes-jeans",
		"http://www.hollisterco.com/shop/wd/dudes-sweatpants",
		"http://www.hollisterco.com/shop/wd/dudes-pants",
		"http://www.hollisterco.com/shop/wd/dudes-shorts",
		"http://www.hollisterco.com/shop/wd/dudes-hollister-sport",
		"http://www.hollisterco.com/shop/wd/dudes-swim",
		"http://www.hollisterco.com/shop/wd/dudes-flip-flops",
		"http://www.hollisterco.com/shop/wd/dudes-accessories",
		"http://www.hollisterco.com/shop/wd/dudes-underwear",
		"http://www.hollisterco.com/shop/wd/bettys-sweaters",
		"http://www.hollisterco.com/shop/wd/bettys-hoodies-and-sweatshirts",
		"http://www.hollisterco.com/shop/wd/bettys-shirts",
		"http://www.hollisterco.com/shop/wd/bettys-fashion-tops",
		"http://www.hollisterco.com/shop/wd/bettys-graphic-t-shirts",
		"http://www.hollisterco.com/shop/wd/bettys-polos",
		"http://www.hollisterco.com/shop/wd/bettys-outerwear",
		"http://www.hollisterco.com/shop/wd/bettys-jeans",
		"http://www.hollisterco.com/shop/wd/bettys-sweatpants",
		"http://www.hollisterco.com/shop/wd/bettys-leggings",
		"http://www.hollisterco.com/shop/wd/bettys-pants",
		"http://www.hollisterco.com/shop/wd/bettys-shorts",
		"http://www.hollisterco.com/shop/wd/bettys-skirts",
		"http://www.hollisterco.com/shop/wd/bettys-dresses-and-rompers",
		"http://www.hollisterco.com/shop/wd/bettys-hollister-sport",
		"http://www.hollisterco.com/shop/wd/bettys-swim",
		"http://www.hollisterco.com/shop/wd/bettys-footwear",
		"http://www.hollisterco.com/shop/wd/bettys-accessories",
		"http://www.hollisterco.com/shop/wd/bettys-sleep",
		"http://www.hollisterco.com/shop/wd/bettys-bras-and-undies",

	]

	
	# allow_list = [
	# 	"shop/wd/guys-(.*)",
	# 	"shop/wd/girls-(.*)"
	# ]

	# deny_list = [
	# 	"shop/wd/(.*)/CustomerService(.*)",
	# 	".*?editItem=true",
	# 	"shop/wd/p/(.*)",

	# 	"shop/wd/guys-a-and-f-looks",
	# 	"shop/wd/guys-new-arrivals",
	# 	"shop/wd/guys-the-coolest-blues",
	# 	"shop/wd/guys-spring-break-layers",
	# 	"shop/wd/guys-casual-cool",
	# 	"shop/wd/guys-what-we-love"
	# 	"shop/wd/guys-body-care",
	# 	"shop/wd/guys-cologne",
	# 	"shop/wd/hidden-gift-cards",
	# 	"shop/wd/guys-featured-items-clearance",


	# 	"shop/wd/womens-fragrance",
	# 	"shop/wd/womens-a-and-f-looks",
	# 	"shop/wd/womens-new-arrivals",
	# 	"shop/wd/womens-a-and-f-the-making-of-a-star",
	# 	"shop/wd/womens-denim-collection",
	# 	"shop/wd/womens-soft-and-pretty",
	# 	"shop/wd/womens-effortless-90s-style",
	# 	"shop/wd/womens-the-prettiest-lace",
	# 	"shop/wd/womens-essential-spring-layers",
	# 	"shop/wd/womens-flagship-exclusives",
	# 	"shop/wd/womens-online-exclusives",
	# 	"shop/wd/womens-what-we-love",
	# 	"shop/wd/womens-featured-items-sale(.*)",
	# 	"shop/wd/hidden-gift-cards",
	# 	"shop/wd/mens-body-care",
	# 	"shop/wd/mens-cologne",
	# 	"shop/wd/mens-a-and-f-looks",
	# 	"shop/wd/mens-new-arrivals",
	# 	"shop/wd/mens-a-and-f-the-making-of-a-star",
	# 	"shop/wd/mens-the-hottest-blues",
	# 	"shop/wd/mens-vintage-texture-and-wash",
	# 	"shop/wd/mens-spring-break-layers",
	# 	"shop/wd/mens-casual-cool",
	# 	"shop/wd/mens-flagship-exclusives",
	# 	"shop/wd/mens-online-exclusives",
	# 	"shop/wd/mens-get-fierce",
	# 	"shop/wd/mens-what-we-love",
	# 	"shop/wd/mens-featured-items-sale(.*)",

	# 	"shop/wd/womens-featured-items-clearance(.*)",
	# 	"shop/wd/mens-featured-items-clearance(.*)"
	# ]
	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="image-wrap"]', '//a[@class="swatch-link"]'), ), callback="parse_item", follow=True, ),
  ]

	def parse_test(self, response):
		sel = Selector(response)
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()

	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="name"]/text()').extract()
		desc = sel.xpath('//h2[@class="copy"]/text()').extract()
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		befor_price = sel.xpath('//div[@class="list-price redline"]/del/text()').extract()
		if not price:
			return

		options = sel.xpath('//li[@class="size select required"]/div[@class="dk_container dk_theme_medium_gray"]/div[@class="dk_options"]/ul[@class="dk_options_inner"]/li[not(@class="dk_option_current")]/a/text()').extract()
		sub_title = sel.xpath('//div[@class="swatches hidden"]/ul/li[@class="color"]/text()').extract()
		seed_image = sel.xpath('//span[@class="zoom prod-img"]/img[@class="prod-img"]/@src').extract()[0]
		breadcrumb = sel.xpath('//div[@id="breadcrumb"]/a/text()').extract()

		prod_state = 200
		model_state = 200
		prod_num = 1
		model_num = 1
		images = []

		

		while prod_state == 200:
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("1?")[0])
			url_str.write(str(prod_num))
			url_str.write("?$anfProductImageMagnify$")

			r = requests.get(url_str.getvalue())
			prod_state = r.status_code
			prod_num = prod_num + 1
			if prod_state == 200:
				images.append(url_str.getvalue())

		while model_state == 200:
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("prod1?")[0])
			url_str.write("model")
			url_str.write(str(model_num))
			url_str.write("?$anfProductImageModMagnify$")

			r = requests.get(url_str.getvalue())
			model_state = r.status_code
			model_num = model_num + 1
			if model_state == 200:
				images.append(url_str.getvalue())


		item = HollisterItem()

		item['title'] = title[0] if title else ""
		item['sub_title'] = sub_title[0] if sub_title else ""
		item['url'] = response.url 
		item['domain'] = "hollisterco.com"
		item['desc'] = desc[0] if desc else ""
		item['currency'] = price[0][0]
		item['price'] = price[0][1:]
		item['options'] = map(lambda option: option.strip(), options)
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "hollister"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb)
		item['created_by'] = 16
		item['county'] = "US"


		return item

	def get_category(self, breadcrumb):
		return "20"

		

