# Scrapy settings for hollister project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'hollister'

SPIDER_MODULES = ['hollister.spiders']
NEWSPIDER_MODULE = 'hollister.spiders'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True
DOWNLOAD_DELAY = 1



DOWNLOADER_MIDDLEWARES = {
	'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
	'hollister.middlewares.UrlModifyMiddleware': 100,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'hollister (+http://www.yourdomain.com)'
