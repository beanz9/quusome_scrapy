from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from elietahari.items import ElietahariItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json

class ElietahariSpider(CrawlSpider):
	name = "elietahari"
	allow_domains = ["elietahari.com"]
	start_urls = [
		"http://www.elietahari.com/en_US/women/shop/dresses?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/blouses?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/suits?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/sweaters-knits?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/skirts?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/pants-shorts?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/denim?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/jackets?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/outerwear?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/accessories?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/handbags?viewall=true",
		"http://www.elietahari.com/en_US/women/shop/sunglasses?viewall=true",

		"http://www.elietahari.com/en_US/shoes/shop/flats?viewall=true",
		"http://www.elietahari.com/en_US/shoes/shop/heels?viewall=true",
		"http://www.elietahari.com/en_US/shoes/shop/sandals?viewall=true",
		"http://www.elietahari.com/en_US/shoes/shop/wedges?viewall=true",
		"http://www.elietahari.com/en_US/shoes/shop/boots?viewall=true",

		"http://www.elietahari.com/en_US/men/shop/shirts?viewall=true",
		"http://www.elietahari.com/en_US/men/shop/knits-sweaters?viewall=true",
		"http://www.elietahari.com/en_US/men/shop/pants-jeans?viewall=true",
		"http://www.elietahari.com/en_US/men/shop/jackets-outerwear?viewall=true"
		# "http://www.elietahari.com/en_US/spark-pump/144170.html?PathToProduct=shoes_shop_heels&cgid=shoes_shop_heels&dwvar_144170_color=001&start=11"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//p[@class="productimage"]/a')), callback="parse_item" ),
		# Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="pagination"]/ul[not(contains(li, "view-all"))]/a')), follow=True ),
  	
  ]

	def parse_item(self, response):
		sel = Selector(response)

		title = sel.xpath('//title/text()').extract()[0].split('|')[0].strip()
		color = sel.xpath('//a[@class="swatchanchor colorRef"]/text()').extract()
		color2 = sel.xpath('//a[@class="swatchanchor colorRef nostock"]/text()').extract()
		color = color + color2
		color_code = sel.xpath('//a[@class="swatchanchor colorRef"]/@rel').extract()
		color_code2 = sel.xpath('//a[@class="swatchanchor colorRef nostock"]/@rel').extract()
		color_code = color_code + color_code2

		size = sel.xpath('//a[@class="swatchanchor"]/text()').extract()
		desc = sel.xpath('//p[@class="longdesc"]/text()').extract()[0].strip()
		desc2 = sel.xpath('//div[@data-tab="details"]/node()').extract()

		sizeTable_text = requests.get('http://www.elietahari.com/on/demandware.store/Sites-elietahari_us-Site/en_US/Page-Include?cid=size-chart-women').text
		size_table_html = Selector(text=sizeTable_text).xpath('//table[@data-tab="in"]/node()').extract()
		breadcrumb = sel.xpath('//div[@class="breadcrumb"]/a/text()').extract()



		desc = desc + ("".join(desc2))

		pid = sel.xpath('//p[@class="stylenumber"]/text()').extract()[0].split(':')[1].strip()

		variants_url = "http://www.elietahari.com/on/demandware.store/Sites-elietahari_us-Site/en_US/Product-GetVariants?pid=" + pid

		json_obj = json.loads(requests.get(variants_url).text , strict=False)

		options = {}

		options["size"] = size
		options["color"] = color

		option_inventory = {}
		price = {}
		price['sale_color'] = {}

		is_sale = "false"

		images = {}


		xml_url = 'http://cdn.fluidretail.net/customers/c1444/' + pid + '/' + pid + '_PDP/pview_' + pid + '_PDP.xml'
		xml = requests.get(xml_url).text
		xml_sel = Selector(text=xml)



		variation_ids = xml_sel.xpath('//category_group[@id="VARIATION"]/category/@id').extract()
		view_ids = xml_sel.xpath('//category_group[@id="VIEW"]/category[not(contains(@inventory_id, "inventory_id"))]/@id').extract()

	

		# for variation in xml_sel.xpath('//category_group[@id="VARIATION"]').extract():
		# 	variation_obj = Selector(text=variation)
		# 	variation_ids = variation_obj.xpath('//category/@id').extract()

		# 	for view in xml_sel.xpath('//category_group[@id="VIEW"]').extract():
		# 		view_obj = Selector(text=view)
		# 		view_ids = view_obj.xpath('//category/@id').extract()

		temp_image = []

		for variation_id in variation_ids:
			for view_id in view_ids:
				for x in xml_sel.xpath('//image').extract():
					x_obj = Selector(text=x)
					i_variantion_id = x_obj.xpath('//category_mapping[@group_id="VARIATION"]/@id').extract()[0]
					i_view_id =  x_obj.xpath('//category_mapping[@group_id="VIEW"]/@id').extract()[0]
					image_type = x_obj.xpath('//category_mapping[@group_id="IMAGETYPE"]/@id').extract()[0]
					
					if image_type == 'ZOOM' and i_variantion_id == variation_id and i_view_id == view_id:
						temp_image.append('http://cdn.fluidretail.net/' + x_obj.xpath('//image/@url').extract()[0].replace('../../../', ''))


		# temp_image = []

		# for x in xml_sel.xpath('//image').extract():
		# 	x_obj = Selector(text=x)
		# 	image_type = x_obj.xpath('//category_mapping[@group_id="IMAGETYPE"]/@id').extract()[0]
		# 	if image_type == 'ZOOM':
		# 		temp_image.append(x_obj.xpath('//image/@url').extract()[0].replace('../../../', ''))



		for variant in json_obj["variations"]["variants"]:
			color = variant["attributes"]["color"]
			size = variant["attributes"]["size"]
			if variant["inStock"]:
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "available"
			else:
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "unavailable"

			standard_price = variant["pricing"]["standard"]
			sale_price = variant["pricing"]["sale"]

			if standard_price != sale_price:
				is_sale = "true"
				price['sale_color'][color] = variant["pricing"]["sale"]
			
			price['original'] = variant["pricing"]["standard"]
			code = variant["fluidAttributeValues"]["color"]

			image_arr = []

			for t in temp_image:
				# print temp_image
				
				if ("_" + code + "_") in t:
					if "_RA" in t:
						image_arr.append(t)
				
			images[color] = image_arr



		item = ElietahariItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "elietahari.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "ELIE TAHARI"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item


	def get_category(self, breadcrumb):
		if 'women' in breadcrumb:
			return self.get_women_category(breadcrumb)
		elif 'men' in breadcrumb:
			return self.get_men_category(breadcrumb)
		elif 'shoes' in breadcrumb:
			return self.get_shoes_category(breadcrumb)

	def get_women_category(self, breadcrumb):
		print breadcrumb
		if 'dresses' in breadcrumb:
			return 'women_dresses'
		elif 'blouses' in breadcrumb:
			return 'women_tops'
		elif 'suits' in breadcrumb:
			return 'women_suits'
		elif 'sweaters & knits' in breadcrumb:
			return 'women_sweaters_knits'
		elif 'skirts' in breadcrumb:
			return 'women_skirts'
		elif 'pants & shorts' in breadcrumb:
			return 'women_pants'
		elif 'denim' in breadcrumb:
			return 'women_jeans'
		elif 'jackets' in breadcrumb:
			return 'women_jackets'
		elif 'outerwear' in breadcrumb:
			return 'women_coats'
		
		elif 'accessories' in breadcrumb:
			return 'women_accessories'
		elif 'handbags' in breadcrumb:
			return 'women_bags_handbags'
		elif 'Sunglasses' in breadcrumb:
			return 'women_accessories_eyewear'

	def get_men_category(self, breadcrumb):
		if 'shirts' in breadcrumb:
			return 'men_clothing_tops'
		elif 'knits & sweaters' in breadcrumb:
			return 'men_clothing_sweaters'
		elif 'pants & jeans' in breadcrumb:
			return 'men_clothing_pants'
		elif 'jackets & outerwear' in breadcrumb:
			return 'men_clothing_outerwears'

	def get_shoes_category(self, breadcrumb):
		if 'flats' in breadcrumb:
			return 'women_flats'
		elif 'heels' in breadcrumb:
			return 'women_pumps'
		elif 'sandals' in breadcrumb:
			return 'women_sandals'
		elif 'wedges' in breadcrumb:
			return 'women_shoes_wedges'
		elif 'boots' in breadcrumb:
			return 'women_boots_booties'









		