from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from ragbone.items import RagboneItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json

class RagboneSpider(CrawlSpider):
	name = "ragbone"
	allow_domains = ["rag-bone.com"]
	start_urls = [
		# "http://www.rag-bone.com/vincent-jacket/d/6800C2575"
		# "http://www.rag-bone.com/thrasher-tank/d/6362C15945"
		# "http://www.rag-bone.com/womens-tops-shirts/l/183"

		"http://www.rag-bone.com/womens-accessories/l/200",
		"http://www.rag-bone.com/womens-dresses/l/181",
		"http://www.rag-bone.com/womens-jackets-blazers-coats/l/182",
		"http://www.rag-bone.com/womens-jean/l/299",
		"http://www.rag-bone.com/womens-pants/l/184",
		"http://www.rag-bone.com/womens-boots/l/205",
		"http://www.rag-bone.com/womens-skirts-shorts/l/185",
		"http://www.rag-bone.com/womens-sweaters/l/186",
		"http://www.rag-bone.com/womens-tanks-tees/l/187",
		"http://www.rag-bone.com/womens-tops-shirts/l/183",

		"http://www.rag-bone.com/mens-jackets-coats/l/732",
		"http://www.rag-bone.com/mens-denim/l/761",
		"http://www.rag-bone.com/mens-shirts/l/731",
		"http://www.rag-bone.com/mens-sweaters/l/734",
		"http://www.rag-bone.com/mens-tees/l/736",
		"http://www.rag-bone.com/mens-pants-shorts/l/735",
		"http://www.rag-bone.com/mens-shoes/l/801",
		"http://www.rag-bone.com/mens-accessories/l/740"
	]


	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="product-link"]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="btn pager-load-more"]')), follow=True ),
  	
  ]

	def parse_item(self, response):
		sel = Selector(response)
		
		desc = sel.xpath('//p[@class="lead"]').extract()[0].replace('<p class="lead">', '').replace('</p>', '')
		# breadcrumb = sel.xpath('//div[@id="breadcrumbs"]/a/text()').extract()
		data = sel.xpath('//html/@data-page').extract()[0]

		json_obj = json.loads(data , strict=False)

		title = json_obj["product"]["ModelName"]

		is_sale = 'false'

		options = {}
		option_inventory = {}
		images = {}

		price = {}
		price['sale_color'] = {}

		base_price = json_obj["product"]['MaxRegularPrice']
		sale_price = json_obj["product"]['MaxSalePrice']
		price['original'] = str(base_price)

		color_arr = []
		for color in json_obj["product"]["ProductColors"]:
			color_arr.append(color["ColorName"])
			if is_sale == 'true':
				price['sale_color'][color["ColorName"]] = str(sale_price)
			images[color["ColorName"]] = []
			for media in json_obj["product"]["Media"]:
				for image in media["Items"]:
					images[color["ColorName"]].append("http:" + image["ImageThumbUrl"].replace('t.jpg', 'z.jpg'))

		size_arr = []
		for size in json_obj["product"]["ProductSizes"]:
			size_arr.append(size["SizeName"])

		options['color'] = color_arr
		options['size'] = size_arr

		if json_obj["product"]["IsOnSale"]:
			is_sale = 'true'

		# image_arr = []
		# for media in json_obj["product"]["Media"]:
		# 	for image in media["Items"]:
		# 		image_arr.append(image["ImageThumbUrl"].replace('t.jpg', 'z.jpg'))


		for color in json_obj["product"]["ProductColors"]:
			for size in json_obj["product"]["ProductSizes"]:
				if size["StockLevel"] == 0:
					option_inventory["color*_*" + color["ColorName"] + "-_-size*_*" + size["SizeName"].replace('.', '||')] = "unavailable"
				else:
					option_inventory["color*_*" + color["ColorName"] + "-_-size*_*" + size["SizeName"].replace('.', '||')] = "available"



		
		category_name = json_obj["product"]["PrimaryCategoryName"]

		category = self.get_category(category_name, title)
		
		if not category:
			return
	
		item = RagboneItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "rag-bone.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "rag & bone"
		item['breadcrumb'] = ""
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = ""
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

	def get_category(self, category_name, title):
		if "Women's" in category_name:
			return self.get_women_category(category_name, title)
		elif "Men's" in category_name:
			return self.get_men_category(category_name, title)
	
	def get_women_category(self, category_name, title):
		if 'Sweaters' in category_name:
			return 'women_sweaters_knits'
		elif 'Handbags' in category_name:
			return 'women_bags_handbags'
		elif 'Hats & Scarves' in category_name:
			if 'Fedora' in title or 'Hat' in title:
				return 'women_accessories_hats' 
			elif 'Scarf' in title:
				return 'women_accessories_scarves'
			elif 'Sunglasses' in title:
				return 'women_accessories_eyewear'
		elif 'Dresses' in category_name:
			return 'women_dresses'
		elif 'Jackets & Blazers' in category_name:
			return 'women_jackets'
		elif 'JEAN Denim' in category_name:
			return 'women_jeans'
		elif 'Pants' in category_name:
			return 'women_pants'
		elif 'Skirts & Shorts' in category_name:
			return 'women_skirts'
		elif 'Tanks & Tees' in category_name:
			return 'women_tops'
		elif 'Tops & Shirts' in category_name:
			return 'women_tops'
		elif 'Boots' in category_name:
			return 'women_boots_booties'
	
	def get_men_category(self, category_name, title):
		if 'Jackets & Coats' in category_name:
			return 'men_clothing_outerwears'
		elif 'Pants & Shorts' in category_name:
			return 'men_clothing_pants'
		elif 'Shirts' in category_name:
			return 'men_clothing_shirts'
		elif 'Sweaters' in category_name:
			return 'men_clothing_sweaters'
		elif 'Shoes' in category_name:
			return 'men_shoes'
		elif 'Accessories' in category_name:
			return 'men_accessories'
		elif 'Tees' in category_name:
			return 'men_clothing_tops'
		

