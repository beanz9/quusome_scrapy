from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.selector import Selector
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from lulus.items import LulusItem
from cStringIO import StringIO
from scrapy.http import Request
import math


class LulusSpider(CrawlSpider):
	name = "lulus"

	allow_domains = ["lulus.com"]
	start_urls = [


		"http://www.lulus.com/categories/13/dresses.html",
		"http://www.lulus.com/categories/179/shoes.html",
		"http://www.lulus.com/categories/10/tops.html",
		"http://www.lulus.com/categories/889/rompers-jumpsuits.html",
		"http://www.lulus.com/categories/11/bottoms.html",
		"http://www.lulus.com/categories/99/accessories.html",
		"http://www.lulus.com/categories/100/jewelry.html",
		"http://www.lulus.com/categories/865/swimwear.html"

	
		# "http://www.lulus.com/categories/179_42/heels.html",
		# "http://www.lulus.com/categories/179_217/sandals.html"
		# "http://www.lulus.com/categories/179_40/flats.html",
		# "http://www.lulus.com/categories/179_41/boots.html",

		# "http://www.lulus.com/categories/10/tops.html",
		# "http://www.lulus.com/categories/889/rompers-jumpsuits.html",

		# "http://www.lulus.com/categories/11_66/skirts.html",
		# "http://www.lulus.com/categories/11_68/pants.html",
		# "http://www.lulus.com/categories/11_94/shorts.html",

		# "http://www.lulus.com/categories/99_39/handbags-and-purses.html",
		# "http://www.lulus.com/categories/99_133/scarves.html",
		# "http://www.lulus.com/categories/99_54/belts.html",
		# "http://www.lulus.com/categories/99_230/hats.html",

		# "http://www.lulus.com/categories/100_55/necklaces.html",
		# "http://www.lulus.com/categories/100_56/earrings.html",
		# "http://www.lulus.com/categories/100_57/bracelets.html",
		# "http://www.lulus.com/categories/100_73/rings.html",

		# "http://www.lulus.com/categories/100_232/watches.html",		

		# "http://www.lulus.com/categories/865/swimwear.html"



		# "http://www.lulus.com/products/obey-modern-lowback-backless-black-top/133586.html",
		# "http://www.lulus.com/products/ebb-and-flow-off-the-shoulder-ivory-top/135890.html",
		# "http://www.lulus.com/products/tote-couture-blue-tote/115098.html"

		# "http://www.lulus.com/products/clean-chic-white-handbag/146826.html",
		# "http://www.lulus.com/products/lulus-exclusive-glass-half-foliage-navy-blue-floral-print-dress/143618.html",
		# "http://www.lulus.com/products/roxy-fringe-black-bandeau-bikini/147642.html",
		# "http://www.lulus.com/products/kenzie-light-wash-distressed-boyfriend-jeans/133762.html",
		# "http://www.lulus.com/products/newfangled-angle-silver-earrings/150514.html",
		# "http://www.lulus.com/products/dreamboat-come-true-ivory-and-navy-blue-striped-maxi-dress/129498.html"
		# "http://www.lulus.com/products/green-thumb-neon-yellow-maxi-dress/85042.html"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="trap-link"]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//li[@class="next"]')), follow=True )
	]





	def parse_item(self, response):
		sel = Selector(response)

		# sold_out_notification = sel.xpath('//div[@class="sold_out_notification"] and //div[@style="display:none"]').extract()[0]
		sold_out_notification = sel.xpath('//div[@class="no-stock show"]/h3/text()').extract()
		not_available = sel.xpath('//h4[@class="not-available"]/text()').extract()
		# print sold_out_notification

		if sold_out_notification:
			is_soldout = "true"
		else:
			is_soldout = "false"

		if is_soldout == "false":
			if not_available:
				is_soldout = "true"
			else:
				is_soldout = "false"


		title = sel.xpath('//span[@class="product-title"]/text()').extract()[0]
		brand = sel.xpath('//p[@class="brand"]/text()').extract()
		desc = sel.xpath('//div[@class="tab-description open"]/p/text()').extract()[0]
		original_price = sel.xpath('//span[@class="price"]/span/text()').extract()[0][1:]
		sale_price = sel.xpath('//span[@class="sale"]/span/text()').extract()

		old_price = sel.xpath('//span[@class="price"]/span/text()').extract()
		color = sel.xpath('//span[@class="value left"]/text()').extract()[0].strip()
		# breadcrumb = []
		# breadcrumb.append(self.global_url.split('/')[-1].replace('.html', ''))
		

		# breadcrumb_str = ">".join(breadcrumb)

		price = {}
		price['original'] = original_price
		is_sale = "false"


		if sale_price != old_price:
			is_sale = "true"
			price['sale_color'] = {color: sale_price[0][1:]}
			price['original'] = original_price
		else:
			price['sale_color'] = {}

		# if original_price:
		# 	price['sale_color'] = {}
		# else:
		# 	is_sale = "true"
		# 	price['sale_color'] = {color: sale_price[0][1:]}
		# 	price['original'] = original_price
			

		options = {}
		option_sizes = sel.xpath('//select[@id="size"]/option/text()').extract()[1:]
		# option_sizes = sel.xpath('//select[@id="size"]').extract()

		# print option_sizes
		

		sold_out_sizes = sel.xpath('//select[@id="size"]/option[@class="\n\t\t\t\t\t\t\t\t\t\tno-stock \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"]/text()').extract()
		# print sold_out_sizes

		# return

		if not option_sizes:
			option_sizes = ["one size"]

		# if not sold_out_sizes:
		# 	sold_out_sizes = ["one size"]

		# if not color:
		# 	color = ["one color"]

		options["size"] = option_sizes
		options["color"] = [color]

		thumbnail_images = sel.xpath('//ul[@class="customScroll"]/li/img/@src').extract()

		images = {}
		image_temp_arr = []

		for thumbnail_image in thumbnail_images:
			image_temp_arr.append(thumbnail_image.replace("small", "xlarge"))

		images[color] = image_temp_arr

		option_inventory = {}

		for option_size in option_sizes:
			is_soldout_size = "false"
			for sold_out_size in sold_out_sizes:
				if option_size == sold_out_size:
					is_soldout_size = "true"
					break

			if is_soldout == "true":
				option_inventory["color*_*" + color + "-_-size*_*" + option_size.replace('.', '||')] = "unavailable"
			else:
				if is_soldout_size == "true":
					option_inventory["color*_*" + color + "-_-size*_*" + option_size.replace('.', '||')] = "unavailable"
				else:
					option_inventory["color*_*" + color + "-_-size*_*" + option_size.replace('.', '||')] = "available"

		if not option_inventory:
			if is_soldout == "true":
				option_inventory["color*_*" + color + "-_-size*_*one size"] = "unavailable"
			else:
				option_inventory["color*_*" + color + "-_-size*_*one size"] = "available"
		
		size_table_html = sel.xpath('//div[@class="tab-sizing"]/table/node()').extract()

		sale_rate = 0

		if price["sale_color"].values():
			min_sale_price =  min(price["sale_color"].values())
			sale_rate = math.ceil((float(price["original"]) - float(min_sale_price))/float(price["original"])*100)


		# print size_table_html

		item = LulusItem()

		category = self.get_all_category(response.url)

		if not category:
			return

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "lulus.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = brand[0] if brand else "LuLu*s"
		item['breadcrumb'] = ""
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		item['is_soldout'] = is_soldout
		item['keywords'] = ""
		item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

	def get_all_category(self, url):
		url_low = url.lower()

		if 'dress' in url_low:
			return 'women_dresses'
		elif 'heels' in url_low or 'pumps' in url_low:
			return "women_pumps"
		elif 'sandals' in url_low:
			return "women_sandals"
		elif 'flats' in url_low:
			return "women_flats"
		elif 'sneakers' in url_low:
			return "women_sneakers"
		elif 'booties' in url_low or 'boots' in url_low:
			return "women_boots_booties"
		elif 'tank-top' in url_low:
			return "women_tank_tops"
		elif 'tee' in url_low:
			return "women_tops"
		elif 'top' in url_low:
			return "women_tops"
		elif 'sweater' in url_low:
			return "women_sweaters_knits"
		elif 'jacket' in url_low or 'blazer' in url_low:
			return "women_jackets"
		elif 'coat' in url_low:
			return "women_coats"
		elif 'romper' in url_low or 'jumpsuit' in url_low:
			return "women_jumpsuit"
		elif 'skirt' in url_low:
			return "women_skirts"
		elif 'shorts' in url_low:
			return "women_shorts"
		elif 'jeans' in url_low:
			return "women_jeans"
		elif 'pants' in url_low:
			return "women_pants"
		elif 'handbag' in url_low:
			return "women_bags_totes"
		elif 'cluch' in url_low:
			return "women_bags_clutches"
		elif 'purse' in url_low:
			return "women_bags_shoulder_bags"
		elif 'tote' in url_low:
			return "women_bags_totes"
		elif 'backpack' in url_low:
			return "women_bags_backpacks"
		elif 'scarf' in url_low:
			return "women_accessories_scarves"
		elif 'sunglasses' in url_low:
			return "women_accessories_eyewear"
		elif 'belt' in url_low:
			return "women_accessories_belts"
		elif 'hat' in url_low:
			return "women_accessories_hats"
		elif 'necklace' in url_low:
			return "women_jewelry_necklaces"
		elif 'earrings' in url_low:
			return "women_jewelry_earrings"
		elif 'bracelet' in url_low:
			return "women_jewelry_bracelets"
		elif 'ring' in url_low:
			return "women_jewelry_rings"
		elif 'watch' in url_low:
			return "women_accessories_watches"
		elif 'bikini' in url_low:
			return "women_bikinis"
		elif 'swimsuit' in url_low:
			return "women_one_pieces"
		elif 'cover-up' in url_low:
			return "women_cover_ups"




	# def get_category(self, breadcrumb_str, title):

	# 	if breadcrumb_str == 'dress':
	# 		return get_dress_category(title)
	# 	elif breadcrumb_str == 'heels':
	# 		return "women_pumps"
	# 	elif breadcrumb_str == 'sandals':
	# 		return "women_sandals"
	# 	elif breadcrumb_str == 'flats':
	# 		return "women_flats"
	# 	elif breadcrumb_str == 'boots':
	# 		return "women_boots_booties"
	# 	elif breadcrumb_str == 'tops':
	# 		return get_top_category(title)
	# 	elif breadcrumb_str == 'rompers-jumpsuits':
	# 		return "women_jumpsuit"
	# 	elif breadcrumb_str == 'skirts':
	# 		return get_skirt_category(title)
	# 	elif breadcrumb_str == 'pants':
	# 		return get_pants_category(title)
	# 	elif breadcrumb_str == 'handbags-and-purses':
	# 		return get_bags_category(title)
	# 	elif breadcrumb_str == 'scarves':
	# 		return "women_accessories_scarves"
	# 	elif breadcrumb_str == 'belts':
	# 		return "women_accessories_belts"
	# 	elif breadcrumb_str == 'hats':
	# 		return "women_accessories_hats"
	# 	elif breadcrumb_str == 'necklaces':
	# 		return "women_jewelry_necklaces"
	# 	elif breadcrumb_str == 'earrings':
	# 		return "women_jewelry_earrings"
	# 	elif breadcrumb_str == 'bracelets':
	# 		return "women_jewelry_bracelets"
	# 	elif breadcrumb_str == 'rings':
	# 		return "women_jewelry_rings"
	# 	elif breadcrumb_str == 'watches':
	# 		return "women_accessories_watches"
	# 	elif breadcrumb_str == 'swimwear':
	# 		return get_swimwear_category(title)
	# 	else:
	# 		return "women"

	# def get_shoes_category(self, url):


	# def get_dress_category(self, url):
	# 	url_low = url.lower()
	# 	if 'dress' in url_low:
	# 		return "women_dresses"
	# 	else:
	# 		return "women"

	# def get_top_category(self, title):
	# 	title_low = title.lower()
	# 	if 'tank top' in title_low:
	# 		return "women_tank_tops"
	# 	elif 'sweater' in title_low:
	# 		return "women_sweaters_knits"
	# 	elif 'blazer' in title_low:
	# 		return "women_jackets"
	# 	elif 'jacket' in title_low:
	# 		return "women_jackets"
	# 	elif 'coat' in title_low:
	# 		return "women_coats"
	# 	else:
	# 		return "women_tops"

	# def get_skirt_category(self, title):
	# 	title_low = title.lower()
	# 	if 'maxi' in title_low:
	# 		return "women_maxi_skirts"
	# 	elif 'midi' in title_low:
	# 		return "women_midi_skirts"
	# 	elif 'mini' in title_low:
	# 		return "women_mini_skirts"
	# 	else:
	# 		return "women_skirts"

	# def get_pants_category(self, title):
	# 	title_low = title.lower()

	# 	if 'jeans' in title_low:
	# 		if 'skinny' in title_low:
	# 			return "women_skinny_jeans"
	# 		elif 'flare' in title_low:
	# 			return "women_flare_wide_jeans"
	# 		else:
	# 			return "women_jeans"
	# 	else:
	# 		return "women_pants"

	# def get_backs_category(self, title):
	# 	title_low = title.lower()

	# 	if 'backpack' in title_low:
	# 		return "women_bags_backpacks"
	# 	elif 'handbag' in title_low:
	# 		return "women_bags_totes"
	# 	elif 'clutch' in title_low:
	# 		return "women_bags_clutches"
	# 	elif 'tote' in title_low:
	# 		return "women_bags_totes"
	# 	else:
	# 		return "women_bags"

	# def get_swimwear_category(self, title):
	# 	title_low = title.lower()

	# 	if 'one piece' in title_low:
	# 		return "women_one_pieces"
	# 	elif 'bikini' in title_low:
	# 		return "women_bikinis"
	# 	elif 'cover-up' in title_low:
	# 		return 'women_cover_ups'
	# 	else:
	# 		return "women_swimwear"





