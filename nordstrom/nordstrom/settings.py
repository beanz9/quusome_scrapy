# Scrapy settings for nordstrom project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'nordstrom'

SPIDER_MODULES = ['nordstrom.spiders']
NEWSPIDER_MODULE = 'nordstrom.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'nordstrom (+http://www.yourdomain.com)'
