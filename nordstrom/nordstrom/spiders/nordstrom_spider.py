#-*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from cStringIO import StringIO
import re
import json
import requests

class NordstromSpider(CrawlSpider):
	name = "nordstrom"
	allow_domains = ["nordstrom.com"]
	start_urls = [
		"http://shop.nordstrom.com/S/halogen-sheer-yoke-long-sleeve-sweater-regular-petite/3567734?origin=category-personalizedsort&contextualcategoryid=0&fashionColor=HEATHER+FAIR+GREY&resultback=3958&cm_sp=personalizedsort-_-browseresults-_-2_12_A"
	]

	def parse(self, response):
		sel = Selector(response)
		javascript = sel.xpath('//script').extract()[1]
		javascript = javascript.split('"styleJson":')[1][1:]
		javascript = javascript.split(',"availSkuCount"')[0][:-1]
		# javascript = javascript[:-23]
		print javascript
		
		# print javascript.encode('ascii','ignore')

		# product = json.loads()

		# print product