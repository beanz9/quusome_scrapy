#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from allsaints.items import AllsaintsItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json

class AllsaintsSpider(CrawlSpider):
	name = "allsaints"
	allow_domains = ["allsaints.com"]
	start_urls = [
		
		# "http://www.us.allsaints.com/women/tops/allsaints-itita-top-wm105f/?colour=33&category=115"
		# "http://www.us.allsaints.com/women/footwear/allsaints-k80-sneaker/?category=331&colour=5",
		# "http://www.us.allsaints.com/women/footwear/allsaints-k80-sneaker/?category=331&colour=5",
		# "http://www.us.allsaints.com/women/sweaters/allsaints-elgar-dress/?category=22&colour=33"

		"http://www.us.allsaints.com/women/leather/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/jackets/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/dresses/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/coats/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/tops/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/sweaters/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/t-shirts/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/denim/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/pants-and-leggings/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/skirts-and-shorts/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/footwear/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/handbags/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/women/shop-accessories/style,any/colour,any/size,any/",

		"http://www.us.allsaints.com/men/leather-jackets/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/coats/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/sweatshirts/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/sweaters/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/shirts/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/t-shirts/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/polos/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/denim/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/pants/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/shorts-and-swim-trunks/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/footwear/style,any/colour,any/size,any/",
		"http://www.us.allsaints.com/men/shop-accessories/style,any/colour,any/size,any/"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="colour-swatch-images"]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@id="pagination_chevron_right"]')), follow=True ),
  	
  ]


	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//span[@itemprop="name"]/text()').extract()[0]
		desc1 = sel.xpath('//div[@id="editors-notes"]/text()').extract()[0]
		desc2 = sel.xpath('//div[@id="details"]/node()').extract()

		desc = desc1 + "".join(desc2)
		breadcrumb = sel.xpath('//div[@class="breadcrumbs visible_only_desktop"]/a/@href').extract()[0].split('/')
		breadcrumb = filter(None, breadcrumb)
		base_price = sel.xpath('//div[@id="desktop_product_name"]/div/text()').extract()[0].strip()[1:]

		is_sale = 'false'

		options = {}
		option_inventory = {}
		images = {}

		price = {}
		price['sale_color'] = {}

		color = sel.xpath('//div[@class="colour-name visible_only_desktop"]/text()').extract()
		options['color'] = color

		size = sel.xpath('//span[@class="size-name"]/text()').extract()
		options['size'] = size

		price['original'] = base_price

		stock = sel.xpath('//a[@class="size-label"]/@data-stock').extract()

		for i in range(0, len(size)):
			if stock[i] == '0':
				option_inventory["color*_*" + color[0] + "-_-size*_*" + size[i].replace('.', '||')] = "unavailable"
			else:
				option_inventory["color*_*" + color[0] + "-_-size*_*" + size[i].replace('.', '||')] = "available"

		image = sel.xpath('//div[@class="swiper-slide"]/img/@src').extract()

		image_arr = []

		for i in image:
			image_arr.append("http:" + i.replace('320', '600'))


		images[color[0]] = image_arr
		size_table_html_tmp = sel.xpath('//table[@class="measurements responsive"]').extract()
		
		if size_table_html_tmp:
			size_table_html = size_table_html_tmp[0].replace('<table class="measurements responsive">', '').replace('</table>', '')
		else:
			size_table_html = "<a href='http://www.us.allsaints.com/size-guide/womenswear/' target='_blank'>사이즈를 확인하세요.</a>"

		category = self.get_category(breadcrumb)


		if not category:
			return

		item = AllsaintsItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "allsaints.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "ALLSAINTS"
		item['breadcrumb'] = breadcrumb
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item	


	def get_category(self, breadcrumb):
		if 'men' in breadcrumb:
			return self.get_men_category(breadcrumb)
		else:
			return self.get_women_category(breadcrumb)

	def get_women_category(self, breadcrumb):
		if 'leather' in breadcrumb:
			return 'women_jackets'
		elif 'jackets' in breadcrumb:
			return 'women_jackets'
		elif 'dresses' in breadcrumb:
			return 'women_dresses'
		elif 'coats' in breadcrumb:
			return 'women_coats'
		elif 'tops' in breadcrumb:
			return 'women_tops'
		elif 'sweaters' in breadcrumb:
			return 'women_sweaters_knits'
		elif 't-shirts' in breadcrumb:
			return 'women_tops'
		elif 'denim' in breadcrumb:
			return 'women_jeans'
		elif 'pants-and-leggings' in breadcrumb:
			return 'women_pants'
		elif 'skirts-and-shorts' in breadcrumb:
			return 'women_skirts'
		elif 'footwear' in breadcrumb:
			return 'women_boots_booties'
		elif 'handbags' in breadcrumb:
			return 'women_bags_handbags'
		elif 'shop-accessories' in breadcrumb:
			return 'women_accessories'

	def get_men_category(self, breadcrumb):
		if 'leather-jackets' in breadcrumb:
			return 'men_clothing_jackets'
		elif 'coats' in breadcrumb:
			return 'men_clothing_outerwears'
		elif 'sweatshirts' in breadcrumb:
			return 'men_clothing_sweaters'
		elif 'sweaters' in breadcrumb:
			return 'men_clothing_sweaters'
		elif 'shirts' in breadcrumb:
			return 'men_clothing_shirts'
		elif 'polos' in breadcrumb:
			return 'men_clothing_shirts'
		elif 'denim' in breadcrumb:
			return 'men_clothing_jeans'
		elif 'shorts-and-swim-trunks' in breadcrumb:
			return 'men_clothing_shorts'
		elif 'footwear' in breadcrumb:
			return 'men_shoes'
		elif 'shop-accessories' in breadcrumb:
			return 'men_accessories'
