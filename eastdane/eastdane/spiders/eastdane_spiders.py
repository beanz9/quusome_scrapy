#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
# from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from eastdane.items import EastdaneItem
from eastdane.eastdane_link_extractor import EastdaneLinkExtractor
from eastdane.eastdane_next_link_extractor import EastdaneNextLinkExtractor
from cStringIO import StringIO
import re
import json
import requests
import collections
import math

class EastdaneSpider(CrawlSpider):
	name = "eastdane"
	allow_domains = ["eastdane.com"]
	start_urls = [
		"http://www.eastdane.com/clothing-shirts/br/v=1/19207.htm",
		"http://www.eastdane.com/clothing-jeans/br/v=1/19208.htm",
		"http://www.eastdane.com/clothing-outerwear/br/v=1/19209.htm",
		"http://www.eastdane.com/clothing-pants/br/v=1/19210.htm",
		"http://www.eastdane.com/clothing-polos-tees/br/v=1/19211.htm",
		"http://www.eastdane.com/clothing-shorts-swim/br/v=1/19212.htm",
		"http://www.eastdane.com/clothing-suits-blazers/br/v=1/19213.htm",
		"http://www.eastdane.com/clothing-sweaters/br/v=1/19214.htm",
		"http://www.eastdane.com/clothing-sweatshirts/br/v=1/19215.htm",
		"http://www.eastdane.com/clothing-underwear/br/v=1/19216.htm",

		"http://www.eastdane.com/shoes-boots/br/v=1/19217.htm",
		"http://www.eastdane.com/shoes-lace-ups/br/v=1/19218.htm",
		"http://www.eastdane.com/shoes-loafers-slip-ons/br/v=1/19219.htm",
		"http://www.eastdane.com/shoes-sandals/br/v=1/19220.htm",
		"http://www.eastdane.com/shoes-sneakers/br/v=1/19221.htm",

		"http://www.eastdane.com/bags/br/v=1/19222.htm",
		"http://www.eastdane.com/accessories-belts/br/v=1/19223.htm",
		"http://www.eastdane.com/accessories-belts/br/v=1/19223.htm",
		"http://www.eastdane.com/accessories-hats-scarves-gloves/br/v=1/19224.htm",
		"http://www.eastdane.com/accessories-jewelry/br/v=1/19225.htm",
		"http://www.eastdane.com/accessories-socks/br/v=1/20262.htm",
		"http://www.eastdane.com/accessories-sunglasses/br/v=1/19227.htm",
		"http://www.eastdane.com/accessories-tech/br/v=1/19228.htm",
		"http://www.eastdane.com/accessories-ties-pocket-squares/br/v=1/19229.htm",
		"http://www.eastdane.com/accessories-travel/br/v=1/19230.htm",
		"http://www.eastdane.com/accessories-wallets-money-clips/br/v=1/19231.htm",
		"http://www.eastdane.com/accessories-watches/br/v=1/19232.htm"

	]

	def custom_process_value(value):
 		return value.split('\'')[1]


 		# return links

	rules = [
		Rule(EastdaneLinkExtractor(), callback="parse_item" ),
		Rule(EastdaneNextLinkExtractor(), follow=True ),
  ]

 	


 	# def parse_start_url(self, response):
 	# 	print "parse_start_url"


	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//span[@class="product-title"]/text()').extract()
		desc = sel.xpath('//div[@itemprop="description"]/node()').extract()

		original_price = sel.xpath('//meta[@itemprop="price"]/@content').extract()[0]

		price = {}
		price['original'] = original_price[1:]

		price_blocks = sel.xpath('//div[@id="productPrices"]/div[@class="priceBlock"]')
		is_sale = "false"

		#멀티가격일경우
		sale_price = {}
		for block in price_blocks:
			block_price = block.xpath('span[@class="salePrice"]/text()').extract()
			if len(block_price) == 0:
				continue
			is_sale = "true"
			block_color = block.xpath('span[@class="priceColors"]/text()').extract()[0]
			for block_color_split in block_color.split(','):
				sale_price[block_color_split.strip()] = block_price[0][1:].strip()
	

		price['sale_color'] =  sale_price

		if not price:
			return

		options = {}

		option_colors = sel.xpath('//div[@id="swatches"]/img[not(contains(@class, "swatchImage hover hidden"))]/@title').extract()
		option_sizes = sel.xpath('//div[@id="sizes"]/span/text()').extract()
		options['size'] = option_sizes
		options['color'] = option_colors

		breadcrumb = []
		breadcrumb = sel.xpath('//ul[@class="breadcrumb-list"]/li[@class="breadcrumb"]/span/text()').extract()
		breadcrumb_child = sel.xpath('//ul[@class="breadcrumb-list"]/li[@class="breadcrumb"]/a/span/text()').extract()
		breadcrumb.extend(breadcrumb_child)


		breadcrumb_str = ">".join(breadcrumb)

		brand = sel.xpath('//a[@itemprop="brand"]/text()').extract()

		swatches = sel.xpath('//div[@id="swatches"]/img')
	
		images = {}

		javascript = sel.xpath('//div[@id="content"]/script[@type="text/javascript"]/text()').extract()[0]
		javascript = javascript[20:].strip()
		javascript = javascript[:-1].strip()
		product = json.loads(javascript)

		#image
		for key in (collections.OrderedDict(sorted(product["colors"].items()))).keys():
			color_name = product["colors"][key]["colorName"]

			color_images = []

			for key2 in (collections.OrderedDict(sorted(product["colors"][key]["images"].items()))).keys():
				color_images.append(product["colors"][key]["images"][key2]["zoom"])

			images[color_name] = color_images

		
		colors = sel.xpath('//div[@id="swatches"]/img/@title').extract()
		sizes = sel.xpath('//div[@id="sizes"]/span/text()').extract()

		inventory = []
		for key in product["colors"].keys():
			for key2 in product["colors"][key]["sizes"]:
				for size in product["sizes"].keys():
					if key2 == product["sizes"][size]["sizeCode"]:
						inventory.append(product["colors"][key]["colorName"] + "-" + size)

		print "================================"

		# option_inventory = []
		option_inventory = {}

		for color in colors:
			for size in sizes:
				size = size.replace('.', '||')
				if color + "-" + size in inventory:
					option_inventory["color*_*" + color + "-_-size*_*" + size] = "available"
					# option_inventory.append("color*_*" + color + "-_-size*_*" + size + ":_:available")
				else:
					option_inventory["color*_*" + color + "-_-size*_*" + size] = "unavailable"
					# option_inventory.append("color*_*" + color + "-_-size*_*" + size + ":_:unavailable")


		#size table
		size_table_html = ""

		table_tr = sel.xpath('//table/tr')

		size_table_html = StringIO()
		for tr in table_tr:
			tr_html = tr.xpath('td[not(@colspan)]').extract()
			
			if tr_html:
				size_table_html.write("<tr>" + "".join(tr_html) + "</tr>")

		sale_rate = 0

		if price["sale_color"].values():
			min_sale_price =  min(price["sale_color"].values())
			sale_rate = math.ceil((float(price["original"]) - float(min_sale_price))/float(price["original"])*100)



		item = EastdaneItem()

		item['title'] = title[0].strip() if title else ""
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "eastdane.com"
		item['desc'] = "".join(desc).strip() if desc else ""
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = brand[0].strip() if brand else ""
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb_str, title[0])
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html.getvalue() if size_table_html else ""
		item['keywords'] = ""
		item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

	def get_category(self, breadcrumb_str, title):
		if breadcrumb_str.startswith("Clothing"):
			return self.get_clothing_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes"):
			return self.get_shoes_categoty(breadcrumb_str)
		elif breadcrumb_str.startswith("Accessories"):
			return self.get_accessories_category(breadcrumb_str, title)
		else:
			return "men"

	def get_clothing_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("Clothing>Shirts"):
			return "men_clothing_shirts"
		elif breadcrumb_str.startswith("Clothing>Jeans"):
			return "men_clothing_jeans"
		elif breadcrumb_str.startswith("Clothing>Outerwear"):
			return "men_clothing_outerwears"
		elif breadcrumb_str.startswith("Clothing>Pants"):
			return "men_clothing_pants"
		elif breadcrumb_str.startswith("Clothing>Polos & Tees"):
			return "men_clothing_tshirts"
		elif breadcrumb_str.startswith("Clothing>Shorts & Swim"):
			return "men_clothing_shorts"
		elif breadcrumb_str.startswith("Clothing>Suits & Blazers"):
			return "men_clothing_suits"
		elif breadcrumb_str.startswith("Clothing>Sweaters"):
			return "men_clothing_sweaters"
		elif breadcrumb_str.startswith("Clothing>Sweatshirts"):
			return "men_clothing_sweatshirts"
		elif breadcrumb_str.startswith("Clothing>Underwear"):
			return "men_clothing_underwear"
		else:
			return "men_clothing"

	def get_shoes_categoty(self, breadcrumb_str):
		if breadcrumb_str.startswith("Shoes>Boots"):
			return "men_shoes_boots"
		elif breadcrumb_str.startswith("Shoes>Lace-Ups"):
			return "men_shoes_raceups"
		elif breadcrumb_str.startswith("Shoes>Loafers & Slip-Ons"):
			return "men_shoes_loafers"
		elif breadcrumb_str.startswith("Shoes>Sandals"):
			return "men_shoes_sandals"
		elif breadcrumb_str.startswith("Shoes>Sneakers"):
			return "men_shoes_sneakers"
		else:
			return "men_shoes"

	def get_accessories_category(self, breadcrumb_str, title):
		if breadcrumb_str.startswith("Accessories>Bags"):
			return self.get_bags_categories(title)
		elif breadcrumb_str.startswith("Accessories>Belts"):
			return "men_accessories_belts"
		elif breadcrumb_str.startswith("Accessories>Hats, Scarves & Gloves"):
			return self.get_hats_scarves_gloves_category(title)
		elif breadcrumb_str.startswith("Accessories>Socks"):
			return "men_accessories_socks"
		elif breadcrumb_str.startswith("Accessories>Sunglasses"):
			return "men_accessories_eyewears"
		elif breadcrumb_str.startswith("Accessories>Tech Accessories"):
			return "men_accessories_tech"
		elif breadcrumb_str.startswith("Accessories>Ties & Pocket Squares"):
			return self.get_ties_pocketsquares_categories(title)
		elif breadcrumb_str.startswith("Accessories>Travel Accessories"):
			return "men_accessories_travel"
		elif breadcrumb_str.startswith("Accessories>Wallets & Money Clips"):
			return "men_accessories_wallets"
		elif breadcrumb_str.startswith("Accessories>Watches"):
			return "men_accessories_watches"
		elif breadcrumb_str.startswith("Accessories>Jewelry"):
			return self.get_jewelry_cagtegories(title)
		else:
			return "men_accessories"

	def get_jewelry_cagtegories(self, title):
		title_low = title.lower()

		if "necklace" in title_low:
			return "men_jewelry_necklaces"
		elif "bracelet" in title_low:
			return "men_jewelry_bracelets"
		elif "cufflinks" in title_low:
			return "men_jewelry_cufflinks"
		elif "ring" in title_low:
			return "men_jewelry_rings"
		elif "bar" in title_low:
			return "men_jewelry_tiebar"
		elif "earring" in title_low:
			return "men_jewelry_earrings"
		else:
			"men_jewelry"

	def get_ties_pocketsquares_categories(self, title):
		title_low = title.lower()

		if "bandana" in title_low:
			return "men_accessories_pocketsquares"
		elif "pocket square" in title_low:
			return "men_accessories_pocketsquares"
		elif "tie" in title_low:
			return "men_accessories_ties"
		else:
			return "men_accessories"

	def get_hats_scarves_gloves_category(self, title):
		title_low = title.lower()

		if "hat" in title_low:
			return "men_accessories_hats"
		if "beanie" in title_low:
			return "men_accessories_hats"
		if "cap" in title_low:
			return "men_accessories_hats"
		elif "scarf" in title_low:
			return "men_accessories_scarves"
		elif "gloves" in title_low:
			return "men_accessories_gloves"
		else:
			return "men_accessories"

	def get_bags_categories(self, title):
		title_low = title.lower()

		if "backpack" in title_low:
			return "men_bags_backpacks"
		elif "tote" in title_low:
			return "men_bags_totes"
		elif "briefcase" in title_low:
			return "men_bags_briefcases"
		else:
			return "men_bags"

	
		




