from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.link import Link
from w3lib.url import url_query_cleaner
import json


class EastdaneNextLinkExtractor(SgmlLinkExtractor):
	def _extract_links(self, response, response_url, response_encoding, base_url=None):
		ret = []

		if base_url is None:
			base_url = urljoin(response_url, self.base_url) if self.base_url else response_url
		
		script = response.body.split('bop.config.filters = ')[1]
		script = script.split('bop.config.products = {')[0]
		product_json = json.loads(script.strip()[:-1].strip())

		if product_json["metadata"]:
			base_index = product_json["metadata"]["baseIndex"] + 40
			product_count = product_json["metadata"]["totalProductCount"]

			if base_index <= product_count:
				url = url_query_cleaner(response_url.encode(response_encoding)) + "?baseIndex=" + str(base_index)
				link = Link(url, "")
				ret.append(link)

		return ret

	def extract_links(self, response):
		# wrapper needed to allow to work directly with text
		links = self._extract_links(response, response.url, response.encoding)
		links = self._process_links(links)
		return links

