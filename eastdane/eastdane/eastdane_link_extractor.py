from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.link import Link
import json


class EastdaneLinkExtractor(SgmlLinkExtractor):
	def _extract_links(self, response_text, response_url, response_encoding, base_url=None):

		ret = []
		if base_url is None:
			base_url = urljoin(response_url, self.base_url) if self.base_url else response_url

		script = response_text.split('bop.config.filters = ')[1]
		script = script.split('bop.config.products = {')[0]
		product_json = json.loads(script.strip()[:-1].strip())

		for product in product_json["products"]:
			link = Link(("http://www.eastdane.com" + product["productDetailLink"]).encode(response_encoding), product["shortDescription"].encode(response_encoding))
			ret.append(link)

		return ret


	