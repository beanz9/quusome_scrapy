#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from andrewmarc.items import AndrewmarcItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json
from collections import OrderedDict
from andrewmarc.link_extractor import LinkExtractor

class AndrewmarcSpider(CrawlSpider):
	name = "andrewmarc"
	allow_domains = ["andrewmarc.com"]
	start_urls = [
		# "http://www.andrewmarc.com/shop-women/lightweight-downs/fiat.html"
		# "http://www.andrewmarc.com/shop-women/leather-jkts/beth.html",
		# "http://www.andrewmarc.com/sale/harlow.html"
		"http://www.andrewmarc.com/shop-women/leather-jkts.html",
		"http://www.andrewmarc.com/shop-women/fur-coats.html",
		"http://www.andrewmarc.com/shop-women/wool-coats.html",
		"http://www.andrewmarc.com/shop-women/lightweight-downs.html",
		"http://www.andrewmarc.com/shop-women/down-coats.html",
		"http://www.andrewmarc.com/shop-women/ready-to-wear/jackets.html",
		"http://www.andrewmarc.com/shop-women/ready-to-wear/tops.html",
		"http://www.andrewmarc.com/shop-women/ready-to-wear/bottoms.html",
		"http://www.andrewmarc.com/shop-women/ready-to-wear/dresses.html",

		"http://www.andrewmarc.com/shop-men/leather-jkts.html",
		"http://www.andrewmarc.com/shop-men/fur-coats.html",
		"http://www.andrewmarc.com/shop-men/wool-coats.html",
		"http://www.andrewmarc.com/shop-men/down-coats.html",
		# "http://www.andrewmarc.com/shop-men/shoes.html",
		# "http://www.andrewmarc.com/shop-men/accessories.html"
	]

	rules = [
		Rule(LinkExtractor(), follow=False, callback="parse_item" )
  	
  ]

	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//div[@class="product-details-head"]/h2/text()').extract()[0]
		sub_title = sel.xpath('//div[@class="product-details-head"]/p/text()').extract()

		if sub_title:
			title = "[" + sub_title[0].upper() + "] " + title
		else:
			title = "[ANDREW MARC] " + title

		desc = "".join(sel.xpath('//div[@class="product-details-info"]/node()').extract())
		category = sel.xpath('//body/@class').extract()[0]

		script = sel.xpath('//html').extract()
		json_obj = json.loads(str(script).split(' var tree = ')[1].split(';')[0] , strict=False)
		product_id = str(script).split('product_id[index] = ')[1].split(';')[0]

		base_price =  json_obj["basePrice"]
		old_price = json_obj["oldPrice"]


		is_sale = 'false'

		if base_price != old_price:
			is_sale = 'true'

		options = {}
		option_inventory = {}
		images = {}

		price = {}
		price['sale_color'] = {}
		price['original'] = old_price
		color_arr = []
		option_arr = []

		for j in json_obj["children"]:
			color_arr.append(json_obj["children"][j]["label"])
			if is_sale == 'true':
				price['sale_color'][json_obj["children"][j]["label"]] = base_price

			url = 'http://www.andrewmarc.com/catalog/product/mediaJson/product_id/' + product_id + '/medias/%7B%22medias%22:[%7B%22x%22:%20%2270%22,%22y%22:%20%2292%22,%22r%22:%20%22255%22,%22g%22:%20%22255%22,%22b%22:%20%22255%22%7D,%7B%22x%22:%20%222500%22,%22y%22:%20%220%22,%22r%22:%20%22255%22,%22g%22:%20%22255%22,%22b%22:%20%22255%22%7D]%7D/attributes/%7B%2276%22:%22' + json_obj["children"][j]["value"] + '%22%7D'
			result = requests.get(url).text
			json_result = json.loads(result , strict=False)
			image_arr = []
			for k in json_result["media_gallery"]:
				idx = 0
				
				for k2 in k:
					if idx == 0:
						image_arr.append(k[k2])
					idx = idx + 1
			images[json_obj["children"][j]["label"]] = image_arr



			for j2 in json_obj["children"][j]["children"]:

				sellable =  str(json_obj["children"][j]["children"][j2]["children"]).split("'sellable':")[1].split(',')[0]
				
				if sellable:
					option_inventory["color*_*" + json_obj["children"][j]["label"] + "-_-size*_*" + json_obj["children"][j]["children"][j2]["label"].replace('.', '||')] = "available"
				else:
					option_inventory["color*_*" + json_obj["children"][j]["label"] + "-_-size*_*" + json_obj["children"][j]["children"][j2]["label"].replace('.', '||')] = "unavailable"

				option_arr.append(json_obj["children"][j]["children"][j2]["label"])

		options['size'] = list(OrderedDict.fromkeys(option_arr))
		options['color'] = color_arr
		size_table_html = ""


		category2 = self.get_category(category)

		if not category2:
			return

		if 'women' in category:
			size_table_html = '<img src="http://www.andrewmarc.com/media/wysiwyg/WOMANsizeCHART_reg.jpg" alt="">'
		else:
			if 'men_shoes' != category2 and 'men_accessories' != category2:
				size_table_html = '<img src="http://www.andrewmarc.com/media/wysiwyg/MENsizeCHART_reg.jpg" alt="">'

		item = AndrewmarcItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "andrewmarc.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "ANDREW MARC"
		item['breadcrumb'] = ""
		item['category'] = category2
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item	



	def get_category(self, category):
		if 'women' in category:
			return self.get_women_category(category)
		else:
			return self.get_men_category(category)

	def get_women_category(self, category):
		if 'categorypath-shop-women-leather-jkts-html' in category:
			return 'women_jackets'
		elif 'categorypath-shop-women-fur-coats-html' in category:
			return 'women_coats'
		elif 'categorypath-shop-women-wool-coats-html' in category:
			return 'women_coats'
		elif 'categorypath-shop-women-lightweight-downs-html' in category:
			return 'women_clothing_outerwears'
		elif 'categorypath-shop-women-down-coats-html' in category:
			return 'women_coats'
		elif 'categorypath-shop-women-ready-to-wear-jackets-html' in category:
			return 'women_jackets'
		elif 'categorypath-shop-women-ready-to-wear-tops-html' in category:
			return 'women_tops'
		elif 'categorypath-shop-women-ready-to-wear-bottoms-html' in category:
			return 'women_skirts'
		elif 'categorypath-shop-women-ready-to-wear-dresses-html' in category:
			return 'women_dresses'

	def get_men_category(self, category):
		if 'categorypath-shop-men-leather-jkts-html' in category:
			return 'men_clothing_jackets'
		elif 'categorypath-shop-men-fur-coats-html' in category:
			return 'men_clothing_outerwears'
		elif 'categorypath-shop-men-wool-coats-html' in category:
			return 'men_clothing_outerwears'
		elif 'categorypath-shop-men-down-coats-html':
			return 'men_clothing_outerwears'
		elif 'categorypath-shop-men-shoes-html' in category:
			return 'men_shoes'
		elif 'categorypath-shop-men-accessories-html' in category:
			return 'men_accessories'




