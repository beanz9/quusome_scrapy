#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from abercrombie.items import AbercrombieItem
from cStringIO import StringIO
from scrapy.http import Request
import json
import tokenize
import token
import requests




class AbercrombieSpider(CrawlSpider):
	name = "abercrombie"
	allow_domains = ["abercrombie.com"]
	start_urls = [
		#no sale one color
		# "http://www.abercrombie.com/shop/wd/womens-skater-dresses-and-rompers/dawn-dress-2674089_01",
		#sale one color
		# "http://www.abercrombie.com/shop/wd/womens-dresses-clearance/johanna-dress-1157988_01",
		#no sale multi color
		# "http://www.abercrombie.com/shop/wd/womens-sleeveless-fashion-tops/jenny-cami-2954571_01",
		#sale multi color
		# "http://www.abercrombie.com/shop/wd/womens-dresses-clearance/christa-dress-2605569_03"

		"http://www.abercrombie.com/shop/wd/womens-fashion-tops",
		# "http://www.abercrombie.com/shop/wd/womens-dresses",
		"http://www.abercrombie.com/shop/wd/womens-skater-dresses-and-rompers",
		"http://www.abercrombie.com/shop/wd/womens-babydoll-dresses-and-rompers",
		"http://www.abercrombie.com/shop/wd/womens-maxi-dresses-and-rompers",
		"http://www.abercrombie.com/shop/wd/womens-bodycon-dresses-and-rompers",
		"http://www.abercrombie.com/shop/wd/womens-romper-dresses-and-rompers",

		"http://www.abercrombie.com/shop/wd/womens-shirts",
		"http://www.abercrombie.com/shop/wd/womens-graphic-tees",
		"http://www.abercrombie.com/shop/wd/womens-polos",
		"http://www.abercrombie.com/shop/wd/womens-hoodies-and-sweatshirts",
		"http://www.abercrombie.com/shop/wd/womens-sweaters",
		"http://www.abercrombie.com/shop/wd/womens-outerwear",
		"http://www.abercrombie.com/shop/wd/womens-shorts",
		"http://www.abercrombie.com/shop/wd/womens-skirts",
		"http://www.abercrombie.com/shop/wd/womens-jeans",
		"http://www.abercrombie.com/shop/wd/womens-overalls",
		"http://www.abercrombie.com/shop/wd/womens-leggings",
		"http://www.abercrombie.com/shop/wd/womens-pants",
		"http://www.abercrombie.com/shop/wd/womens-footwear",
		"http://www.abercrombie.com/shop/wd/womens-accessories",
		"http://www.abercrombie.com/shop/wd/womens-sleep"

		#mens
		"http://www.abercrombie.com/shop/wd/mens-outerwear",
		"http://www.abercrombie.com/shop/wd/mens-sweaters",
		"http://www.abercrombie.com/shop/wd/mens-shirts",
		"http://www.abercrombie.com/shop/wd/mens-tees",
		"http://www.abercrombie.com/shop/wd/mens-graphic-tees",
		"http://www.abercrombie.com/shop/wd/mens-polos",
		"http://www.abercrombie.com/shop/wd/mens-hoodies-and-sweatshirts",
		"http://www.abercrombie.com/shop/wd/mens-jeans",
		"http://www.abercrombie.com/shop/wd/mens-joggers",
		"http://www.abercrombie.com/shop/wd/mens-sweatpants",
		"http://www.abercrombie.com/shop/wd/mens-pants",
		"http://www.abercrombie.com/shop/wd/mens-shorts",
		# "http://www.abercrombie.com/shop/wd/mens-swim",
		# "http://www.abercrombie.com/shop/wd/mens-flip-flops",
		"http://www.abercrombie.com/shop/wd/mens-accessories",
		"http://www.abercrombie.com/shop/wd/mens-underwear",


	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="image-wrap lazyload"]/a')), callback="parse_item" ),
		# Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@id="pagination-container-top"]/div[@class="pagination"]/div[@class="pages"]/span[@class="next"]'), tags=("span"), attrs=("data-next-link")), follow=True ),
  ]

	def parse_item(self, response):
		sel = Selector(response)

		data_seq = sel.xpath('//a[@class="swatch-link"]/@data-seq').extract()

		# if data_seq:
		item = self.multi_color(response, data_seq)
		# else:
			# item = self.one_color(response)

		return item


		

	def multi_color(self, response, data_seq):
		sel = Selector(response)

		if not data_seq:
			data_seq = sel.xpath('//input[@name="cseq"]/@value').extract()[:1]

		title = sel.xpath('//h1[@class="name"]/text()').extract()
		breadcrumb = sel.xpath('//div[@id="breadcrumb"]/a/text()').extract()
		list_price = sel.xpath('//h4[@class="list-price"]/del/text()').extract()
		color = sel.xpath('//div[@class="color-wrap"]/p/span[@class="color"]/text()').extract()
		desc = sel.xpath('//p[@class="copy"]/text()').extract()
		is_sale = "false"

		size_table_html = sel.xpath('//li[@class="table"]/table/node()').extract()

		storeId = sel.xpath('//input[@name="storeId"]/@value').extract()[0]
		catalogId = sel.xpath('//input[@name="catalogId"]/@value').extract()[0]
		categoryId = sel.xpath('//input[@name="catId"]/@value').extract()[0]
		productId = sel.xpath('//input[@name="productId"]/@value').extract()[0]

		

		price = {}
		if list_price:
			is_sale = "true"
			price['original'] = list_price[0][1:]
		else:
			price['original'] = sel.xpath('//h4[@class="offer-price"]/text()').extract()[0][1:]
			

		images = {}
		sale_price = {}
		options = {}
		options_color_arr = []
		option_inventory = {}


		for seq in data_seq:
			url = StringIO()
			url.write("http://www.abercrombie.com/webapp/wcs/stores/servlet/GetColorJSON?storeId=")
			url.write(storeId)
			url.write("&catalogId=")
			url.write(catalogId)
			url.write("&categoryId=")
			url.write(categoryId)
			url.write("&productId=")
			url.write(productId)
			url.write("&seq=")
			url.write(seq)
			
			r = requests.get(url.getvalue())
			result = json.loads(r.text, strict=False)

			if is_sale == "true":
				sale_price[result["name"]] = result["pricenum"] 

			options_color_arr.append(result["name"])
			option_size_arr = []
			for r in result["items"]:
				option_size_arr.append(r["size"])
				if r["soldOut"] == "true":
					option_inventory["color*_*" + result["name"] + "-_-size*_*" + r["size"]] = "unavailable"
				else:
					option_inventory["color*_*" + result["name"] + "-_-size*_*" + r["size"]] = "available"

			options["size"] = option_size_arr
			

			prod_num = 1
			model_num = 1
			prod_state = 200
			model_state = 200



			image = []
			while prod_state == 200:

				url_str = StringIO()
				url_str.write("http://anf.scene7.com/is/image/anf/anf_")
				url_str.write(result["imgPrefix"])
				url_str.write("_prod")
				url_str.write(str(prod_num))
				url_str.write("?$productMagnify-anf$")

				url = url_str.getvalue()
				prod_state = requests.get(url).status_code
				prod_num += 1
				if prod_state == 200:
					image.append(url)

			while model_state == 200:
				url_str = StringIO()
				url_str.write("http://anf.scene7.com/is/image/anf/anf_")
				if result["modelImgPrefix"][-2:] == "00":
					url_str.write(result["modelImgPrefix"][:-2] + "01")
				else:
					url_str.write(result["modelImgPrefix"])

				url_str.write("_model")
				url_str.write(str(model_num))
				url_str.write("?$productMagnifyOFP-anf$")

				url = url_str.getvalue()
				model_state = requests.get(url).status_code
				model_num += 1
				if model_state == 200:
					image.append(url)
		

			images[result["name"]] = image

		options["color"] = options_color_arr


		if is_sale:
			price['sale_color'] = sale_price
		else:
			price['sale_color'] = {}


		item = AbercrombieItem()

		item['title'] = title[0].strip() if title else ""
		item['sub_title'] = ""
		item['url'] = response.url
		item['domain'] = "abercrombie.com"
		item['desc'] = desc[0] if desc else ""
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "Abercrombie"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = self.get_size_table_html(sel, breadcrumb)
		item['is_soldout'] = "false"

		return item



	def get_size_table_html(self, sel, breadcrumb):
		breadcrumb_str = ">".join(breadcrumb)
		if breadcrumb_str.startswith("Womens"):
			return self.get_women_size_table_html(sel, breadcrumb)
		else:
			return self.get_men_size_table_html(sel, breadcrumb)



	def get_men_size_table_html(self, sel, breadcrumb):
		breadcrumb_str = ">".join(breadcrumb)

		if (breadcrumb_str.startswith("mens>Outerwear") or
		breadcrumb_str.startswith("mens>Sweaters") or
		breadcrumb_str.startswith("mens>Shirts") or
		breadcrumb_str.startswith("mens>Tees & Henleys") or
		breadcrumb_str.startswith("mens>Graphic Tees") or
		breadcrumb_str.startswith("mens>Polos") or
		breadcrumb_str.startswith("mens>Hoodies & Sweatshirts")):

			table = sel.xpath('//li[@id="mens-tops-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""

		elif (breadcrumb_str.startswith("mens>Jeans") or
		breadcrumb_str.startswith("mens>Sweatpants") or
		breadcrumb_str.startswith("mens>Pants") or
		breadcrumb_str.startswith("mens>Shorts") or
		breadcrumb_str.startswith("mens>Underwear")):

			table = sel.xpath('//li[@id="mens-bottoms-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""



	def get_women_size_table_html(self, sel, breadcrumb):
		breadcrumb_str = ">".join(breadcrumb)

		if (breadcrumb_str.startswith("Womens>Tops") or 
			breadcrumb_str.startswith("Womens>Dresses & Rompers") or 
			breadcrumb_str.startswith("Womens>Shirts") or 
			breadcrumb_str.startswith("Womens>Graphic Tees")or 
			breadcrumb_str.startswith("Womens>Polos") or 
			breadcrumb_str.startswith("Womens>Hoodies & Sweatshirts") or 
			breadcrumb_str.startswith("Womens>Sweaters") or 
			breadcrumb_str.startswith("Womens>Outerwear")):

			table = sel.xpath('//li[@id="womens-tops-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""

		elif (breadcrumb_str.startswith("Womens>Shorts") or breadcrumb_str.startswith("Womens>Skirts")
		or breadcrumb_str.startswith("Womens>Shorts") or breadcrumb_str.startswith("Womens>Jeans")
		or breadcrumb_str.startswith("Womens>Overalls") or breadcrumb_str.startswith("Womens>Sweatpants")
		or breadcrumb_str.startswith("Womens>Leggings") or breadcrumb_str.startswith("Womens>Pants")
		or breadcrumb_str.startswith("Womens>Leggings") or breadcrumb_str.startswith("Womens>Pants")):
			
			table = sel.xpath('//li[@id="womens-bottoms-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""

		elif breadcrumb_str.startswith("Womens>Footwear"):
			table = sel.xpath('//li[@id="womens-shoes-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""

		elif breadcrumb_str.startswith("Womens>Accessories>Belts"):
			table = sel.xpath('//li[@id="womens-belt-tab"]/ul/li/table/node()').extract()
			return "".join(table) if table else ""

	def get_category(self, breadcrumb):
		breadcrumb_str = ">".join(breadcrumb)

		if breadcrumb_str.startswith("Womens"):
			return self.get_womens_category(breadcrumb_str)
		else:
			return self.get_mens_category(breadcrumb_str)

	def get_mens_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("Mens>Outerwear"):
			return "men_clothing_outerwears"
		elif breadcrumb_str.startswith("Mens>Sweaters"):
			return "men_clothing_sweaters"
		elif breadcrumb_str.startswith("Mens>Shirts"):
			return "men_clothing_shirts"
		elif breadcrumb_str.startswith("Mens>Tees & Henleys"):
			return "men_clothing_tshirts"
		elif breadcrumb_str.startswith("Mens>Graphic Tees"):
			return "men_clothing_tshirts"
		elif breadcrumb_str.startswith("Mens>Polos"):
			return "men_clothing_shirts"
		elif breadcrumb_str.startswith("Mens>Hoodies & Sweatshirts"):
			return "men_clothing_sweatshirts"
		elif breadcrumb_str.startswith("Mens>Jeans"):
			return "men_clothing_jeans"
		elif breadcrumb_str.startswith("Mens>Sweatpants"):
			return "men_clothing_pants"
		elif breadcrumb_str.startswith("Mens>Pants"):
			return "men_clothing_pants"
		elif breadcrumb_str.startswith("Mens>Shorts"):
			return "men_clothing_shorts"
		elif breadcrumb_str.startswith("Mens>Accessories>Caps"):
			return "men_accessories_hats"
		elif breadcrumb_str.startswith("Mens>Accessories>Socks"):
			return "men_accessories_socks"
		elif breadcrumb_str.startswith("Mens>Accessories>Bags"):
			return "men_bags"
		elif breadcrumb_str.startswith("Mens>Accessories>Tech"):
			return "men_accessories_tech"
		elif breadcrumb_str.startswith("Mens>Accessories>Phone Cases"):
			return "men_accessories_tech"
		elif breadcrumb_str.startswith("Mens>Accessories>Wallets"):
			return "men_accessories_wallets"
		elif breadcrumb_str.startswith("Mens>Accessories>Bracelets"):
			return "men_jewelry_bracelets"
		elif breadcrumb_str.startswith("Mens>Accessories>Belts"):
			return "men_accessories_belts"
		elif breadcrumb_str.startswith("Mens>Underwear"):
			return "men_clothing_underwear"
		else:
			return "men"



	def get_womens_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("Womens>Tops>Sleeveless"):
			return "women_tank_tops"
		elif breadcrumb_str.startswith("Womens>Tops>Short Sleeve"):
			return "women_short_sleeve"
		elif breadcrumb_str.startswith("Womens>Tops>Long Sleeve"):
			return "women_long_sleeve"
		elif breadcrumb_str.startswith("Womens>Dresses & Rompers>Skater"):
			return "women_dresses"
		elif breadcrumb_str.startswith("Womens>Dresses & Rompers>Babydoll"):
			return "women_dresses"
		elif breadcrumb_str.startswith("Womens>Dresses & Rompers>Maxi"):
			return "women_dresses"
		elif breadcrumb_str.startswith("Womens>Dresses & Rompers>Bodycon"):
			return "women_dresses"
		elif breadcrumb_str.startswith("Womens>Dresses & Rompers>Rompers & Jumpsuits"):
			return "women_jumpsuit"
		elif breadcrumb_str.startswith("Womens>Shirts"):
			return "women_shirts"
		elif breadcrumb_str.startswith("Womens>Graphic Tees"):
			return "women_short_sleeve"
		elif breadcrumb_str.startswith("Womens>Polos"):
			return "women_shirts"
		elif breadcrumb_str.startswith("Womens>Hoodies & Sweatshirts"):
			return "women_sweatshirts_hoodies"
		elif breadcrumb_str.startswith("Womens>Sweaters>Pullover"):
			return "women_sweaters_knits"
		elif breadcrumb_str.startswith("Womens>Sweaters>Cardigan"):
			return "women_cardigans"
		elif breadcrumb_str.startswith("Womens>Outerwear"):
			return "women_jackets"
		elif breadcrumb_str.startswith("Womens>Shorts>Denim"):
			return "women_shorts_jeans"
		elif breadcrumb_str.startswith("Womens>Shorts"):
			return "women_shorts"
		elif breadcrumb_str.startswith("Womens>Skirts>Midi"):
			return "women_midi_skirts"
		elif breadcrumb_str.startswith("Womens>Skirts>Maxi"):
			return "women_maxi_skirts"
		elif breadcrumb_str.startswith("Womens>Skirts>Skater"):
			return "women_mini_skirts"
		elif breadcrumb_str.startswith("Womens>Skirts>Bodycon"):
			return "women_mini_skirts"
		elif breadcrumb_str.startswith("Womens>Jeans>Boyfriend"):
			return "women_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Jean Legging"):
			return "women_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Super Skinny High Rise"):
			return "women_skinny_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Super Skinny"):
			return "women_skinny_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Natural Waist Jegging"):
			return "women_skinny_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Jegging"):
			return "women_skinny_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Skinny"):
			return "women_skinny_jeans"
		elif breadcrumb_str.startswith("Womens>Jeans>Boot"):
			return "women_bootcut_jeans"
		elif breadcrumb_str.startswith("Womens>Overalls"):
			return "women_jeans"
		elif breadcrumb_str.startswith("Womens>Sweatpants"):
			return "women_sweat_pants"
		elif breadcrumb_str.startswith("Womens>Leggings"):
			return "women_leggings"
		elif breadcrumb_str.startswith("Womens>Pants"):
			return "women_pants"
		elif breadcrumb_str.startswith("Womens>Footwear>Sandals"):
			return "women_sandals"
		elif breadcrumb_str.startswith("Womens>Footwear>Sneakers"):
			return "women_sneakers"
		elif breadcrumb_str.startswith("Womens>Accessories>Bags"):
			return "women_bags"
		elif breadcrumb_str.startswith("Womens>Accessories>Tech"):
			return "women_accessories_tech"
		elif breadcrumb_str.startswith("Womens>Accessories>Sunglasses"):
			return "women_accessories_eyewear"
		elif breadcrumb_str.startswith("Womens>Accessories>Jewelry"):
			return "women_jewelry"
		elif breadcrumb_str.startswith("Womens>Accessories>Hair"):
			return "women_accessories_hair"
		elif breadcrumb_str.startswith("Womens>Accessories>Socks & Tights"):
			return "women_accessories_hosiery"
		elif breadcrumb_str.startswith("Womens>Accessories>Scarves"):
			return "women_accessories_scarves"
		elif breadcrumb_str.startswith("Womens>Accessories>Caps"):
			return "women_accessories_hats"
		elif breadcrumb_str.startswith("Womens>Accessories>Belts"):
			return "women_accessories_belts"
		elif breadcrumb_str.startswith("Womens>Sleep"):
			return "women_loungewear"
		else:
			return "women"










	# def one_color(self, response):
	# 	sel = Selector(response)
	# 	title = sel.xpath('//h1[@class="name"]/text()').extract()
	# 	breadcrumb = sel.xpath('//div[@id="breadcrumb"]/a/text()').extract()
	# 	list_price = sel.xpath('//h4[@class="list-price"]/del/text()').extract()
	# 	color = sel.xpath('//div[@class="color-wrap"]/p/span[@class="color"]/text()').extract()
	# 	desc = sel.xpath('//p[@class="copy"]/text()').extract()
	# 	is_sale = "false"

	# 	size_table_html = sel.xpath('//li[@class="table"]/table/node()').extract()

	# 	price = {}
		
	# 	price['sale_color'] = {}

	# 	if list_price:
	# 		is_sale = "true"
	# 		price['sale_color'][color[0]] = sel.xpath('//h4[@class="offer-price"]/text()').extract()[0][1:]
	# 		price['original'] = list_price[0][1:]
	# 	else:
	# 		price['original'] = sel.xpath('//h4[@class="offer-price"]/text()').extract()[0][1:]
			

	# 	options = {}
	# 	options["color"] = color
	# 	options["size"] = map(lambda l: l.strip(), sel.xpath('//select[@class="size-select"]/option/text()').extract()[1:])

	# 	option_inventory = {}

	# 	# for color in options["color"]:
	# 	for size in options["size"]:
	# 		size = size.replace('.', '||')
	# 		option_inventory["color*_*" + color[0] + "-_-size*_*" + size] = "available"

	# 	image_prefix = sel.xpath('//div[@class="image-wrap"]/@data-color-img-prefix').extract()[0]
	# 	images = {}

	# 	prod_num = 1
	# 	model_num = 1
	# 	prod_state = 200
	# 	model_state = 200

	# 	for color in options["color"]:

	# 		image = []
	# 		while prod_state == 200:

	# 			url_str = StringIO()
	# 			url_str.write("http://anf.scene7.com/is/image/anf/anf_")
	# 			url_str.write(image_prefix)
	# 			url_str.write("_prod")
	# 			url_str.write(str(prod_num))
	# 			url_str.write("?$productMagnify-anf$")

	# 			url = url_str.getvalue()
	# 			prod_state = requests.get(url).status_code
	# 			prod_num += 1
	# 			if prod_state == 200:
	# 				image.append(url)

	# 		while model_state == 200:
	# 			url_str = StringIO()
	# 			url_str.write("http://anf.scene7.com/is/image/anf/anf_")
	# 			url_str.write(image_prefix)
	# 			url_str.write("_model")
	# 			url_str.write(str(model_num))
	# 			url_str.write("?$productMagnifyOFP-anf$")

	# 			url = url_str.getvalue()
	# 			model_state = requests.get(url).status_code
	# 			model_num += 1
	# 			if model_state == 200:
	# 				image.append(url)

	# 		images[color] = image


		

	# 	item = AbercrombieItem()

	# 	item['title'] = title[0].strip() if title else ""
	# 	item['sub_title'] = ""
	# 	item['url'] = response.url
	# 	item['domain'] = "abercrombie.com"
	# 	item['desc'] = desc[0] if desc else ""
	# 	item['currency'] = "USD"
	# 	item['price'] = price
	# 	item['options'] = options
	# 	item['images'] = images
	# 	item['is_done'] = "false"
	# 	item['brand'] = "Abercrombie"
	# 	item['breadcrumb'] = breadcrumb
	# 	item['category'] = ""
	# 	item['created_by'] = ""
	# 	item['county'] = "US"
	# 	item['inventory'] = option_inventory
	# 	item['is_sale'] = is_sale
	# 	item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
	# 	item['is_soldout'] = "false"

	# 	return item

		



