from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from rebeccataylor.items import RebeccataylorItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json

class RebeccataylorSpider(CrawlSpider):
	name = "rebeccataylor"
	allow_domains = ["rebeccataylor.com"]
	start_urls = [
		"http://www.rebeccataylor.com/shop/tops/icat/rttops",
		"http://www.rebeccataylor.com/shop/dresses/icat/rtdresses",
		"http://www.rebeccataylor.com/shop/jackets-and-coats/icat/rtjackets",
		"http://www.rebeccataylor.com/shop/sweaters/icat/rtsweaters",
		"http://www.rebeccataylor.com/shop/bottoms/icat/rtbottoms",
		"http://www.rebeccataylor.com/shop/accessories-/icat/rtaccessories",

		# "http://www.rebeccataylor.com/sale/silk-knit-combo-top/invt/rt314630b158&bklist=icat,4,shop,rtshop,rttops",
		# "http://www.rebeccataylor.com/tops/long-sleeve-combo-top/invt/rt414708b181&bklist=icat,4,shop,rtshop,rttops"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="image"]/a')), callback="parse_item" ),
  ]

	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="invtname"]/text()').extract()[0]
		desc = sel.xpath('//div[@class="data active"]/node()').extract()
		desc = "".join(desc)
		breadcrumb = sel.xpath('//div[@id="breadcrumbs"]/a/text()').extract()
		was_price = sel.xpath('//span[@id="attr-wasprice"]/text()').extract()
		sell_price = sel.xpath('//span[@id="attr-sellprice"]/text()').extract()

		is_sale = "false"


		if was_price:
			is_sale = "true"
			original_price = was_price[0][1:]
		else:
			original_price = sell_price[0][1:]

		size_table_html = """
			<tbody><tr><td class="lst" rowspan="2"><br><br>USA &amp; Canada</td><th>XS</th><th colspan="2">S</th><th colspan="2">M</th><th colspan="2">L</th></tr>
			<tr><th>0</th><th class="fill">2</th><th>4</th><th class="fill">6</th><th>8</th><th class="fill">10</th><th>12</th></tr>
			<tr><td class="fill lst">Europe</td><td>30</td><td class="fill">32</td><td>34</td><td class="fill">36</td><td>38</td><td class="fill">40</td><td>42</td></tr>
			<tr><td class="fill lst">UK</td><td>2</td><td class="fill">4</td><td>6</td><td class="fill">8</td><td>10</td><td class="fill">12</td><td>14</td></tr>
			<tr><td class="fill lst">Australia</td><td>4</td><td class="fill">6</td><td>8</td><td class="fill">10</td><td>12</td><td class="fill">14</td><td>16</td></tr>
			<tr><td class="fill lst">Japan</td><td>3</td><td class="fill">5</td><td>7</td><td class="fill">9</td><td>11</td><td class="fill">13</td><td>15</td></tr>
			<tr><td class="fill lst">Italy &amp; Russia</td><td>34</td><td class="fill">36</td><td>38</td><td class="fill">40</td><td>42</td><td class="fill">44</td><td>46</td></tr>
			<tr><td class="fill lst">France</td><td>32</td><td class="fill">34</td><td>36</td><td class="fill">38</td><td>40</td><td class="fill">42</td><td>44</td></tr>
			</tbody>"""

	

		options = {}
		option_inventory = {}
		images = {}
		

		price = {}
		price['original'] = original_price
		price['sale_color'] = {}


		javascript = sel.xpath('//script[@type="text/javascript"]').extract()
		image_javascript = javascript[46].split('Venda.Ebiz.ProductDetail.loadImage("')

		javascript = javascript[45].split('product.setAttributeData(')
		



		temp_color = []
		temp_size = []
		for i in range(1, len(javascript)):
			color = javascript[i].split('{"att1":"')[1].split('",')[0]
			size = javascript[i].split('"att2":"')[1].split('",')[0]
			temp_color.append(color)
			temp_size.append(size)

			sell_price = javascript[1].split('"atrsell":"')[1].split('",')[0]

			if is_sale == "true":
				price['sale_color'][color] = sell_price

			option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "available"

		options['color'] = list(set(temp_color))
		options['size'] = list(set(temp_size))

		for i in range(1, len(image_javascript)):
			image_arr = []
			color = image_javascript[i].split('",{')[0]
			for j in image_javascript[i].split('setxlsideview: [')[1].split(']')[0].split(','):
				image_arr.append(j.replace('"', ''))

			image_arr.pop(0)
			image_arr[len(image_arr)-1], image_arr[0] = image_arr[0], image_arr[len(image_arr)-1]
			images[color] = image_arr
		
		category = self.get_category(breadcrumb, title)

		if not category:
			return
	
		item = RebeccataylorItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "rebeccataylor.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "REBECCA TAYLOR"
		item['breadcrumb'] = breadcrumb
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

	def get_category(self, breadcrumb, title):
		if 'tops' in breadcrumb:
			return 'women_tops'
		elif 'dresses' in breadcrumb:
			return 'women_dresses'
		elif 'jackets and coats' in breadcrumb:
			return 'women_clothing_outerwears'
		elif 'sweaters' in breadcrumb:
			return 'women_sweaters_knits'
		elif 'bottoms' in breadcrumb:
			if 'Jumpsuit' in title:
				return 'women_jumpsuit'
			elif 'Skirt' in title:
				return 'women_skirts'
			elif 'Pant' in title:
				return 'women_pants'
			elif 'Jean' in title or 'Jeans' in title:
				return 'women_jeans'
		elif 'accessories ' in breadcrumb:
			return 'women_accessories'


				


