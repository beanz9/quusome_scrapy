#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from clubmonaco.items import ClubmonacoItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json

class ClubmonacoSpider(CrawlSpider):
	name = "clubmonaco"
	allow_domains = ["clubmonaco.com"]
	start_urls = [
		# "http://www.clubmonaco.com/product/index.jsp?productId=35057976"


		"http://www.clubmonaco.com/family/index.jsp?categoryId=16266406&size=99&cp=12243590.12266442.16266406&ab=ln_women_clothing_coatsandtrenches",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=16293766&size=99&cp=12243590.12266442.16293766&ab=ln_women_clothing_jacketsandvests",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454411&size=99&cp=12243590.12266330.12454411&ab=ln_women_shops_dressshop",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454410&size=99&cp=12243590.12266442.12454410&ab=ln_women_clothing_sweaters",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454412&size=99&cp=12243590.12266442.12454412&ab=ln_women_clothing_skirts",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454408&size=99&cp=12243590.12266442.12454408&ab=ln_women_clothing_knitsandtees",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454409&size=99&cp=12243590.12266442.12454409&ab=ln_women_clothing_shirts",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12754144&size=99&cp=12243590.12266442.12754144&ab=ln_women_clothing_pants",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=20384176&size=99&cp=12243590.12266442.20384176&ab=ln_women_clothing_jeans",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=34345236&size=99&cp=12243590.12266442.34345236&ab=ln_women_clothing_jumpers",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=24996716&size=99&cp=12243590.12266442.24996716&ab=ln_women_clothing_shorts",
		
		#구두
		"http://www.clubmonaco.com/family/index.jsp?categoryId=23254596&size=99&cp=12243590.12266330.23254596&ab=ln_women_shops_shoeshop"

		#스카프
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454418&size=99&cp=12243590.12266445.12454418&ab=ln_women_accessories_scarvesandwraps"

		#모자
		"http://www.clubmonaco.com/family/index.jsp?categoryId=16266436&size=99&cp=12243590.12266445.16266436&ab=ln_women_accessories_hats",

		#장갑
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454785&size=99&cp=12243590.12266445.12454785&ab=ln_women_accessories_gloves",

		#주얼리
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454417&size=99"

		#핸드백
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454738&size=99&cp=12243590.12266445.12454738&ab=ln_women_accessories_handbags"
		
		#벨트
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454740&size=99&cp=12243590.12266445.12454740&ab=ln_women_accessories_belts",

		#선글라스
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454419&size=99&cp=12243590.12266445.12454419&ab=ln_women_accessories_sunglasses",

		#남자
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454393&size=99&cp=12243591.12280933.12454393&ab=ln_men_clothing_outerwear",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454392&size=99&cp=12243591.12280933.12454392&ab=ln_men_clothing_sweaters",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454432&size=99&cp=12243591.12280933.12454432&ab=ln_men_clothing_casualshirts",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454431&size=99&cp=12243591.12280933.12454431&ab=ln_men_clothing_dressshirts",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454397&size=99&cp=12243591.12280933.12454397&ab=ln_men_clothing_pants",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=16247806&size=99&cp=12243591.12280933.16247806&ab=ln_men_clothing_suits",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454394&size=99&cp=12243591.12280933.12454394&ab=ln_men_clothing_blazers",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=19748236&size=99&cp=12243591.12280933.19748236&ab=ln_men_clothing_teesandpoloshirts",
		"http://www.clubmonaco.com/family/index.jsp?categoryId=18731206&size=99&cp=12243591.12280933.18731206&ab=ln_men_clothing_shorts",

		#구두
		"http://www.clubmonaco.com/family/index.jsp?categoryId=23240636&size=99&cp=12243591.12280931.23240636&ab=ln_men_shops_shoeshop",
		#모자,장갑,스카프
		"http://www.clubmonaco.com/family/index.jsp?categoryId=16247896&size=99&cp=12243591.12280936.16247896&ab=ln_men_accessories_hats,glovesandscarves",
		#벨트
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454546&size=99&cp=12243591.12280936.12454546&ab=ln_men_accessories_belts",
		#선글라스
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454399&size=99&cp=12243591.12280936.12454399&ab=ln_men_accessories_sunglasses",
		#넥타이
		"http://www.clubmonaco.com/family/index.jsp?categoryId=16247876&size=99",
		#가방
		"http://www.clubmonaco.com/family/index.jsp?categoryId=12454549&size=99&cp=12243591.12280936.12454549&ab=ln_men_accessories_bags"
	

	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="photo"]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="enabled next"]')), follow=True ),
  	
  ]


	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="product-title fn"]/text()').extract()[0]
		desc = "".join(sel.xpath('//div[@id="tab-details"]/node()').extract())
		breadcrumb = list(set(sel.xpath('//ul[@class="breadcrumbs page-path"]/li/a/text()').extract()))
		base_price = sel.xpath('//div[@class="money"]/span/text()').extract()

		is_sale = 'false'

		options = {}
		option_inventory = {}
		images = {}

		price = {}
		price['sale_color'] = {}

		script = sel.xpath('//script[@type="text/javascript"]').extract()[29]
		inven_script = script.split('sku_id:')

		temp_size = script.split('ess.sizes = {')[1].split('};')[0].split(',')
		temp_size_arr = []
		for s in temp_size:
			temp_size_arr.append(s.split(':')[0].replace('"', '').replace('.', '||'))

		temp_color = script.split("color: '")
		temp_color_arr = []
		for c in range(1, len(temp_color)):
			temp_color_arr.append(temp_color[c].split("',")[0])

		options['color'] = temp_color_arr
		options['size'] = temp_size_arr

		for i in range(1, len(inven_script)):
			size = inven_script[i].split('availShipEstMessage:')[0].split('size: "')[1].split('",')[0]
			color = inven_script[i].split('availShipEstMessage:')[0].split('color: "')[1].split('",')[0]
			avail = inven_script[i].split('availShipEstMessage:')[0].split('avail: "')[1].split('",')[0]

			if avail == 'IN_STOCK':
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "available"
			else:
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "unavailable"

			base_price = inven_script[i].split('availShipEstMessage:')[0].split('base: "$')[1].split('",')[0]
			current_price = inven_script[i].split('availShipEstMessage:')[0].split('current: "$')[1].split('",')[0]

			price['original'] = base_price
			

			if base_price != current_price:
				price['sale_color'][color] = current_price
				is_sale = 'true'



		image_script = script.split('availableSkuIds: new Array(')
		first_image_arr = []

		for i in range(1, len(image_script)):
			color = image_script[i].split(');')[0].split('colorName: "')[1].split('",')[0]
			image_script2 = image_script[i].split(');')[0].split('zoomImageURL: "')
			
			image_arr = []
			for j in range(1, len(image_script2)):
				image_str = image_script2[j].split('",')[0]
				json_url = 'http://s7d2.scene7.com/is/image/ClubMonacoGSI/' + image_str +'?req=set,json&scl=1&$flyout_main$&'
				res = requests.get(json_url).text
				iv = res.split('"iv":"')[1].split('"}')[0]

				
				url = 'http://s7d2.scene7.com/is/image/ClubMonacoGSI/' + image_str + '?$flyout_main$&iv=' + iv + '&wid=1650&hei=1650&fit=fit,1'
				image_arr.append(url)

			if i == 1:
				first_image_arr = image_arr
			else:
				for j in range(0, len(first_image_arr)):
					if j > 0:
						image_arr.append(first_image_arr[j])

			images[color] = image_arr


		if 'Men' in breadcrumb:
			size_table_html = '<img height="1358" width="960" src="http://www.clubmonaco.com/graphics/info/shared/CMUS_MensSizeChart_Update042513.jpg">'
		else:
			size_table_html = '<img height="1199" width="960" src="http://www.clubmonaco.com/graphics/info/shared/CMUS_WomensSizeChart_Update042513.jpg">'

		category = self.get_category(breadcrumb)


		if not category:
			return

		item = ClubmonacoItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "clubmonaco.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "CLUB MONACO"
		item['breadcrumb'] = breadcrumb
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item		

	def get_category(self, breadcrumb):
		if 'Men' in breadcrumb:
			return self.get_men_category(breadcrumb)
		else:
			return self.get_women_category(breadcrumb)

	def get_men_category(self, breadcrumb):
		if 'Outerwear' in breadcrumb:
			return 'men_clothing_outerwears'
		elif 'Sweaters' in breadcrumb:
			return 'men_clothing_sweaters'
		elif 'Casual Shirts' in breadcrumb:
			return 'men_clothing_shirts'
		elif 'Dress Shirts' in breadcrumb:
			return 'men_clothing_shirts'
		elif 'Pants' in breadcrumb:
			return 'men_clothing_pants'
		elif 'Suits' in breadcrumb:
			return 'men_clothing_suits'
		elif 'Blazers' in breadcrumb:
			return 'men_clothing_jackets'
		elif 'Tees and Polo Shirts' in breadcrumb:
			return 'men_clothing_tops'
		elif 'Shorts' in breadcrumb:
			return 'men_clothing_shorts'
		elif 'Sneakers' in breadcrumb:
			return 'men_shoes_sneakers'
		elif 'Dress Shoes' in breadcrumb:
			return 'men_shoes_raceups'
		elif 'Loafers' in breadcrumb:
			return 'men_shoes_loafers'
		elif 'Boots' in breadcrumb:
			return 'men_shoes_boots'
		elif 'Sandals' in breadcrumb:
			return 'men_shoes_sandals'
		elif 'Hats' in breadcrumb:
			return 'men_accessories_hats'
		elif 'Gloves' in breadcrumb:
			return 'men_accessories_gloves'
		elif 'Scarves' in breadcrumb:
			return 'men_accessories_scarves'
		elif 'Ties' in breadcrumb:
			return 'men_accessories_ties'
		elif 'Bags' in breadcrumb:
			return 'men_bags'
		elif 'Sunglasses' in breadcrumb:
			return 'men_accessories_eyewears'


	def get_women_category(self, breadcrumb):
		if 'Coats and Trenches' in breadcrumb:
			return 'women_coats'
		elif 'Jackets and Vests' in breadcrumb:
			return 'women_jackets'
		elif 'The Dress Shop' in breadcrumb:
			return 'women_dresses'
		elif 'Sweaters' in breadcrumb:
			return 'women_sweaters_knits'
		elif 'Skirts' in breadcrumb:
			return 'women_skirts'
		elif 'Knits and Tees' in breadcrumb:
			return 'women_sweaters_knits'
		elif 'Shirts' in breadcrumb:
			return 'women_tops'
		elif 'Pants' in breadcrumb:
			return 'women_pants'
		elif 'Jeans' in breadcrumb:
			return 'women_jeans'
		elif 'Jumpers' in breadcrumb:
			return 'women_jumpsuit'
		elif 'Shorts' in breadcrumb:
			return 'women_shorts'
		elif 'Sandals' in breadcrumb:
			return 'women_sandals'
		elif 'Heels' in breadcrumb:
			return 'women_pumps'
		elif 'Flats' in breadcrumb:
			return 'women_flats'
		elif 'Boots' in breadcrumb:
			return 'women_boots_booties'
		elif 'Scarves and Wraps' in breadcrumb:
			return 'women_accessories_scarves'
		elif 'Hats' in breadcrumb:
			return 'women_accessories_hats'
		elif 'Gloves' in breadcrumb:
			return 'women_accessories_gloves'
		elif 'Necklaces' in breadcrumb:
			return 'women_jewelry_necklaces'
		elif 'Bracelets' in breadcrumb:
			return 'women_jewelry_bracelets'
		elif 'Earrings' in breadcrumb:
			return 'women_jewelry_earrings'
		elif 'Rings' in breadcrumb:
			return 'women_jewelry_rings'
		elif 'Handbags' in breadcrumb:
			return 'women_bags'
		elif 'Belts' in breadcrumb:
			return 'women_accessories_belts'
		elif 'Sunglasses' in breadcrumb:
			return 'women_accessories_eyewear'
