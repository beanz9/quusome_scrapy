# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class AbercrombieItem(Item):
    # define the fields for your item here like:
    title = Field()
    sub_title = Field()
    url = Field()
    domain = Field()
    desc = Field()
    currency = Field()
    price = Field()
    options = Field()
    images = Field()
    is_done = Field()
    brand = Field()
    breadcrumb = Field()
    category = Field()
    created_by = Field()
    county = Field()
    inventory_enable = Field()
    is_soldout = Field()
