from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from abercrombie.items import AbercrombieItem
from cStringIO import StringIO
from scrapy.exceptions import DropItem
import requests

class AbercrombieSpider(CrawlSpider):
	name = "abercrombie"
	allowed_domains = ["abercrombie.com"]
	# splash_url = "http://localhost:8050/render.html?url="
	start_urls = [
		"http://www.abercrombie.com/shop/wd/womens-sleeveless-fashion-tops/hadley-tank-2804615_01"
	]

	def parse(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="name"]/text()').extract()
		print title

		desc = sel.xpath('//h2[@class="copy"]/text()').extract()
		print desc