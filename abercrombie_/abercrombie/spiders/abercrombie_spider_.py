from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from abercrombie.items import AbercrombieItem
from cStringIO import StringIO
from scrapy.exceptions import DropItem
import requests

class AbercrombieSpider(CrawlSpider):
	name = "abercrombie"
	allowed_domains = ["abercrombie.com"]
	# splash_url = "http://localhost:8050/render.html?url="
	start_urls = [
		# "http://localhost:8050/render.html?url=http://www.abercrombie.com/shop/wd/mens-super-skinny-jeans/a-and-f-super-skinny-jeans-2103069_01"
		
		"http://www.abercrombie.com/shop/wd/mens-shirts",
		"http://www.abercrombie.com/shop/wd/mens-hoodies-and-sweatshirts",
		"http://www.abercrombie.com/shop/wd/mens-sweaters",
		"http://www.abercrombie.com/shop/wd/mens-graphic-tees",
		"http://www.abercrombie.com/shop/wd/mens-tees",
		"http://www.abercrombie.com/shop/wd/mens-polos",
		"http://www.abercrombie.com/shop/wd/mens-outerwear",
		"http://www.abercrombie.com/shop/wd/mens-jeans",
		"http://www.abercrombie.com/shop/wd/mens-sweatpants",
		"http://www.abercrombie.com/shop/wd/mens-pants",
		"http://www.abercrombie.com/shop/wd/mens-shorts",
		"http://www.abercrombie.com/shop/wd/mens-a-and-f-active",
		"http://www.abercrombie.com/shop/wd/mens-swim",
		"http://www.abercrombie.com/shop/wd/mens-flip-flops",
		"http://www.abercrombie.com/shop/wd/mens-accessories",
		"http://www.abercrombie.com/shop/wd/mens-underwear",
		"http://www.abercrombie.com/shop/wd/womens-sweaters",
		"http://www.abercrombie.com/shop/wd/womens-hoodies-and-sweatshirts",
		"http://www.abercrombie.com/shop/wd/womens-shirts",
		"http://www.abercrombie.com/shop/wd/womens-fashion-tops",
		"http://www.abercrombie.com/shop/wd/womens-graphic-tees",
		"http://www.abercrombie.com/shop/wd/womens-polos",
		"http://www.abercrombie.com/shop/wd/womens-outerwear",
		"http://www.abercrombie.com/shop/wd/womens-jeans",
		"http://www.abercrombie.com/shop/wd/womens-sweatpants",
		"http://www.abercrombie.com/shop/wd/womens-leggings",
		"http://www.abercrombie.com/shop/wd/womens-pants",
		"http://www.abercrombie.com/shop/wd/womens-shorts",
		"http://www.abercrombie.com/shop/wd/womens-skirts",
		"http://www.abercrombie.com/shop/wd/womens-dresses",
		"http://www.abercrombie.com/shop/wd/womens-a-and-f-active",
		"http://www.abercrombie.com/shop/wd/womens-swim",
		"http://www.abercrombie.com/shop/wd/womens-flip-flops",
		"http://www.abercrombie.com/shop/wd/womens-accessories",
		# "http://www.abercrombie.com/shop/wd/womens-sleep",
		"http://www.abercrombie.com/shop/wd/womens-bras-and-undies",

	]

	# rules = (
	# 	Rule(SgmlLinkExtractor(allow=("shop/wd/mens-",), callback='parse_test', follow=True),
	# )
	
	allow_list = [
		"shop/wd/mens-(.*)",
		"shop/wd/womens-(.*)",
		# "shop/wd/mens-(.*)/(.*)_(.*)",
		# "shop/wd/womens-(.*)/(.*)_(.*)",
	]

	deny_list = [
		"shop/wd/(.*)/CustomerService(.*)",
		# "shop/wd/mens-(.*)/(.*)",
		# "shop/wd/womens-(.*)/(.*)",
		".*?editItem=true",
		"shop/wd/p/(.*)",
		"shop/wd/womens-fragrance",
		"shop/wd/womens-a-and-f-looks",
		"shop/wd/womens-new-arrivals",
		"shop/wd/womens-a-and-f-the-making-of-a-star",
		"shop/wd/womens-denim-collection",
		"shop/wd/womens-soft-and-pretty",
		"shop/wd/womens-effortless-90s-style",
		"shop/wd/womens-the-prettiest-lace",
		"shop/wd/womens-essential-spring-layers",
		"shop/wd/womens-flagship-exclusives",
		"shop/wd/womens-online-exclusives",
		"shop/wd/womens-what-we-love",
		"shop/wd/womens-featured-items-sale(.*)",
		"shop/wd/hidden-gift-cards",
		"shop/wd/mens-body-care",
		"shop/wd/mens-cologne",
		"shop/wd/mens-a-and-f-looks",
		"shop/wd/mens-new-arrivals",
		"shop/wd/mens-a-and-f-the-making-of-a-star",
		"shop/wd/mens-the-hottest-blues",
		"shop/wd/mens-vintage-texture-and-wash",
		"shop/wd/mens-spring-break-layers",
		"shop/wd/mens-casual-cool",
		"shop/wd/mens-flagship-exclusives",
		"shop/wd/mens-online-exclusives",
		"shop/wd/mens-get-fierce",
		"shop/wd/mens-what-we-love",
		"shop/wd/mens-featured-items-sale(.*)",

		"shop/wd/womens-featured-items-clearance(.*)",
		"shop/wd/mens-featured-items-clearance(.*)"
	]
	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="image-wrap"]', '//a[@class="swatch-link"]'), allow=(allow_list), deny=(deny_list) ), callback="parse_item", follow=True, ),
		# Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="swatch-link"]'), allow=(allow_list), deny=(deny_list) ), callback="parse_item", follow=True, ),
  ]



	# def process_request_test(request, spider):
	# 	print request.url
		# splash_url = "http://localhost:8050/render.html?url="
		# rt = request.replace(url=splash_url + request.url)
		
		# print rt.url

	# def process_request(self, request, spider):
	# 	print ">>>>>"
 #    # original_url = request.url
 #    # m_url = 'http://whatsmyuseragent.com/'
 #    # request.url = m_url

	def parse_test(self, response):
		sel = Selector(response)
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		print price
		print "!!!"

	# def process_links_test(self, links):
	# 	for link in links:
	# 		link.url = self.splash_url + link.url

	# 	return links


	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="name"]/text()').extract()
		desc = sel.xpath('//h2[@class="copy"]/text()').extract()
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		befor_price = sel.xpath('//div[@class="list-price redline"]/del/text()').extract()
		if not price:
			return

		options = sel.xpath('//li[@class="size select required"]/div[@class="dk_container dk_theme_dark_gray"]/div[@class="dk_options"]/ul[@class="dk_options_inner"]/li[not(@class="dk_option_current")]/a/text()').extract()
		sub_title = sel.xpath('//div[@class="swatches hidden"]/ul/li[@class="color"]/text()').extract()
		# thumbnail_images = sel.xpath('//div[@class="image-util product-view"]/ul/li/@data-image-name').extract()
		seed_image = sel.xpath('//span[@class="zoom prod-img"]/img[@class="prod-img"]/@src').extract()[0]
		breadcrumb = sel.xpath('//div[@id="breadcrumb"]/a/text()').extract()

		print "=======breadcrumb========"
		print breadcrumb
		prod_state = 200
		model_state = 200
		prod_num = 1
		model_num = 1
		images = []

		

		while prod_state == 200:
			# r = requests.get("http:" + seed_image + )
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("1?")[0])
			url_str.write(str(prod_num))
			url_str.write("?$anfProductImageMagnify$")

			r = requests.get(url_str.getvalue())
			prod_state = r.status_code
			prod_num = prod_num + 1
			if prod_state == 200:
				images.append(url_str.getvalue())

		while model_state == 200:
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("prod1?")[0])
			url_str.write("model")
			url_str.write(str(model_num))
			url_str.write("?$anfProductImageModMagnify$")

			r = requests.get(url_str.getvalue())
			model_state = r.status_code
			model_num = model_num + 1
			if model_state == 200:
				images.append(url_str.getvalue())
		# http://anf.scene7.com/is/image/anf/anf_72487_03_model1?$anfProductImageModMagnify$

		options_list = {}
		options_list['size'] = (map(lambda option: option.strip(), options))

		item = AbercrombieItem()

		item['title'] = title[0] if title else ""
		item['sub_title'] = sub_title[0] if sub_title else ""
		item['url'] = response.url 
		item['domain'] = "abercrombie.com"
		item['desc'] = desc[0] if desc else ""
		item['currency'] = "USD"
		item['price'] = price[1:] if price else ""
		item['options'] = options_list
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "abercrombie"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = ""
		# item['is_soldout'] = "false"


		return item

	def get_category(self, breadcrumb):
		return "20"

		

