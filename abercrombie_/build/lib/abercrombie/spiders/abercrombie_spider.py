from scrapy.spider import Spider
from scrapy.selector import Selector
from abercrombie.items import AbercrombieItem
from cStringIO import StringIO
import requests

class AbercrombieSpider(Spider):
	name = "abercrombie"
	allowed_domains = ["abercrombie.com"]

	start_urls = [
		"http://localhost:8050/render.html?url=http://www.abercrombie.com/shop/wd/mens-super-skinny-jeans/a-and-f-super-skinny-jeans-2103069_01"
	]

	def parse(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="name"]/text()').extract()
		desc = sel.xpath('//h2[@class="copy"]/text()').extract()
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		options = sel.xpath('//div[@class="dk_container dk_theme_size"]/div[@class="dk_options"]/ul[@class="dk_options_inner"]/li[not(@class="dk_option_current")]/a/text()').extract()
		sub_title = sel.xpath('//div[@class="swatches hidden"]/ul/li[@class="color"]/text()').extract()
		# thumbnail_images = sel.xpath('//div[@class="image-util product-view"]/ul/li/@data-image-name').extract()
		seed_image = sel.xpath('//span[@class="zoom prod-img"]/img[not(@class="prod-img")]/@src').extract()[0]
		
		prod_state = 200
		model_state = 200
		prod_num = 1
		model_num = 1
		images = []

		

		while prod_state == 200:
			# r = requests.get("http:" + seed_image + )
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("1?")[0])
			url_str.write(str(prod_num))
			url_str.write("?$anfProductImageMagnify$")

			r = requests.get(url_str.getvalue())
			prod_state = r.status_code
			prod_num = prod_num + 1
			if prod_state == 200:
				images.append(url_str.getvalue())

		while model_state == 200:
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("prod1?")[0])
			url_str.write("model")
			url_str.write(str(model_num))
			url_str.write("?$anfProductImageModMagnify$")

			r = requests.get(url_str.getvalue())
			model_state = r.status_code
			model_num = model_num + 1
			if model_state == 200:
				images.append(url_str.getvalue())
		# http://anf.scene7.com/is/image/anf/anf_72487_03_model1?$anfProductImageModMagnify$


		item = AbercrombieItem()

		item['title'] = title[0] if title else ""
		item['sub_title'] = sub_title[0] if sub_title else ""
		item['url'] = response.url.split("url=")[1]
		item['domain'] = "abercrombie.com"
		item['desc'] = desc
		item['currency'] = price[0][0]
		item['price'] = price[0][1:]
		item['options'] = map(lambda option: option.strip(), options)
		item['images'] = images
		# item['image_urls'] = ["http:" + x for x in images]

		return item
		

