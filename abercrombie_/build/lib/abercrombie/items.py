# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class AbercrombieItem(Item):
    # define the fields for your item here like:
    title = Field()
    sub_title = Field()
    url = Field()
    domain = Field()
    desc = Field()
    currency = Field()
    price = Field()
    options = Field()
    # product_images = Field()
    # image_urls = Field()
    images = Field()
