# Scrapy settings for abercrombie project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'abercrombie'

SPIDER_MODULES = ['abercrombie.spiders']
NEWSPIDER_MODULE = 'abercrombie.spiders'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'abercrombie (+http://www.yourdomain.com)'

# ITEM_PIPELINES = {'scrapy.contrib.pipeline.images.ImagesPipeline': 1}
# IMAGES_STORE = '/Users/Lee/projects/quusome_scrapy/abercrombie/images'
