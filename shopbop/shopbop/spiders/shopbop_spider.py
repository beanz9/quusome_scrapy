#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from shopbop.items import ShopbopItem
from cStringIO import StringIO
import re
import json
import requests
import collections

class ShopbopSpider(CrawlSpider):
	name = "shopbop"
	allow_domains = ["shopbop.com"]
	start_urls = [
		# "http://www.shopbop.com/cami-tibi/vp/v=1/1544436106.htm?folderID=2534374302060705&fm=other-shopbysize&colorId=13149",
		# "http://www.shopbop.com/koemi-top-theyskens-theory/vp/v=1/1529452048.htm?folderID=2534374302060705&fm=other-shopbysize&colorId=12867"
		# "http://www.shopbop.com/luxe-sarinda-blouse-theory/vp/v=1/1518824853.htm?folderID=2534374302060705&fm=other-shopbysize&colorId=12397"
		# "http://www.shopbop.com/rudy-tank-bb-dakota/vp/v=1/1509915894.htm?folderID=2534374302060705&fm=other-shopbysize&colorId=58394"
		# "http://www.shopbop.com/sports-active-racerback-tank-mesh/vp/v=1/1500993603.htm?folderID=2534374302060705&fm=other-shopbysize&colorId=11667"
		# "http://www.shopbop.com/hammond-shorts-elizabeth-james/vp/v=1/1503161050.htm?folderID=2534374302024684&fm=other-shopbysize&colorId=12867"
		# "http://www.shopbop.com/astoria-blouse-just-female/vp/v=1/1556602611.htm?folderID=2534374302060711&fm=other-shopbysize&colorId=10991"
		
		#--jean--#
		#Boot Cut Jeans
		"http://www.shopbop.com/clothing-denim-boot-cut-jeans/br/v=1/2534374302064887.htm?skipMobile=true",
		#Cropped Jeans
		"http://www.shopbop.com/clothing-denim-cropped-jeans/br/v=1/2534374302064889.htm?skipMobile=true",
		#High Waisted Jeans
		"http://www.shopbop.com/clothes-denim-high-waisted-jeans/br/v=1/2534374302064882.htm?skipMobile=true",
		#Flare Jeans / Wide Leg
		"http://www.shopbop.com/clothing-denim-flare-jeans-wide-leg/br/v=1/2534374302064886.htm?skipMobile=true",
		#Maternity Jeans
		"http://www.shopbop.com/clothes-denim-maternity-jeans/br/v=1/2534374302080360.htm?skipMobile=true",
		#Skinny Jeans
		"http://www.shopbop.com/clothes-denim-skinny-jeans/br/v=1/2534374302064885.htm?skipMobile=true",
		#Straight Leg Jeans
		"http://www.shopbop.com/clothes-denim-straight-leg-jeans/br/v=1/2534374302064884.htm?skipMobile=true",
		# #Coated Jeans
		# "http://www.shopbop.com/clothing-denim-coated-jeans/br/v=1/2534374302204237.htm",
		# #Colored Jeans
		# "http://www.shopbop.com/clothing-denim-colored-jeans/br/v=1/2534374302204241.htm",
		# #Printed Jeans
		# "http://www.shopbop.com/clothing-denim-printed-jeans/br/v=1/2534374302204239.htm",
		#Shorts
		"http://www.shopbop.com/clothing-denim-shorts/br/v=1/2534374302073699.htm?skipMobile=true",

		#--coats--#
		"http://www.shopbop.com/clothing-jackets-coats/br/v=1/2534374302196584.htm?skipMobile=true",
		#Trench Coats
		"http://www.shopbop.com/clothing-jackets-coats-trench/br/v=1/2534374302196583.htm?skipMobile=true",
		#Pea Coats
		"http://www.shopbop.com/clothing-jackets-coats-pea/br/v=1/2534374302196586.htm?skipMobile=true",

		#--jackets--#
		#Blazers
		"http://www.shopbop.com/clothing-jackets-coats-blazers/br/v=1/2534374302066388.htm?skipMobile=true",
		#denim
		"http://www.shopbop.com/clothing-jackets-coats-denim/br/v=1/2534374302066387.htm?skipMobile=true",
		#Fashion
		"http://www.shopbop.com/clothing-jackets-coats-fashion/br/v=1/2534374302066386.htm?skipMobile=true",
		#Leather
		"http://www.shopbop.com/jackets-leather/br/v=1/2534374302101674.htm?skipMobile=true",
		#Puffer Jackets
		"http://www.shopbop.com/clothing-jackets-coats-puffer/br/v=1/2534374302196587.htm?skipMobile=true",

		#--jumpsuit--#
		"http://www.shopbop.com/clothing-jumpsuits-rompers/br/v=1/2534374302034081.htm?skipMobile=true",

		#--Loungewear / Yoga--#
		"http://www.shopbop.com/clothing-loungewear/br/v=1/2534374302053051.htm?skipMobile=true",

		#--Maternity--#
		"http://www.shopbop.com/clothing-maternity/br/v=1/2534374302172178.htm?skipMobile=true",

		#--pants--#
		#Boot Cut / Flare
		"http://www.shopbop.com/clothing-pants-boot-cut-flare/br/v=1/2534374302180154.htm?skipMobile=true",
		#Cropped
		"http://www.shopbop.com/clothing-pants-cropped/br/v=1/2534374302070476.htm?skipMobile=true",
		#High Waisted
		"http://www.shopbop.com/clothing-pants-high-waisted/br/v=1/2534374302196585.htm?skipMobile=true",
		#Leggings
		"http://www.shopbop.com/clothing-pants-leggings/br/v=1/13287.htm?skipMobile=true",
		#skinny
		"http://www.shopbop.com/clothing-pants-skinny/br/v=1/2534374302067314.htm?skipMobile=true",
		#Slouchy Trousers
		"http://www.shopbop.com/clothes-pants-slouchy-trousers/br/v=1/2534374302159448.htm?skipMobile=true",
		#Straight Leg
		"http://www.shopbop.com/clothing-pants-straight-leg/br/v=1/2534374302180155.htm?skipMobile=true",
		#Sweatpants / Yoga
		"http://www.shopbop.com/clothing-pants-sweatpants-yoga/br/v=1/2534374302180202.htm?skipMobile=true",
		#Wide Leg
		"http://www.shopbop.com/clothing-pants-wide-leg/br/v=1/2534374302067313.htm?skipMobile=true",

		#--short--#
		"http://www.shopbop.com/clothing-shorts/br/v=1/2534374302024684.htm?skipMobile=true",

		#--Skirts--#
		#Mini
		"http://www.shopbop.com/skirts-mini/br/v=1/2534374302149758.htm?skipMobile=true",
		#Knee Length
		"http://www.shopbop.com/clothing-skirts-knee-length/br/v=1/2534374302070727.htm?skipMobile=true",
		#Midi
		"http://www.shopbop.com/clothing-skirts-midi/br/v=1/2534374302182060.htm?skipMobile=true",
		#Maxi
		"http://www.shopbop.com/skirts-maxi/br/v=1/2534374302150097.htm?skipMobile=true",
		#Denim
		"http://www.shopbop.com/clothing-skirts-denim/br/v=1/2534374302079171.htm?skipMobile=true",

		#--Suit Separates--#
		"http://www.shopbop.com/clothing-suit-separates/br/v=1/2534374302063184.htm?skipMobile=true",

		#--Sweaters / Knits--#
		#Cardigans
		"http://www.shopbop.com/clothing-sweaters-knits-cardigans/br/v=1/2534374302069137.htm?skipMobile=true",
		#Cowl Neck
		"http://www.shopbop.com/clothing-sweaters-cowlneck/br/v=1/2534374302192440.htm?skipMobile=true",
		#Crew / Scoop Necks
		"http://www.shopbop.com/clothing-sweaters-knits-crew-scoop-necks/br/v=1/2534374302069135.htm?skipMobile=true",
		#Turtlenecks
		"http://www.shopbop.com/clothing-sweaters-knits-turtlenecks/br/v=1/2534374302069131.htm?skipMobile=true",
		#V Necks
		"http://www.shopbop.com/clothing-sweaters-knits-necks/br/v=1/2534374302069130.htm?skipMobile=true",

		#--Swimwear--#
		#Bikinis
		"http://www.shopbop.com/clothes-swimwear-bikinis/br/v=1/2534374302067620.htm?skipMobile=true",
		#Cover Ups
		"http://www.shopbop.com/clothes-swimwear-cover-ups/br/v=1/2534374302067619.htm?skipMobile=true",
		#One Pieces
		"http://www.shopbop.com/clothes-swimwear-one-pieces/br/v=1/2534374302067618.htm?skipMobile=true",

		#--Tops--#
		#Blouses
		"http://www.shopbop.com/clothing-tops-blouses/br/v=1/2534374302060711.htm",
		#Button Down Shirts
		"http://www.shopbop.com/clothes-tops-button-down-shirts/br/v=1/2534374302150953.htm?skipMobile=true",
		#Sweatshirts / Hoodies
		"http://www.shopbop.com/clothing-tops-sweatshirts-hoodies/br/v=1/2534374302060709.htm?skipMobile=true",
		#Tank Tops
		"http://www.shopbop.com/clothing-tops-tank/br/v=1/2534374302060705.htm?skipMobile=true",
		#Tees Long Sleeve
		"http://www.shopbop.com/clothing-tops-tees-long-sleeve/br/v=1/2534374302060707.htm?skipMobile=true",
		#Tees Short Sleeve
		"http://www.shopbop.com/clothing-tops-tees-short-sleeve/br/v=1/2534374302060706.htm?skipMobile=true",
		#Tunics
		"http://www.shopbop.com/clothing-tops-tunics/br/v=1/2534374302060703.htm?skipMobile=true",
		#Vests
		"http://www.shopbop.com/clothing-vests/br/v=1/2534374302196580.htm?skipMobile=true",

		# #--shoes--#
		# #Booties
		# #Flat
		# "http://www.shopbop.com/shoes-booties-flat/br/v=1/2534374302159515.htm",
		# #Heeled
		# "http://www.shopbop.com/shoes-booties-heeled/br/v=1/2534374302153373.htm",
		# #Lace Up
		# "http://www.shopbop.com/shoes-booties-lace/br/v=1/2534374302183263.htm",
		# #Wedges
		# "http://www.shopbop.com/shoes-booties-wedges/br/v=1/2534374302159513.htm",

		# #Boots
		# #Flat
		# "http://www.shopbop.com/shoes-boots-flat/br/v=1/2534374302112433.htm",
		# #Heeled
		# "http://www.shopbop.com/shoes-boots-heeled/br/v=1/2534374302153371.htm",
		# #Knee High
		# "http://www.shopbop.com/shoes-boots-knee-high/br/v=1/2534374302153374.htm",
		# #Wedges
		# "http://www.shopbop.com/shoes-boots-wedges/br/v=1/2534374302159445.htm",

		# #Flats
		# #Ballet
		# "http://www.shopbop.com/shoes-flats-ballet/br/v=1/2534374302201780.htm",
		# #Espadrilles
		# "http://www.shopbop.com/shoes-flats-espadrilles/br/v=1/27787.htm",
		# #Loafers
		# "http://www.shopbop.com/shoes-flats-loafers/br/v=1/2534374302201781.htm",
		# #Oxfords
		# "http://www.shopbop.com/shoes-flats-oxfords/br/v=1/2534374302201782.htm",
		# #Slippers
		# "http://www.shopbop.com/shoes-flats-slippers/br/v=1/2534374302201880.htm",

		# #Pumps
		# #Evening
		# "http://www.shopbop.com/shoes-pumps-heels-evening/br/v=1/2534374302159474.htm",
		# #Heels
		# "http://www.shopbop.com/shoes-pumps-heels/br/v=1/2534374302163493.htm",
		# #Open Toe
		# "http://www.shopbop.com/shoes-pumps-heels-open-toe/br/v=1/2534374302159460.htm",
		# #Platforms
		# "http://www.shopbop.com/shoes-pumps-heels-platforms/br/v=1/2534374302159453.htm",
		# #Wedges
		# "http://www.shopbop.com/shoes-pumps-heels-wedges/br/v=1/2534374302159497.htm",

		# #Sandals
		# #Evening
		# "http://www.shopbop.com/shoes-sandals-evening/br/v=1/2534374302159476.htm",
		# #Flat
		# "http://www.shopbop.com/shoes-sandals-flat/br/v=1/2534374302112443.htm",
		# #Flip Flops
		# "http://www.shopbop.com/shoes-sandals-flip-flops/br/v=1/2534374302112437.htm",
		# #High Heeled
		# "http://www.shopbop.com/shoes-sandals-high-heeled/br/v=1/2534374302112444.htm",
		# #Platforms
		# "http://www.shopbop.com/shoes-sandals-platforms/br/v=1/2534374302159496.htm",
		# #Wedges
		# "http://www.shopbop.com/shoes-sandals-wedges/br/v=1/2534374302159499.htm",

		# #Sneakers
		# "http://www.shopbop.com/shoes-sneakers/br/v=1/2534374302112446.htm",


		#--shoes--#
		#Booties
		"http://www.shopbop.com/shoes-booties/br/v=1/2534374302112431.htm?skipMobile=true",
		#Boots
		"http://www.shopbop.com/shoes-boots/br/v=1/2534374302112432.htm?skipMobile=true",
		#Flats
		"http://www.shopbop.com/shoes-flats/br/v=1/2534374302112436.htm?skipMobile=true",
		#Pumps
		"http://www.shopbop.com/shoes-pumps/br/v=1/2534374302112441.htm?skipMobile=true",
		#Sandals
		"http://www.shopbop.com/shoes-sandals/br/v=1/2534374302112442.htm?skipMobile=true",
		#Sneakers
		"http://www.shopbop.com/shoes-sneakers/br/v=1/2534374302112446.htm?skipMobile=true",

		#--bags--#
		#Backpacks
		"http://www.shopbop.com/bags-backpacks/br/v=1/2534374302055388.htm?skipMobile=true",
		#Clutches
		"http://www.shopbop.com/bags-clutches/br/v=1/2534374302055387.htm?skipMobile=true",
		#Cosmetic Pouches
		"http://www.shopbop.com/bags-cosmetic-pouches/br/v=1/2534374302055383.htm?skipMobile=true",
		#Cross Body Bags
		"http://www.shopbop.com/bags-cross-body/br/v=1/2534374302055380.htm?skipMobile=true",
		#Shoulder Bags
		"http://www.shopbop.com/bags-shoulder/br/v=1/2534374302055384.htm?skipMobile=true",
		#Totes
		"http://www.shopbop.com/bags-totes/br/v=1/2534374302055237.htm?skipMobile=true",
		#Wallets
		"http://www.shopbop.com/bags-wallets/br/v=1/2534374302056128.htm?skipMobile=true",


		#--Accessories--#
		#Belts
		"http://www.shopbop.com/accessories-belts/br/v=1/2534374302062844.htm?skipMobile=true",
		#Gloves
		"http://www.shopbop.com/accessories-gloves/br/v=1/2534374302062843.htm?skipMobile=true",
		#Hair Accessories
		"http://www.shopbop.com/accessories-hair/br/v=1/2534374302062842.htm?skipMobile=true",
		#Hats
		"http://www.shopbop.com/accessories-hats/br/v=1/2534374302062819.htm?skipMobile=true",
		#Hosiery
		"http://www.shopbop.com/hosiery/br/v=1/2534374302072037.htm?skipMobile=true",
		#Keychains
		"http://www.shopbop.com/accessories-keychains/br/v=1/2534374302062839.htm?skipMobile=true",
		#Scarves / Wraps
		"http://www.shopbop.com/accessories-scarves-wraps/br/v=1/2534374302062834.htm?skipMobile=true",
		#Sunglasses / Eyewear
		"http://www.shopbop.com/accessories-sunglasses-eyewear/br/v=1/2534374302029451.htm?skipMobile=true",
		#Tech Accessories
		"http://www.shopbop.com/accessories-tech/br/v=1/2534374302062840.htm?skipMobile=true",
		#Umbrellas
		"http://www.shopbop.com/accessories-umbrellas/br/v=1/2534374302062835.htm?skipMobile=true",
		#Watches
		"http://www.shopbop.com/accessories-watches/br/v=1/2534374302055823.htm?skipMobile=true",

		#--Jewelry--#
		#Bracelets
		"http://www.shopbop.com/accessories-jewelry-bracelets/br/v=1/2534374302060428.htm?skipMobile=true",
		#Earrings
		"http://www.shopbop.com/accessories-jewelry-earrings/br/v=1/2534374302060430.htm?skipMobile=true",
		#Necklaces
		"http://www.shopbop.com/accessories-jewelry-necklaces/br/v=1/2534374302060432.htm?skipMobile=true",
		#Rings
		"http://www.shopbop.com/accessories-jewelry-rings/br/v=1/2534374302060434.htm?skipMobile=true",

	]

	def custom_process_value(value):
 		return value.split('\'')[1]


 		# return links

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[@class="photo"]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@id="pagination-container-top"]/div[@class="pagination"]/div[@class="pages"]/span[@class="next"]'), tags=("span"), attrs=("data-next-link")), follow=True ),
  ]

 	


 	# def parse_start_url(self, response):
 	# 	print "parse_start_url"


	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//span[@itemprop="name"]/text()').extract()
		desc = sel.xpath('//div[@itemprop="description"]/node()').extract()

		original_price = sel.xpath('//meta[@itemprop="price"]/@content').extract()[0]

		price = {}
		price['original'] = original_price[1:]

		price_blocks = sel.xpath('//div[@id="productPrices"]/div[@class="priceBlock"]')
		is_sale = "false"

		#멀티가격일경우
		sale_price = {}
		for block in price_blocks:
			block_price = block.xpath('span[@class="salePrice"]/text()').extract()
			if len(block_price) == 0:
				continue
			is_sale = "true"
			block_color = block.xpath('span[@class="priceColors"]/text()').extract()[0]
			for block_color_split in block_color.split(','):
				sale_price[block_color_split.strip()] = block_price[0][1:].strip()
	

		price['sale_color'] =  sale_price

		if not price:
			return

		options = {}

		option_colors = sel.xpath('//div[@id="swatches"]/img[not(contains(@class, "swatchImage hover hidden"))]/@title').extract()
		option_sizes = sel.xpath('//div[@id="sizes"]/span/text()').extract()
		options['size'] = option_sizes
		options['color'] = option_colors

		breadcrumb = []
		breadcrumb = sel.xpath('//ul[@class="breadcrumb-list"]/li[@class="breadcrumb"]/span/text()').extract()
		breadcrumb_child = sel.xpath('//ul[@class="breadcrumb-list"]/li[@class="breadcrumb"]/a/span/text()').extract()
		breadcrumb.extend(breadcrumb_child)


		breadcrumb_str = ">".join(breadcrumb)

		brand = sel.xpath('//a[@itemprop="brand"]/text()').extract()

		swatches = sel.xpath('//div[@id="swatches"]/img')
	
		images = {}

		javascript = sel.xpath('//div[@id="content"]/script[@type="text/javascript"]/text()').extract()[0]
		javascript = javascript[20:].strip()
		javascript = javascript[:-1].strip()
		product = json.loads(javascript)

		#image
		for key in (collections.OrderedDict(sorted(product["colors"].items()))).keys():
			color_name = product["colors"][key]["colorName"]

			color_images = []

			for key2 in (collections.OrderedDict(sorted(product["colors"][key]["images"].items()))).keys():
				color_images.append(product["colors"][key]["images"][key2]["zoom"])

			images[color_name] = color_images

		
		colors = sel.xpath('//div[@id="swatches"]/img/@title').extract()
		sizes = sel.xpath('//div[@id="sizes"]/span/text()').extract()

		inventory = []
		for key in product["colors"].keys():
			for key2 in product["colors"][key]["sizes"]:
				for size in product["sizes"].keys():
					if key2 == product["sizes"][size]["sizeCode"]:
						inventory.append(product["colors"][key]["colorName"] + "-" + size)

		print "================================"

		# option_inventory = []
		option_inventory = {}

		for color in colors:
			for size in sizes:
				size = size.replace('.', '||')
				if color + "-" + size in inventory:
					option_inventory["color*_*" + color + "-_-size*_*" + size] = "available"
					# option_inventory.append("color*_*" + color + "-_-size*_*" + size + ":_:available")
				else:
					option_inventory["color*_*" + color + "-_-size*_*" + size] = "unavailable"
					# option_inventory.append("color*_*" + color + "-_-size*_*" + size + ":_:unavailable")


		#size table
		size_table_html = ""

		table_tr = sel.xpath('//table/tr')

		size_table_html = StringIO()
		for tr in table_tr:
			tr_html = tr.xpath('td[not(@colspan)]').extract()
			
			if tr_html:
				size_table_html.write("<tr>" + "".join(tr_html) + "</tr>")


		item = ShopbopItem()

		item['title'] = title[0].strip() if title else ""
		item['sub_title'] = ""
		item['url'] = response.url.split('?')[0] 
		item['domain'] = "shopbop.com"
		item['desc'] = "".join(desc).strip() if desc else ""
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = brand[0].strip() if brand else ""
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb_str)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html.getvalue() if size_table_html else ""
		item['is_crawled'] = 'true'

		return item

	def get_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("Clothing"):
			return self.get_clothing_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes"):
			return self.get_shoes_categoty(breadcrumb_str)
		elif breadcrumb_str.startswith("Bags"):
			return self.get_bags_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Accessories"):
			if breadcrumb_str.startswith("Accessories>Jewelry"):
				return self.get_jewelry_category(breadcrumb_str)
			else:
				return self.get_accessories_category(breadcrumb_str)
		else:
			return "women"

	#Jewelry category
	def get_jewelry_category(self, breadcrumb_str):
		if breadcrumb_str == "Accessories>Jewelry>Bracelets":
			return "women_jewelry_bracelets"
		elif breadcrumb_str == "Accessories>Jewelry>Earrings":
			return "women_jewelry_earrings"
		elif breadcrumb_str == "Accessories>Jewelry>Necklaces":
			return "women_jewelry_necklaces"
		elif breadcrumb_str == "Accessories>Jewelry>Rings":
			return "women_jewelry_rings"
		else:
			return "women_jewelry"

	#Accessories category
	def get_accessories_category(self, breadcrumb_str):
		if breadcrumb_str == "Accessories>Belts":
			return "women_accessories_belts"
		elif breadcrumb_str == "Accessories>Gloves":
			return "women_accessories_gloves"
		elif breadcrumb_str == "Accessories>Hair Accessories":
			return "women_accessories_hair"
		elif breadcrumb_str == "Accessories>Hats":
			return "women_accessories_hats"
		elif breadcrumb_str == "Accessories>Hosiery":
			return "women_accessories_hosiery"
		elif breadcrumb_str == "Accessories>Keychains":
			return "women_accessories_keychains"
		elif breadcrumb_str == "Accessories>Scarves / Wraps":
			return "women_accessories_scarves"
		elif breadcrumb_str == "Accessories>Sunglasses / Eyewear":
			return "women_accessories_eyewear"
		elif breadcrumb_str == "Accessories>Tech Accessories":
			return "women_accessories_tech"
		elif breadcrumb_str == "Accessories>Umbrellas":
			return "women_accessories_umbrellas"
		elif breadcrumb_str == "Accessories>Watches":
			return "women_accessories_watches"
		else:
			return "women_accessories"

	#bags category
	def get_bags_category(self, breadcrumb_str):
		if breadcrumb_str == "Bags>Backpacks":
			return "women_bags_backpacks"
		elif breadcrumb_str == "Bags>Clutches":
			return "women_bags_clutches"
		elif breadcrumb_str == "Bags>Cosmetic Pouches":
			return "women_bags_pouches"
		elif breadcrumb_str == "Bags>Cross Body Bags":
			return "women_bags_cross_body_bags"
		elif breadcrumb_str == "Bags>Shoulder Bags":
			return "women_bags_shoulder_bags"
		elif breadcrumb_str == "Bags>Totes":
			return "women_bags_totes"
		elif breadcrumb_str == "Bags>Wallets":
			return "women_bags_wallets"
		else:
			return "women_bags"

	
	#shoes category
	def get_shoes_categoty(self, breadcrumb_str):
		if breadcrumb_str.startswith("Shoes>Booties") or breadcrumb_str.startswith("Shoes>Boots"):
			return "women_boots_booties"
			# return self.get_shoes_boots_booties_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes>Flats"):
			return "women_flats"
			# return self.get_shoes_flats_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes>Pumps"):
			return "women_pumps"
			# return self.get_shoes_pumps_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes>Sandals"):
			return "women_sandals"
			# return self.get_shoes_sandals_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Shoes>Sneakers"):
			return "women_sneakers"
		else:
			return "women_shoes"

	#shoes sandals category
	# def get_shoes_sandals_category(self, breadcrumb_str):
	# 	if breadcrumb_str == "Shoes>Sandals>Evening":
	# 		return "women_sandals_evening"
	# 	elif breadcrumb_str == "Shoes>Sandals>Flat":
	# 		return "women_sandals_flat"
	# 	elif breadcrumb_str == "Shoes>Sandals>Flip Flops":
	# 		return "women_sandals_flip_flops"
	# 	elif breadcrumb_str == "Shoes >Sandals>High Heeled"

	#shoes pumps category
	# def get_shoes_pumps_category(breadcrumb_str):
	# 	if breadcrumb_str == "Shoes>Pumps>Evening":
	# 		return "women_pumps_evening"
	# 	elif breadcrumb_str == "Shoes>Pumps>Heels":
	# 		return "women_pumps_heels"
	# 	elif breadcrumb_str == "Shoes>Pumps>Open Toe":
	# 		return "women_pumps_toe"
	# 	elif breadcrumb_str == "Shoes>Pumps>Platforms":
	# 		return "women_pumps_platforms"
	# 	elif breadcrumb_str == "Shoes>Pumps>Wedges":
	# 		return "women_pumps_wedges"

	#shoes flats categoy
	# def get_shoes_flats_category(breadcrumb_str):
	# 	if breadcrumb_str == "Shoes>Flats>Ballet":
	# 		return "women_flats_ballet"
	# 	elif breadcrumb_str == "Shoes>Flats>Espadrilles":
	# 		return "women_flats_espadrilles" 
	# 	elif breadcrumb_str == "Shoes>Flats>Loafers":
	# 		return "women_flats_loafers"
	# 	elif breadcrumb_str == "Shoes>Flats>Oxfords":
	# 		return "women_flats_oxfords"
	# 	elif breadcrumb_str == "Shoes>Flats>Slippers":
	# 		return "women_flats_slippers"

	#shoes boots & booties category
	# def get_shoes_boots_booties_category(self, breadcrumb_str):
	# 	if breadcrumb_str == "Shoes>Boots>Flat" || breadcrumb_str == "Shoes>Booties>Flat":
	# 		return "women_boots_booties_flat"
	# 	elif breadcrumb_str == "Shoes>Boots>Heeled" || breadcrumb_str == "Shoes>Booties>Heeled":
	# 		return "women_boots_booties_heeled"
	# 	elif breadcrumb_str == "Shoes>Boots>Knee High":
	# 		return "women_boots_booties_over_the_knee"
	# 	elif breadcrumb_str == "Shoes>Boots>Wedges" || breadcrumb_str == "Shoes>Booties>Wedges":
	# 		return "women_boots_booties_wedges"
	# 	elif breadcrumb_str == "Shoes>Booties>Lace Up":
	# 		return "women_boots_booties_lace_up"

	#shoes booties category
	# def get_shoes_booties_category(self, breadcrumb_str):
	# 	if breadcrumb_str == "Shoes>Booties>Flat":
	# 		return "women_booties_flat"
	# 	elif breadcrumb_str == "Shoes>Booties>Heeled":
	# 		return "women_booties_heeled"
	# 	elif breadcrumb_str == "Shoes>Booties>Lace Up":
	# 		return "women_booties_lace_up"
	# 	elif breadcrumb_str == "Shoes>Booties>Wedges":
	# 		return "women_booties_wedges"

	#clothing category
	def get_clothing_category(self, breadcrumb_str):

		if breadcrumb_str.startswith("Clothing>Denim"):
			return self.get_jeans_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Jackets / Coats"):
			return self.get_jackets_coats_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Pants / Leggings"):
			return self.get_pants_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Skirts"):
			return self.get_skirts_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Sweaters / Knits"):
			return self.get_sweates_knits_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Swimwear"):
			return self.get_swimwear_category(breadcrumb_str)
		elif breadcrumb_str.startswith("Clothing>Tops") or breadcrumb_str.startswith("Clothing>Vests"):
			return self.get_tops_category(breadcrumb_str)
		else:
			if breadcrumb_str == "Clothing>Jumpsuits / Rompers":
				return "women_jumpsuit"
			elif breadcrumb_str == "Clothing>Leather":
				return "women_leather"
			elif breadcrumb_str == "Clothing>Loungewear / Yoga":
				return "women_loungewear"
			elif breadcrumb_str == "Clothing>Maternity":
				return "women_maternity"
			elif breadcrumb_str == "Clothing>Shorts":
				return "women_shorts"
			elif breadcrumb_str == "Clothing>Suit Separates":
				return "women_suits"
			else:
				return "women_clothing"

	def get_tops_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Tops>Blouses":
			return "women_blouses"
		elif breadcrumb_str == "Clothing>Tops>Button Down Shirts":
			return "women_shirts"
		elif breadcrumb_str == "Clothing>Tops>Sweatshirts / Hoodies":
			return "women_sweatshirts_hoodies"
		elif breadcrumb_str == "Clothing>Tops>Tank Tops":
			return "women_tank_tops"
		elif breadcrumb_str == "Clothing>Tops>Tees Long Sleeve":
			return "women_long_sleeve"
		elif breadcrumb_str == "Clothing>Tops>Tees Short Sleeve":
			return "women_short_sleeve"
		elif breadcrumb_str == "Clothing>Tops>Tunics":
			return "women_tunics"
		elif breadcrumb_str == "Clothing>Vests":
			return "women_vests"
		else:
			return "women_tops"

	def get_swimwear_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Swimwear>Bikinis":
			return "women_bikinis"
		elif breadcrumb_str == "Clothing>Swimwear>Cover Ups":
			return "women_cover_ups"
		elif breadcrumb_str == "Clothing>Swimwear>One Pieces":
			return "women_one_pieces"
		else:
			return "women_swimwear"

	def get_sweates_knits_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Sweaters / Knits>Cardigans":
			return "women_cardigans"
		elif breadcrumb_str == "Clothing>Sweaters / Knits>Cowl Neck":
			return "women_cowl_necks"
		elif breadcrumb_str == "Clothing>Sweaters / Knits>Crew / Scoop Necks":
			return "women_crew_necks"
		elif breadcrumb_str == "Clothing>Sweaters / Knits>Turtlenecks":
			return "women_turtle_necks"
		elif breadcrumb_str == "Clothing>Sweaters / Knits>V Necks":
			return "women_v_necks"
		else:
			return "women_sweaters_knits"

	

	def get_skirts_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Skirts>Mini":
			return "women_mini_skirts"
		elif breadcrumb_str == "Clothing>Skirts>Knee Length":
			return "women_knee_length_skirts"
		elif breadcrumb_str == "Clothing>Skirts>Midi":
			return "women_midi_skirts"
		elif breadcrumb_str == "Clothing>Skirts>Maxi":
			return "women_maxi_skirts"
		elif breadcrumb_str == "Clothing>Skirts>Denim":
			return "women_denim_skirts"
		else:
			return "women_skirts"

	def get_pants_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Pants / Leggings>Boot Cut / Flare":
			return "women_bootcut_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Cropped":
			return "women_cropped_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>High Waisted":
			return "women_high_waisted_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Leggings":
			return "women_leggings"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Skinny":
			return "women_skinny_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Slouchy Trousers":
			return "women_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Straight Leg":
			return "women_straight_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Sweatpants / Yoga":
			return "women_sweat_pants"
		elif breadcrumb_str == "Clothing>Pants / Leggings>Wide Leg":
			return "women_wideleg_pants"
		else:
			return "women_pants"

	def get_jackets_coats_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Jackets / Coats>Coats":
			return "women_coats"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Trench Coats":
			return "women_coats"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Pea Coats":
			return "women_coats"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Blazers":
			return "women_jackets"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Puffer Jackets":
			return "women_jackets"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Denim":
			return "women_jackets"
		elif breadcrumb_str == "Clothing>Jackets / Coats>Fashion":
			return "women_jackets"
		else:
			return "women_clothing"


	def get_jeans_category(self, breadcrumb_str):
		if breadcrumb_str == "Clothing>Denim>Boot Cut Jeans":
			return "women_bootcut_jeans"
		elif breadcrumb_str == "Clothing>Denim>Cropped Jeans":
			return "women_cropped_jeans"
		elif breadcrumb_str == "Clothing>Denim>High Waisted Jeans":
			return "women_high_waisted_jeans"
		elif breadcrumb_str == "Clothing>Denim>Flare Jeans / Wide Leg":
			return "women_flare_wide_jeans"
		elif breadcrumb_str == "Clothing>Denim>Skinny Jeans":
			return "women_skinny_jeans"
		elif breadcrumb_str == "Clothing>Denim>Straight Leg Jeans":
			return "women_straight_jeans"
		# elif breadcrumb_str == "Clothing>Denim>Coated Jeans":
		# 	return "women_coated_jeans"
		# elif breadcrumb_str == "Clothing>Denim>Colored Jeans":
		# 	return "women_colored_jeans"
		# elif breadcrumb_str == "Clothing>Denim>Printed Jeans":
		# 	return "women_printed_jeans"
		elif breadcrumb_str == "Clothing>Denim>Shorts":
			return "women_shorts_jeans"
		else:
			return "women_jeans"
		




