#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from sixpm.items import SixpmItem
from cStringIO import StringIO
from scrapy.http import Request
import json
import tokenize
import token
import requests
import decimal
import math

class SixpmSpider(CrawlSpider):
	name = "sixpm"
	allow_domains = ["6pm.com"]
	start_urls = [
		# "http://www.6pm.com/mavi-jeans-serena-low-rise-super-skinny-light-orange",
		# "http://www.6pm.com/volcom-stix-skinny-jeans-blue-rinse"
		# "http://www.6pm.com/brigitte-bailey-lace-3-4-sleeve-top-ivory"
		# "http://www.6pm.com/asics-gel-nimbus-15-lite-s-black-lightning-hot-pink-1"
		# "http://www.6pm.com/roxy-bliss-crossbody-satchel-estate-blue"
		# "http://www.6pm.com/french-connection-runaway-tote-black-cognac"
		# "http://www.6pm.com/gabriella-rocha-aviator-sunglasses-neon-orange"
		# "http://www.6pm.com/taryn-rose-kellen-gold-metallic-suede"
		# "http://www.6pm.com/izod-short-sleeve-mini-stripe-jersey-crew-neck-tee-blue-grotto"
		# "http://www.6pm.com/rockport-davinton-moc-toe-oxford-black"
		# "http://www.6pm.com/sperry-top-sider-canoe-liner-3-pack-ivory-red-black"
		
		# 여자 신발
		"http://www.6pm.com/womens-shoes~b?s=goliveRecentSalesStyle/desc/",
		# 여자 의류
		"http://www.6pm.com/womens-clothing~4?s=goliveRecentSalesStyle/desc/",
		# 여자 가방
		"http://www.6pm.com/womens-bags~3?s=goliveRecentSalesStyle/desc/",
		# 여자 선글라스
		"http://www.6pm.com/women-eyewear~1",
		# 여자 주얼리
		"http://www.6pm.com/women-jewelry~1",
		# 여자 시계
		"http://www.6pm.com/women-watches~1",
		# 여자 악세서리
		"http://www.6pm.com/womens-accessories~k?s=goliveRecentSalesStyle/desc/",

		# 남자 신발
		"http://www.6pm.com/mens-shoes~s?s=goliveRecentSalesStyle/desc/",
		# 남자 의류
		"http://www.6pm.com/mens-clothing~5?s=goliveRecentSalesStyle/desc/",
		# 남자 가방
		"http://www.6pm.com/bags~2q#!/mens-bags~8?s=goliveRecentSalesStyle/desc/",
		# 남자 선글라스
		"http://www.6pm.com/men-eyewear~1",
		# 남자 주얼리
		"http://www.6pm.com/men-jewelry~1",
		# 남자 시계
		"http://www.6pm.com/men-watches~2",
		# 남자 악세서리
		"http://www.6pm.com/men-accessories~1",

		# "http://www.6pm.com/crocs-yukon-black-black"
		# "http://www.6pm.com/native-eyewear-itso-polarized-gunmetal"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[contains(@class, "product")]')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//a[contains(@class, "arrow pager")]')), follow=True ),
  ]


	def parse_item(self, response):
		sel = Selector(response)

		brand = sel.xpath('//a[@class="brand"]/text()').extract()
		title = sel.xpath('//a[@class="link fn"]/text()').extract()
		breadcrumb = sel.xpath('//div[@id="breadcrumbs"]/a/text()').extract()
		old_price = sel.xpath('//span[@class="oldPrice"]/text()').extract()

		if old_price:
			old_price = old_price[0].split('$')[1]
		else:
			old_price = ""

		price = sel.xpath('//div[@class="price"]/text()').extract()[0][1:]
		desc = sel.xpath('//div[@class="description"]/node()').extract()
		# color = sel.xpath('//select[@id="color"]/option/text()').extract()
		# label_size = sel.xpath('//label[@id="labelsize"]/@class').extract()[0]
		# size = sel.xpath('//select[@id="' + label_size  + '"]/option/text()').extract()[1:]


		scripts = sel.xpath('//script[@type="text/javascript"]/text()').extract()

		
		script = ""
		for script in scripts:
			if "Product page private namespace" in script:
				script = script
				break

		temp_str = script.split("var productGender = ")[1]
		productGender = temp_str.split("var zetaCategories")[0]
		productGender = json.loads(productGender.replace(';', ''), strict=False)

		# temp_str = script.split("var zetaCategories = ")[1]
		# zetaCategories = temp_str.split("var prodgroupkill")[0]
		# zetaCategories = json.loads(zetaCategories.replace(';', ''), strict=False)

		# temp_str = script.split("category = ")[1]
		# category = temp_str.split("subCategory = ")[0]
		# category = json.loads(category.replace(';', ''), strict=False)

		# temp_str = script.split("subCategory = ")[1]
		# subCategory = temp_str.split("var categoryNum")[0]
		# subCategory = json.loads(subCategory.replace(';', ''), strict=False)

		# zeta_cagtegory = []
		# for z in zetaCategories:
		# 	zeta_cagtegory.append(z.values()[0])

		temp_str = script.split("var stockJSON = ")[1]
		stockJSON = temp_str.split("var dimensions = ")[0]

		stockJSON = json.loads(stockJSON.replace(';', ''), strict=False)


		temp_str = temp_str.split("var dimensions = ")[1]
		dimensions = temp_str.split("var dimToUnitToValJSON")[0]
		dimensions = json.loads(dimensions.replace(';', ''), strict=False)


		temp_str = temp_str.split("var dimToUnitToValJSON = ")[1]
		dimToUnitToValJSON = temp_str.split('var dimensionIdToNameJson')[0]
		dimToUnitToValJSON = json.loads(dimToUnitToValJSON.replace(';', ''), strict=False)


		temp_str = temp_str.split("var dimensionIdToNameJson = ")[1]
		dimensionIdToNameJson = temp_str.split('var valueIdToNameJSON')[0]
		dimensionIdToNameJson = json.loads(dimensionIdToNameJson.replace(';', ''), strict=False)


		temp_str = temp_str.split("var valueIdToNameJSON = ")[1]
		valueIdToNameJSON = temp_str.split("var colorNames")[0]
		valueIdToNameJSON = json.loads(valueIdToNameJSON.replace(';', ''), strict=False)


		temp_str = temp_str.split("var colorNames = ")[1]
		colorNames = temp_str.split("var colorPrices")[0]
	
		colorNames = json.loads(colorNames.replace(';', '').replace('\'', '"'), strict=False)


		temp_str = temp_str.split("var colorPrices = ")[1]
		colorPrices = temp_str.split("var styleIds")[0]
		colorPrices = json.loads(colorPrices.replace(';', '').replace('\'', '"'), strict=False)


		temp_str = temp_str.split("var styleIds = ")[1]
		styleIds = temp_str.split("var colorIds")[0]
		styleIds = json.loads(styleIds.replace(';', '').replace('\'', '"'), strict=False)


		images = {}

		exist_image = True
		
		for style in styleIds:
			image_arr = []
			str_ = temp_str.split("pImgs[" + str(styleIds[style]) + "]['4x']")
			for idx, val in enumerate(str_):
				if idx > 1:
					exist_image = True
					temp_str_ = str_[idx]
					image_arr.append(temp_str_.split("filename: '")[1].split("',")[0])
				else:
					exist_image = False

			images[colorNames[style]] = image_arr

		if not exist_image:
			return


		
		price = {}
		price['sale_color'] = {}
		for c in colorPrices:
			if old_price: 
				price['sale_color'][colorNames[c]] = str(colorPrices[c]["nowInt"])
			else:
				price['sale_color'] = {}
			price['original'] = str(colorPrices[c]["wasInt"])


		color_arr = []
		for key, value in colorNames.items():
			color_arr.append(key + "_|_" + value)


		size_arr_1 = []
		size_arr_2 = []

		for key, value in dimensionIdToNameJson.items():
			if value == "size":
				for d in dimToUnitToValJSON[key]:
					for d2 in dimToUnitToValJSON[key][d]:
						for v in valueIdToNameJSON[str(d2)]:
							if v == "value":
								size_arr_1.append(key + "-" + str(d2) + "_|_" + valueIdToNameJSON[str(d2)][v] ) 
			else:
				for d in dimToUnitToValJSON[key]:
					for d2 in dimToUnitToValJSON[key][d]:
						for v in valueIdToNameJSON[str(d2)]:
							if v == "value":
								size_arr_2.append("|||" + key + "-" + str(d2) + "_|_" + value + "(" + valueIdToNameJSON[str(d2)][v] + ")")


		size_arr = []
		for size_1 in size_arr_1:
			if size_arr_2:
				for size_2 in size_arr_2:
					size_arr.append(size_1 + size_2)
			else:
				size_arr.append(size_1)




		inventory = {}
		options = {}
		options["color"] = []
		options["size"] = []


		for c in color_arr:
			options["color"].append(c.split('_|_')[1])
			options["size"] = []
			if size_arr:
				for s in size_arr:
					if size_arr_2:
						c_val = c.split('_|_')[0]
						op1 = s.split("|||")[0].split("_|_")[0].split("-")[0]

						op1_val = s.split("|||")[0].split("_|_")[0].split("-")[1]
						op2 = s.split("|||")[1].split("_|_")[0].split("-")[0]
						op2_val = s.split("|||")[1].split("_|_")[0].split("-")[1]

						option = s.split("|||")[0].split("_|_")[1] + " - " + s.split("|||")[1].split("_|_")[1]

						options["size"].append(option)
						if self.get_exist(stockJSON, "color", c_val, op1, op1_val, op2, op2_val):
							inventory["color*_*" + colorNames[c_val] + "-_-size*_*" + option.replace('.', '||')] = "available"
						else:
							inventory["color*_*" + colorNames[c_val] + "-_-size*_*" + option.replace('.', '||')] = "unavailable"
					else:
						c_val = c.split('_|_')[0]
						op1 = s.split('_|_')[0].split('-')[0]
						op1_val = s.split('_|_')[0].split('-')[1]

						option = s.split('_|_')[1]
						options["size"].append(option)
						
						if self.get_exist(stockJSON, "color", c_val, op1, op1_val):
							inventory["color*_*" + colorNames[c_val] + "-_-size*_*" + option.replace('.', '||')] = "available"
						else:
							inventory["color*_*" + colorNames[c_val] + "-_-size*_*" + option.replace('.', '||')] = "unavailable"
			else:
				options["size"] = ["One Size"]
				c_val = c.split('_|_')[0]
				if self.get_exist(stockJSON, "color", c_val):
					inventory["color*_*" + colorNames[c_val] + "-_-size*_*One Size"] = "available"
				else:
					inventory["color*_*" + colorNames[c_val] + "-_-size*_*One Size"] = "unavailable"

		

		if not options["size"]:
			options["size"] = ["One Size"]

		if not options["color"]:
			options["color"] = ["One Color"]


		is_sale = "false"

		if old_price: 
			is_sale = "true"

		is_soldout = "false"
		unavailable_count = 0

		for i in inventory.values():
			if i == "unavailable":
				unavailable_count += 1

		if unavailable_count == len(inventory.values()):
			is_soldout = "true"


		keywords = map(lambda it: it.strip(), sel.xpath('//meta[@name="keywords"]/@content').extract()[0].split(','))
		sale_rate = 0

		if price["sale_color"].values():
			min_sale_price =  min(price["sale_color"].values())
			sale_rate = math.ceil((float(price["original"]) - float(min_sale_price))/float(price["original"])*100)



		category = self.get_category(productGender, breadcrumb)

		if not category:
			return

		item = SixpmItem()

		item['title'] = title[0].strip() if title else ""
		item['sub_title'] = ""
		item['url'] = response.url
		item['domain'] = "6pm.com"
		item['desc'] = desc[0] if desc else ""
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = brand[0] if brand else ""
		item['breadcrumb'] = breadcrumb
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = ""
		item['is_soldout'] = is_soldout
		item['keywords'] = keywords
		item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item


	def get_exist(self, list, color, c_val, op1="", op1_val="", op2="", op2_val=""):
		
		is_exist = False
		if op1 == "" and op1_val == "" and op2 == "" and op2_val == "":
			for l in list:
				if l[color] == c_val:
					is_exist =  True
					break

		elif op2 == "" and op1_val == "" and op1 != "" and op1_val != "":
			for l in list:
				if l[color] == c_val and l[op1] == op1_val and l[op2] == op2_val:
					is_exist =  True
					break
		else:
			for l in list:
				if l[color] == c_val and l[op1] == op1_val:
					is_exist =  True
					break


		return is_exist

	def get_category(self, productGender, breadcrumb):

		if productGender == "Womens":
			return self.get_womens_category(breadcrumb)
		elif productGender == "Mens":
			return self.get_mens_category(breadcrumb)
		else:
			return False

	def get_mens_category(self, breadcrumb):

		if "Shoes" in breadcrumb:
			return self.get_mens_shoes_category(breadcrumb)
		elif "Clothing" in breadcrumb:
			return self.get_mens_clothing_category(breadcrumb)
		elif "Bags" in breadcrumb:
			return self.get_mens_bags_category(breadcrumb)
		elif "Eyewear" in breadcrumb:
			return "men_accessories_eyewears"
		elif "Jewelry" in breadcrumb:
			return self.get_mens_jewelry_category(breadcrumb)
		elif "Watches" in breadcrumb:
			return "men_accessories_watches"
		elif "Accessories" in breadcrumb:
			print "FFFFFF"
			return self.get_mens_accessories_category(breadcrumb)

	def get_mens_accessories_category(self, breadcrumb):
		if "Hats" in breadcrumb:
			return "men_accessories_hats"
		elif "Belts" in breadcrumb:
			return "men_accessories_belts"
		elif "Gloves" in breadcrumb:
			return "men_accessories_gloves"
		elif "Ties" in breadcrumb:
			return "men_accessories_ties"
		elif "Tech Accessories" in breadcrumb:
			return "men_accessories_tech"
		elif "Scarves" in breadcrumb:
			return "men_accessories_scarves"
		else:
			return "men_accessories"

	def get_mens_jewelry_category(self, breadcrumb):
		if "Cufflinks" in breadcrumb:
			return "men_jewelry_cufflinks"
		elif "Bracelets" in breadcrumb:
			return "men_jewelry_bracelets"
		elif "Tie Clips" in breadcrumb:
			return "men_jewelry_tiebar"
		elif "Rings" in breadcrumb:
			return "men_jewelry_rings"
		elif "Necklaces" in breadcrumb:
			return "men_jewelry_necklaces"
		else:
			return "men_jewelry"

	def get_mens_bags_category(self, breadcrumb):
		if "Backpacks" in breadcrumb:
			return "men_bags_backpacks"
		elif "Wallets & Accessories" in breadcrumb:
			return "men_accessories_wallets"
		elif "Briefcases" in breadcrumb:
			return "men_bags_briefcases"
		else:
				return "men_bags"

	def get_mens_clothing_category(self, breadcrumb):
		if "Shirts & Tops" in breadcrumb:
			return "men_clothing_tops"
		elif "Coats & Outerwear" in breadcrumb:
			return "men_clothing_outerwears"
		elif "Shorts" in breadcrumb:
			return "men_clothing_shorts"
		elif "Swimwear" in breadcrumb:
			return "men_clothing_swimwear"
		elif "Hoodies & Sweatshirts" in breadcrumb:
			return "men_clothing_sweatshirts_hoodies"
		elif "Jeans" in breadcrumb:
			return "men_clothing_jeans"
		elif "Pants" in breadcrumb:
			return "men_clothing_pants"
		elif "Sweaters" in breadcrumb:
			return "men_clothing_sweaters"
		elif "Socks" in breadcrumb:
			return "men_accessories_socks"
		elif "Underwear" in breadcrumb:
			return "men_clothing_underwear"
		elif "Blazers & Jackets" in breadcrumb:
			return "men_clothing_jackets"
		elif "Sleepwear" in breadcrumb:
			return "men_clothing_loungewear"
		elif "Suits" in breadcrumb:
			return "men_clothing_suits"
		else:
			return "men_clothing"

	def get_mens_shoes_category(self, breadcrumb):
		if "Sneakers & Athletic Shoes" in breadcrumb:
			return "men_shoes_sneakers"
		elif "Oxfords" in breadcrumb:
			return "men_shoes_oxfords"
		elif "Loafers" in breadcrumb:
			return "men_shoes_loafers"
		elif "Boots" in breadcrumb:
			return "men_shoes_boots"
		elif "Sandals" in breadcrumb:
			return "men_shoes_sandals"
		elif "Boat Shoes" in breadcrumb:
			return "men_shoes_boat"
		elif "Clogs & Mules" in breadcrumb:
			return "men_shoes_clogs"
		elif "Slippers" in breadcrumb:
			return "men_shoes_slippers"
		else:
			return "men_shoes"
		
	def get_womens_category(self, breadcrumb):
		if "Clothing" in breadcrumb:
			return self.get_womens_clothing_category(breadcrumb)
		elif "Shoes" in breadcrumb:
			return self.get_womens_shoes_category(breadcrumb)
		elif "Bags" in breadcrumb:
			return self.get_womens_bags_category(breadcrumb)
		elif "Eyewear" in breadcrumb:
			return "women_accessories_eyewear"
		elif "Jewelry" in breadcrumb:
			return self.get_womens_jewelry_category(breadcrumb)
		elif "Watches" in breadcrumb:
			return "women_accessories_watches"
		elif "Accessories" in breadcrumb:
			return self.get_womens_accessories_category(breadcrumb)


	def get_womens_accessories_category(self, breadcrumb):
		if "Hats" in breadcrumb:
			return "women_accessories_hats"
		elif "Scarves" in breadcrumb:
			return "women_accessories_scarves"
		elif "Belts" in breadcrumb:
			return "women_accessories_belts"
		elif "Gloves" in breadcrumb:
			return "women_accessories_gloves"
		elif "Tech Accessories" in breadcrumb:
			return "women_accessories_tech"
		elif "Hair Accessories" in breadcrumb:
			return "women_accessories_hair"
		else:
			return "women_accessories"


	def get_womens_jewelry_category(self, breadcrumb):
		if "Earrings" in breadcrumb:
			return "women_jewelry_earrings"
		elif "Necklaces" in breadcrumb:
			return "women_jewelry_necklaces"
		elif "Bracelets" in breadcrumb:
			return "women_jewelry_bracelets"
		elif "Rings" in breadcrumb:
			return "women_jewelry_rings"
		else:
			return "women_jewelry"


	def get_womens_bags_category(self, breadcrumb):
		if "Handbags" in breadcrumb:
			return "women_bags_handbags"
		elif "Wallets & Accessories" in breadcrumb:
			return "women_bags_wallets"
		elif "Backpacks" in breadcrumb:
			return "women_bags_backpacks"
		else:
			return "women_bags"
	


	def get_womens_shoes_category(self, breadcrumb):
		if "Sandals" in breadcrumb:
			return "women_sandals"
		elif "Heels" in breadcrumb:
			return "women_pumps"
		elif "Boots" in breadcrumb:
			return "women_boots_booties"
		elif "Sneakers & Athletic Shoes" in breadcrumb:
			return "women_sneakers"
		elif "Flats" in breadcrumb:
			return "women_flats"
		elif "Loafers" in breadcrumb:
			return "women_flats_loafers"
		elif "Clogs & Mules" in breadcrumb:
			return "women_shoes_clogs"
		elif "Oxfords" in breadcrumb:
			return "women_flats_oxfords"
		elif "Boat Shoes" in breadcrumb:
			return "women_shoes_boat"
		elif "Slippers" in breadcrumb:
			return "women_flats_slippers"
		else:
			return "women_shoes"

		

	def get_womens_clothing_category(self, breadcrumb):
		if "Shirts & Tops" in breadcrumb:
			return "women_tops"
		elif "Dresses" in breadcrumb:
			return "women_dresses"
		elif "Swimwear" in breadcrumb:
			return "women_swimwear"
		elif "Pants" in breadcrumb:
			return "women_pants"
		elif "Jeans" in breadcrumb:
			return "women_jeans"
		elif "Coats & Outerwear" in breadcrumb:
			return "women_coats"
		elif "Sweaters" in breadcrumb:
			return "women_sweaters_knits"
		elif "Shorts" in breadcrumb:
			return "women_shorts"
		elif "Underwear & Intimates" in breadcrumb:
			return "women_lingerie"
		elif "Skirts" in breadcrumb:
			return "women_skirts"
		elif "Hoodies & Sweatshirts" in breadcrumb:
			return "women_sweatshirts_hoodies"
		elif "Sleepwear" in breadcrumb:
			return "women_loungewear"
		elif "Blazers & Jackets" in breadcrumb:
			return "women_jackets"
		elif "Socks" in breadcrumb:
			return "women_accessories_hosiery"
		elif "Jumpsuits & Rompers" in breadcrumb:
			return "women_jumpsuit"
		elif "Suits" in breadcrumb:
			return "women_suits"
		else:
			return "women_clothing"





