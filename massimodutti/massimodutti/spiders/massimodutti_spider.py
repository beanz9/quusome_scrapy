#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from massimodutti.items import MassimoduttiItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json
from collections import OrderedDict

class MassimoduttiSpider(CrawlSpider):
	name = "massimodutti"
	allow_domains = ["massimodutti.com"]
	start_urls = [
		"http://www.massimodutti.com/us/en/women/t-shirts/view-all/t-shirt-with-transparencies-c1124511p4526700.html?colorId=518",
	]

	def parse(self, response):
		print ">>>>>>>>>"
		sel = Selector(response)
		title = sel.xpath('//h1[@class="product-name"]/text()').extract()[0]
		html = sel.xpath('//html').extract()

		print "!!!"
		print html