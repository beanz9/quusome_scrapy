# Scrapy settings for asos project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'asos'

SPIDER_MODULES = ['asos.spiders']
NEWSPIDER_MODULE = 'asos.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'asos (+http://www.yourdomain.com)'
