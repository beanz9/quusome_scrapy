from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from asos.items import AsosItem
from cStringIO import StringIO
from scrapy.http import Request
import json
import tokenize
import token
import requests
import decimal
import math

class AsosSpider(CrawlSpider):
	name = "asos"
	allow_domains = ["asos.com"]
	start_urls = [
		"http://us.asos.com/ASOS/ASOS-Crop-Top-with-Bardot-Sweetheart-Neckline/Prod/pgeproduct.aspx?iid=4430916&cid=11550&sh=0&pge=0&pgesize=36&sort=-1&clr=Dark+green"
	]

	def parse(self, response):
		sel = Selector(response)
		brand = sel.xpath('//title/text()').extract()[0].split('|')[0].strip()
		title = sel.xpath('//span[@class="product_title"]/text()').extract()[0]
		breadcrumb = sel.xpath('//span[@id="ctl00_ContentMainPage_ctlBreadCrumbs_lblBreadCrumbs"]/a/text()').extract()
		
		print breadcrumb

