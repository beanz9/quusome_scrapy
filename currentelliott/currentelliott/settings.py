# Scrapy settings for currentelliott project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'currentelliott'

SPIDER_MODULES = ['currentelliott.spiders']
NEWSPIDER_MODULE = 'currentelliott.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'currentelliott (+http://www.yourdomain.com)'
ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
# MONGODB_URI = 'mongodb://mongo_scrapy:beanz9_quusome_mongo@128.199.245.247:27017/scrapy'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True
