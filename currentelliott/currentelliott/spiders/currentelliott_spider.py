#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from currentelliott.items import CurrentelliottItem
from cStringIO import StringIO
from scrapy.http import Request
import math
import requests	
import json
from collections import OrderedDict

class CurrentelliottSpider(CrawlSpider):
	name = "currentelliott"
	allow_domains = ["currentelliott.com"]
	start_urls = [
		# "http://www.currentelliott.com/shop/jackets/the-infantry-jacket-dirty-white"
		# "http://www.currentelliott.com/shop/tops/the-denim-tee-whirlwind"
		# "http://www.currentelliott.com/shop/sale/the-perfect-shirt-without-epaulettes-army-camo"
		"http://www.currentelliott.com/shop/denim",
		"http://www.currentelliott.com/shop/shirts-61",
		"http://www.currentelliott.com/shop/tops",
		"http://www.currentelliott.com/shop/jackets",
		"http://www.currentelliott.com/shop/dresses-skirts",
		"http://www.currentelliott.com/shop/sweats/sweatshirts",
		"http://www.currentelliott.com/shop/sweats/sweatpants"
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="item-wrapper"]/a')), callback="parse_item" ),
  	
  ]


	def parse_item(self, response):
		sel = Selector(response)

		title = sel.xpath('//div[@class="product-name"]/h1/text()').extract()[0]
		desc1 = sel.xpath('//div[@id="panel2"]/text()').extract()[0]
		desc2 = sel.xpath('//div[@id="panel3"]/node()').extract()
		desc = desc1 + "<br>" + "".join(desc2)
		color = sel.xpath('//div[@id="product-color-options-container"]/label/span/text()').extract()[0]
		regular_price = sel.xpath('//span[@class="regular-price"]/span[@class="price"]/text()').extract()
		size = sel.xpath('//ul[@class="chosen-results"]/li[@class="active-result"]/text()').extract()
		image = sel.xpath('//div[@class="hide-for-small"]/div[@class="product-image"]/img/@src').extract()
		category = sel.xpath('//body/@class').extract()[0]

		is_sale = 'false'
		options = {}
		option_inventory = {}
		images = {}

		images[color] = image

		price = {}
		price['sale_color'] = {}

		if regular_price:
			price['original'] = regular_price[0][1:]
		else:
			old_price = sel.xpath('//p[@class="old-price"]/span[@class="price"]/text()').extract()[0]
			sale_price = sel.xpath('//p[@class="special-price"]/span[@class="price"]/text()').extract()[0]
			price['original'] = old_price.strip()[1:]
			price['sale_color'][color] = sale_price.strip()[1:]
			is_sale = 'true'

		options['color'] = [color]

		html = str(sel.xpath('//html').extract())

		script = html.split('"options":')[1].split('}},')[0]
		json_obj = json.loads(script, strict=False)
		option_arr = []
		for j in json_obj:
			size = j['label']
			if '(out of stock)' in size:
				option_arr.append(size.replace(' (out of stock) ', ''))
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace(' (out of stock) ', '').replace('.', '||')] = "unavailable"
			else:
				option_arr.append(size)
				option_inventory["color*_*" + color + "-_-size*_*" + size.replace('.', '||')] = "available"

		options['size'] = option_arr

		category2 = self.get_category(category)

		size_table_html = '<img src="http://smcdn.currentelliott.com/skin/frontend/currentelliott/currentelliott/images/size_chart.jpg">'
		if not category2:
			return

		item = CurrentelliottItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "currentelliott.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "CURRENT/ELLIOTT"
		item['breadcrumb'] = ""
		item['category'] = category2
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item		

	def get_category(self, category):
		if 'categorypath-shop-denim' in category:
			return 'women_jeans'
		elif 'category-shirts' in category:
			return 'women_tops'
		elif 'category-tops' in category:
			return 'women_tops'
		elif 'category-jackets' in category:
			return 'women_jackets'
		elif 'category-dresses-skirts' in category:
			return 'women_skirts'
		elif 'category-sweatshirts' in category:
			return 'women_sweaters_knits'
		elif 'category-sweatpants' in category:
			return 'women_pants'



