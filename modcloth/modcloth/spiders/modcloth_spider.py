#-*- coding: utf-8 -*-

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from modcloth.items import ModclothItem
from cStringIO import StringIO
from scrapy.http import Request
import json
import tokenize
import token
import math


class ModclothSpider(CrawlSpider):
	name = "modcloth"
	allow_domains = ["modcloth.com"]
	start_urls = [
		"http://www.modcloth.com/shop/dresses",
		"http://www.modcloth.com/shop/tops",
		"http://www.modcloth.com/shop/bottoms",
		"http://www.modcloth.com/shop/outerwear",
		"http://www.modcloth.com/shop/swimwear",
		#레깅스
		"http://www.modcloth.com/shop/tights-socks",
		#란제리
		"http://www.modcloth.com/shop/intimates",

		"http://www.modcloth.com/shop/shoes-heels",
		"http://www.modcloth.com/shop/shoes-wedges",
		"http://www.modcloth.com/shop/shoes-boots",
		"http://www.modcloth.com/shop/shoes-flats",
		"http://www.modcloth.com/shop/shoes-sandals",

		"http://www.modcloth.com/shop/bags",
		"http://www.modcloth.com/shop/jewelry",
		"http://www.modcloth.com/shop/sunglasses",
		"http://www.modcloth.com/shop/hats",
		"http://www.modcloth.com/shop/hairaccessories",
		"http://www.modcloth.com/shop/tech-accessories",
		"http://www.modcloth.com/shop/scarves",
		"http://www.modcloth.com/shop/gloves",
		"http://www.modcloth.com/shop/belts",
		"http://www.modcloth.com/shop/umbrellas",




		
	]

	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="thumbnail"]/a')), callback="parse_item" ),
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="pagination arrows pagination-top"]/div[@class="pagination-pages arrows"]/div[@class="pagination"]/a[@class="next_page"]')), follow=True ),
  ]

	def parse_item(self, response):
		sel = Selector(response)
		product = sel.xpath('//div[@id="main-content"]/script').extract()[0]
		product = product.split('var product = ')[1]
		product = product.split(',\n      product_id =')[0]
		product = json.loads(product)

	

		title = product["name"]
		# desc = sel.xpath('//div[@id="pdp_limited_description"]/p/text()').extract()

		# if not desc:
		# 	desc = sel.xpath('//div[@id="pdp_limited_description"]/p/span/text()').extract()
		

		# desc = desc[0]

		# desc2 = sel.xpath('//div[@id="details-and-measures"]/ul/li').extract()
		# desc = "<ul>"

		# for d in desc2:
		# 	if d != '':
		# 		desc += "<li>"
		# 		desc += d
		# 		desc += "</li>"

		# desc += "</ul>"

		# desc = "<ul>" + "".join(desc2) + "</ul>"

		# desc = desc + "\n" + "".join(desc2)

		# original_price = sel.xpath('//span[@class="product_price_sale"]/text()').extract()[0][1:]
		# sale_price = sel.xpath('//span[@class="sale-price"]/text()').extract()

		desc = sel.xpath('//div[@itemprop="description"]/node()').extract()


		price = {}
		price['original'] = product["price"]
		is_sale = "false"

		if product["sale_price"] != None:
			if product["sale_price"] != "0":
				is_sale = "true"
				price['sale_color'] = {"one color": product["sale_price"]}
			else:
				price['sale_color'] = {}
		else:
			price['sale_color'] = {}



		options = {}
		size_option_arr = []

		option_sizes = product["ordered_variant_values_for_sizes"]
		for option_size in option_sizes:
			size_option_arr.append(option_size["value"])
		
		options["size"] = size_option_arr

		if not size_option_arr:
			options["size"] = ["one size"]

		options["color"] = ["one color"]

		breadcrumb = sel.xpath('//div[@id="breadcrumbs"]/ul/li/a/text()').extract()
		breadcrumb_str = ">".join(breadcrumb)

		out_of_stock = sel.xpath('//span[@class="stock-scarce"]/text()').extract()


		image_arr = sel.xpath('//ul[@id="product-image-thumbnails"]/script').extract()
		images = {}
		image_temp_arr = []


		for image in image_arr:
			image = image.split('{')[1]
			image = image.split('}')[0]

			# print image
			image = json.loads("{" + self.fixLazyJsonWithComments(image) + "}")

			if image["is_zoomable"]:
				image_temp_arr.append(image["hi_res_url"])
			else:
				image_temp_arr.append(image["pdp_url"])

		images["one color"] = image_temp_arr

		option_inventory = {}

		for p in product["ordered_variant_values_for_sizes"]:
			id = p["customized_id"]
			for v in product["variants"]:
				if id == v["id"]:
					size = p["value"].replace('.', '||')
					if v["in_stock"]:
						option_inventory["color*_*one color-_-size*_*" + size] = "available"
					else:
						option_inventory["color*_*one color-_-size*_*" + size] = "unavailable"
					break

		if not option_inventory:
			if out_of_stock:
				option_inventory["color*_*one color-_-size*_*one size"] = "unavailable"
			else:
				option_inventory["color*_*one color-_-size*_*one size"] = "available"

		sale_rate = 0

		if price["sale_color"].values():
			min_sale_price =  min(price["sale_color"].values())
			sale_rate = math.ceil((float(price["original"]) - float(min_sale_price))/float(price["original"])*100)


		measurements_table_html = sel.xpath('//table[@class="measurements-data"]/node()').extract()

		brand = sel.xpath('//h2[@id="pdp-brand"]/a/text()').extract()
		

		size_table_html = ""

		if breadcrumb_str.startswith("ModCloth>Clothing"):
			size_table_html = "<tr class='header'>\n<td></td>\n<td>XS</td>\n<td>S</td>\n<td>M</td>\n<td>L</td>\n<td>XL</td>\n<td>1X</td>\n<td>2X</td>\n<td>3X</td>\n<td>4X</td>\n</tr>\n<tr>\n<th>\nBust\n<span>(inches)</span>\n</th>\n<td class='inch'>31-32</td>\n<td class='inch'>33-34</td>\n<td class='inch'>35-36</td>\n<td class='inch'>37-39</td>\n<td class='inch'>40-42</td>\n<td class='inch'>43-45</td>\n<td class='inch'>46-48</td>\n<td class='inch'>50-53</td>\n<td class='inch'>54-56</td>\n</tr>\n<tr>\n<th>\nWaist\n<span>(inches)</span>\n</th>\n<td class='inch'>23-24</td>\n<td class='inch'>25-26</td>\n<td class='inch'>27-28</td>\n<td class='inch'>29-31</td>\n<td class='inch'>32-34</td>\n<td class='inch'>35-37</td>\n<td class='inch'>38-40</td>\n<td class='inch'>42-45</td>\n<td class='inch'>47-49</td>\n</tr>\n<tr>\n<th>\nHips\n<span>(inches)</span>\n</th>\n<td class='inch'>33-34</td>\n<td class='inch'>35-36</td>\n<td class='inch'>37-38</td>\n<td class='inch'>39-41</td>\n<td class='inch'>42-44</td>\n<td class='inch'>45-47</td>\n<td class='inch'>48-50</td>\n<td class='inch'>52-55</td>\n<td class='inch'>57-60</td>\n</tr>\n<tr class='empty'>\n<td colspan='10'></td>\n</tr>\n<tr>\n<th class='cm'>US</th>\n<td>00-0</td>\n<td>2-4</td>\n<td>6-8</td>\n<td>10-12</td>\n<td>14</td>\n<td>16-18</td>\n<td>20-22</td>\n<td>24-26</td>\n<td>28-30</td>\n</tr>\n<tr>\n<th class='cm'>Juniors</th>\n<td>0-1</td>\n<td>3-5</td>\n<td>7-9</td>\n<td>11</td>\n<td>13</td>\n<td>-</td>\n<td>-</td>\n<td>-</td>\n<td>-</td>\n</tr>\n<tr>\n<th class='cm'>Jeans</th>\n<td>24-25</td>\n<td>26-27</td>\n<td>28-29</td>\n<td>30-31</td>\n<td>32-33</td>\n<td>34</td>\n<td>-</td>\n<td>-</td>\n<td>-</td>\n</tr>\n<tr>\n<th class='cm'>UK</th>\n<td>4</td>\n<td>6-8</td>\n<td>10-12</td>\n<td>14-16</td>\n<td>18</td>\n<td>20</td>\n<td>22</td>\n<td>24</td>\n<td>26</td>\n</tr>\n<tr>\n<th class='cm'>European (EU)</th>\n<td>32-34</td>\n<td>36-38</td>\n<td>40-42</td>\n<td>44-46</td>\n<td>48</td>\n<td>50</td>\n<td>52</td>\n<td>54</td>\n<td>56</td>\n</tr>"
		elif breadcrumb_str.startswith("ModCloth>Shoes"):
			size_table_html = "<tr>\n<th>US</th>\n<td>5</td>\n<td>5.5</td>\n<td>6</td>\n<td>6.5</td>\n<td>7</td>\n<td>7.5</td>\n<td>8</td>\n<td>8.5</td>\n<td>9</td>\n<td>9.5</td>\n<td>10</td>\n<td>10.5</td>\n<td>11</td>\n</tr>\n<tr>\n<th>UK</th>\n<td>3</td>\n<td>3.5</td>\n<td>4</td>\n<td>4.5</td>\n<td>5</td>\n<td>5.5</td>\n<td>6</td>\n<td>6.5</td>\n<td>7</td>\n<td>7.5</td>\n<td>8</td>\n<td>8.5</td>\n<td>9</td>\n</tr>\n<tr>\n<th>European (EU)</th>\n<td>35</td>\n<td>35.5</td>\n<td>36</td>\n<td>37</td>\n<td>37.5</td>\n<td>38</td>\n<td>38.5</td>\n<td>39</td>\n<td>40</td>\n<td>40.5</td>\n<td>41</td>\n<td>42</td>\n<td>42.5</td>\n</tr>\n<tr class='empty'>\n<td colspan='14'></td>\n</tr>\n<tr>\n<th>\nCentimeters\n</th>\n<td class='cm'>21.6</td>\n<td class='cm'>22.2</td>\n<td class='cm'>22.5</td>\n<td class='cm'>23</td>\n<td class='cm'>23.5</td>\n<td class='cm'>23.8</td>\n<td class='cm'>24.1</td>\n<td class='cm'>24.6</td>\n<td class='cm'>25.1</td>\n<td class='cm'>25.4</td>\n<td class='cm'>25.9</td>\n<td class='cm'>26.2</td>\n<td class='cm'>26.7</td>\n</tr><tr><th>Inches</th><td class='inch'>8.5</td>\n<td class='inch'>8.8</td>\n<td class='inch'>8.9</td>\n<td class='inch'>9.1</td>\n<td class='inch'>9.3</td>\n<td class='inch'>9.4</td>\n<td class='inch'>9.5</td>\n<td class='inch'>9.7</td>\n<td class='inch'>9.9</td>\n<td class='inch'>10</td>\n<td class='inch'>10.2</td>\n<td class='inch'>10.3</td>\n<td class='inch'>10.5</td>\n</tr>"

		item = ModclothItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "modcloth.com"
		item['desc'] = "".join(desc) if desc else "" 
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"

		if brand:
			item['brand'] = brand[0].strip()
		else:
			item['brand'] = "ModCloth"

		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb_str)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = size_table_html
		
		if out_of_stock:
			item['is_soldout'] = "true"
		else:
			item['is_soldout'] = "false"

		item['measurements_table_html'] = "".join(measurements_table_html) if measurements_table_html else "" 
		item['keywords'] = ""
		item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

	def get_category(self, breadcrumb_str):

		if breadcrumb_str.startswith("ModCloth>Clothing"):
			return self.get_clothing_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Shoes"):
			return self.get_shoes_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories"):
			return self.get_bags_accessories_category(breadcrumb_str)
		else:
			return "women"

	def get_bags_accessories_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Bags & Accessories>Totes & Backpacks"):
			return "women_bags_totes"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Small Bags & Clutches"):
			return "women_bags_clutches"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Wallets & Accessories"):
			return "women_bags_wallets"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Jewelry"):
			return self.get_jewelry_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Sunglasses"):
			return "women_accessories_eyewear"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Hats"):
			return "women_accessories_hats"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Hair Accessories"):
			return "women_accessories_hair"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Tech Accessories"):
			return "women_accessories_tech"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Scarves"):
			return "women_accessories_scarves"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Gloves"):
			return "women_accessories_gloves"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Belts"):
			return "women_accessories_belts"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Umbrellas"):
			return "women_accessories_umbrellas"
		else:
			return "women_bags"

	def get_jewelry_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Bags & Accessories>Jewelry>Necklaces"):
			return "women_jewelry_necklaces"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Jewelry>Earrings"):
			return "women_jewelry_earrings"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Jewelry>Bracelets & Watches"):
			return "women_jewelry_bracelets"
		elif breadcrumb_str.startswith("ModCloth>Bags & Accessories>Jewelry>Rings & Pins"):
			return "women_jewelry_rings"
		else:
			return "women_jewelry"

	def get_shoes_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Shoes>Heels"):
			return "women_pumps_heels"
		elif breadcrumb_str.startswith("ModCloth>Shoes>Wedges"):
			return "women_pumps_wedges"
		elif breadcrumb_str.startswith("ModCloth>Shoes>Boots & Booties"):
			return "women_boots_booties"
		elif breadcrumb_str.startswith("ModCloth>Shoes>Flats"):
			return "women_flats"
		elif breadcrumb_str.startswith("ModCloth>Shoes>Sandals"):
			return "women_sandals"
		else:
			return "women_shoes"

	def get_clothing_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Tops"):
			return self.get_tops_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Bottoms"):
			return self.get_bottoms_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Outerwear"):
			return self.get_outerwear_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Swimwear"):
			return self.get_swimwear_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tights & Socks"):
			return self.get_tights_socks_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Intimates"):
			return self.get_intimates_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Dresses"):
			return "women_dresses"
		else:
			return "women_clothing"

	def get_intimates_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Intimates>Undergarments"):
			return "women_lingerie"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Intimates>Boudoir"):
			return "women_lingerie"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Intimates>Sleepwear & Lounge"):
			return "women_loungewear"
		else:
			return "women_lingerie"

	def get_tights_socks_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Tights & Socks>Tights"):
			return "women_accessories_hosiery"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tights & Socks>Socks"):
			return "women_accessories_hosiery"
		else:
			return "women_accessories_hosiery"

	def get_swimwear_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Swimwear>One Piece"):
			return "women_one_pieces"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Swimwear>Two Piece"):
			return "women_bikinis"
		else:
			return "women_swimwear"

	def get_outerwear_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Outerwear>Jackets"):
			return "women_jackets"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Outerwear>Coats"):
			return "women_coats"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Outerwear>Blazers & Vests"):
			return "women_jackets"
		return "women_clothing"


	def get_bottoms_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Bottoms>Skirts"):
			return "women_skirts"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Bottoms>Pants"):
			return "women_pants"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Bottoms>Leggings"):
			return "women_leggings"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Bottoms>Denim"):
			return "women_jeans"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Bottoms>Shorts & Rompers"):
			return "women_shorts"
		else:
			return "women_pants"

	def get_tops_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Tops>Graphic Tees"):
			return "women_graphic_tees"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tops>Blouses"):
			return "women_blouses"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tops>Tunics"):
			return "women_tunics"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tops>Sweaters"):
			return self.get_tops_sweaters_category(breadcrumb_str)
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tops>Basics"):
			return "women_tops"
		else:
			return "women_tops"

	# def get_bottoms_category(self, breadcrumb_str):


	def get_tops_sweaters_category(self, breadcrumb_str):
		if breadcrumb_str.startswith("ModCloth>Clothing>Tops>Sweaters>Cardigans"):
			return "women_cardigans"
		elif breadcrumb_str.startswith("ModCloth>Clothing>Tops>Sweaters>Pullovers"):
			return "women_sweaters_knits_pullovers"
		else:
			return "women_sweaters_knits"

	def fixLazyJsonWithComments (self, in_text):
	  """ Same as fixLazyJson but removing comments as well
	  """
	  result = []
	  tokengen = tokenize.generate_tokens(StringIO(in_text).readline)

	  sline_comment = False
	  mline_comment = False
	  last_token = ''

	  for tokid, tokval, _, _, _ in tokengen:

	    # ignore single line and multi line comments
	    if sline_comment:
	      if (tokid == token.NEWLINE) or (tokid == tokenize.NL):
	        sline_comment = False
	      continue

	    # ignore multi line comments
	    if mline_comment:
	      if (last_token == '*') and (tokval == '/'):
	        mline_comment = False
	      last_token = tokval
	      continue

	    # fix unquoted strings
	    if (tokid == token.NAME):
	      if tokval not in ['true', 'false', 'null', '-Infinity', 'Infinity', 'NaN']:
	        tokid = token.STRING
	        tokval = u'"%s"' % tokval

	    # fix single-quoted strings
	    elif (tokid == token.STRING):
	      if tokval.startswith ("'"):
	        tokval = u'"%s"' % tokval[1:-1].replace ('"', '\\"')

	    # remove invalid commas
	    elif (tokid == token.OP) and ((tokval == '}') or (tokval == ']')):
	      if (len(result) > 0) and (result[-1][1] == ','):
	        result.pop()

	    # detect single-line comments
	    elif tokval == "//":
	      sline_comment = True
	      continue

	    # detect multiline comments
	    elif (last_token == '/') and (tokval == '*'):
	      result.pop() # remove previous token
	      mline_comment = True
	      continue

	    result.append((tokid, tokval))
	    last_token = tokval

	  return tokenize.untokenize(result)

	def get_chart_size(self, response):
		print response


	
