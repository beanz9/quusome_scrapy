# Scrapy settings for modcloth project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'modcloth'

SPIDER_MODULES = ['modcloth.spiders']
NEWSPIDER_MODULE = 'modcloth.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'modcloth (+http://www.yourdomain.com)'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
# MONGODB_URI = 'mongodb://mongo_scrapy:beanz9_quusome_mongo@128.199.245.247:27017/scrapy'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True
# DOWNLOAD_DELAY = 1

DOWNLOADER_MIDDLEWARES = {
	# 'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
	# 'modcloth.middlewares.UrlModifyMiddleware': 100,
}
