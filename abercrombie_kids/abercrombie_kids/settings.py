# Scrapy settings for abercrombie_kids project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'abercrombie_kids'

SPIDER_MODULES = ['abercrombie_kids.spiders']
NEWSPIDER_MODULE = 'abercrombie_kids.spiders'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True
DOWNLOAD_DELAY = 1



DOWNLOADER_MIDDLEWARES = {
	'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
	'abercrombie_kids.middlewares.UrlModifyMiddleware': 100,
}

