from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from abercrombie_kids.items import AbercrombieKidsItem
from cStringIO import StringIO
from scrapy.exceptions import DropItem
import requests

class AbercrombieKidsSpider(CrawlSpider):
	name = "abercrombie_kids"
	allowed_domains = ["abercrombiekids.com"]
	# splash_url = "http://localhost:8050/render.html?url="
	start_urls = [
		"http://www.abercrombiekids.com/shop/wd/guys-shirts",
		"http://www.abercrombiekids.com/shop/wd/guys-hoodies-and-sweatshirts",
		"http://www.abercrombiekids.com/shop/wd/guys-sweaters",
		"http://www.abercrombiekids.com/shop/wd/guys-graphic-tees",
		"http://www.abercrombiekids.com/shop/wd/guys-tees",
		"http://www.abercrombiekids.com/shop/wd/guys-polos",
		"http://www.abercrombiekids.com/shop/wd/guys-outerwear",
		"http://www.abercrombiekids.com/shop/wd/guys-jeans",
		"http://www.abercrombiekids.com/shop/wd/guys-sweatpants",
		"http://www.abercrombiekids.com/shop/wd/guys-pants",
		"http://www.abercrombiekids.com/shop/wd/guys-shorts",
		"http://www.abercrombiekids.com/shop/wd/guys-summer-layers",
		"http://www.abercrombiekids.com/shop/wd/guys-swim",
		"http://www.abercrombiekids.com/shop/wd/guys-flip-flops",
		"http://www.abercrombiekids.com/shop/wd/guys-accessories",
		"http://www.abercrombiekids.com/shop/wd/guys-underwear",
		"http://www.abercrombiekids.com/shop/wd/girls-hoodies-and-sweatshirts",
		"http://www.abercrombiekids.com/shop/wd/girls-sweaters",
		"http://www.abercrombiekids.com/shop/wd/girls-shirts",
		"http://www.abercrombiekids.com/shop/wd/girls-fashion-tops",
		"http://www.abercrombiekids.com/shop/wd/girls-graphic-tees",
		"http://www.abercrombiekids.com/shop/wd/girls-polos",
		"http://www.abercrombiekids.com/shop/wd/girls-outerwear",
		"http://www.abercrombiekids.com/shop/wd/girls-jeans",
		"http://www.abercrombiekids.com/shop/wd/girls-sweatpants",
		"http://www.abercrombiekids.com/shop/wd/girls-leggings",
		"http://www.abercrombiekids.com/shop/wd/girls-pants",
		"http://www.abercrombiekids.com/shop/wd/girls-shorts",
		"http://www.abercrombiekids.com/shop/wd/girls-skirts",
		"http://www.abercrombiekids.com/shop/wd/girls-dresses",
		"http://www.abercrombiekids.com/shop/wd/girls-a-and-f-active",
		"http://www.abercrombiekids.com/shop/wd/girls-swim",
		"http://www.abercrombiekids.com/shop/wd/girls-flip-flops",
		"http://www.abercrombiekids.com/shop/wd/girls-accessories",


	]

	
	# allow_list = [
	# 	"shop/wd/guys-(.*)",
	# 	"shop/wd/girls-(.*)"
	# ]

	# deny_list = [
	# 	"shop/wd/(.*)/CustomerService(.*)",
	# 	".*?editItem=true",
	# 	"shop/wd/p/(.*)",

	# 	"shop/wd/guys-a-and-f-looks",
	# 	"shop/wd/guys-new-arrivals",
	# 	"shop/wd/guys-the-coolest-blues",
	# 	"shop/wd/guys-spring-break-layers",
	# 	"shop/wd/guys-casual-cool",
	# 	"shop/wd/guys-what-we-love"
	# 	"shop/wd/guys-body-care",
	# 	"shop/wd/guys-cologne",
	# 	"shop/wd/hidden-gift-cards",
	# 	"shop/wd/guys-featured-items-clearance",


	# 	"shop/wd/womens-fragrance",
	# 	"shop/wd/womens-a-and-f-looks",
	# 	"shop/wd/womens-new-arrivals",
	# 	"shop/wd/womens-a-and-f-the-making-of-a-star",
	# 	"shop/wd/womens-denim-collection",
	# 	"shop/wd/womens-soft-and-pretty",
	# 	"shop/wd/womens-effortless-90s-style",
	# 	"shop/wd/womens-the-prettiest-lace",
	# 	"shop/wd/womens-essential-spring-layers",
	# 	"shop/wd/womens-flagship-exclusives",
	# 	"shop/wd/womens-online-exclusives",
	# 	"shop/wd/womens-what-we-love",
	# 	"shop/wd/womens-featured-items-sale(.*)",
	# 	"shop/wd/hidden-gift-cards",
	# 	"shop/wd/mens-body-care",
	# 	"shop/wd/mens-cologne",
	# 	"shop/wd/mens-a-and-f-looks",
	# 	"shop/wd/mens-new-arrivals",
	# 	"shop/wd/mens-a-and-f-the-making-of-a-star",
	# 	"shop/wd/mens-the-hottest-blues",
	# 	"shop/wd/mens-vintage-texture-and-wash",
	# 	"shop/wd/mens-spring-break-layers",
	# 	"shop/wd/mens-casual-cool",
	# 	"shop/wd/mens-flagship-exclusives",
	# 	"shop/wd/mens-online-exclusives",
	# 	"shop/wd/mens-get-fierce",
	# 	"shop/wd/mens-what-we-love",
	# 	"shop/wd/mens-featured-items-sale(.*)",

	# 	"shop/wd/womens-featured-items-clearance(.*)",
	# 	"shop/wd/mens-featured-items-clearance(.*)"
	# ]
	rules = [
		Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="image-wrap"]', '//a[@class="swatch-link"]'), ), callback="parse_item", follow=True, ),
  ]

	def parse_test(self, response):
		sel = Selector(response)
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		print price
		print "!!!"

	def parse_item(self, response):
		sel = Selector(response)
		title = sel.xpath('//h1[@class="name"]/text()').extract()
		desc = sel.xpath('//h2[@class="copy"]/text()').extract()
		price = sel.xpath('//div[@class="info hidden"]/div[@class="price"]/h4[@class="offer-price"]/text()').extract()
		befor_price = sel.xpath('//div[@class="list-price redline"]/del/text()').extract()
		if not price:
			return

		options = sel.xpath('//li[@class="size select required"]/div[@class="dk_container dk_theme_dark_gray"]/div[@class="dk_options"]/ul[@class="dk_options_inner"]/li[not(@class="dk_option_current")]/a/text()').extract()
		sub_title = sel.xpath('//div[@class="swatches hidden"]/ul/li[@class="color"]/text()').extract()
		# thumbnail_images = sel.xpath('//div[@class="image-util product-view"]/ul/li/@data-image-name').extract()
		seed_image = sel.xpath('//span[@class="zoom prod-img"]/img[@class="prod-img"]/@src').extract()[0]
		breadcrumb = sel.xpath('//div[@id="breadcrumb"]/a/text()').extract()

		prod_state = 200
		model_state = 200
		prod_num = 1
		model_num = 1
		images = []

		

		while prod_state == 200:
			# r = requests.get("http:" + seed_image + )
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("1?")[0])
			url_str.write(str(prod_num))
			url_str.write("?$anfProductImageMagnify$")

			r = requests.get(url_str.getvalue())
			prod_state = r.status_code
			prod_num = prod_num + 1
			if prod_state == 200:
				images.append(url_str.getvalue())

		while model_state == 200:
			url_str = StringIO()

			url_str.write("http:")
			url_str.write(seed_image.split("prod1?")[0])
			url_str.write("model")
			url_str.write(str(model_num))
			url_str.write("?$anfProductImageModMagnify$")

			r = requests.get(url_str.getvalue())
			model_state = r.status_code
			model_num = model_num + 1
			if model_state == 200:
				images.append(url_str.getvalue())
		# http://anf.scene7.com/is/image/anf/anf_72487_03_model1?$anfProductImageModMagnify$


		item = AbercrombieKidsItem()

		item['title'] = title[0] if title else ""
		item['sub_title'] = sub_title[0] if sub_title else ""
		item['url'] = response.url 
		item['domain'] = "abercrombiekids.com"
		item['desc'] = desc[0] if desc else ""
		item['currency'] = price[0][0]
		item['price'] = price[0][1:]
		item['options'] = map(lambda option: option.strip(), options)
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "abercrombiekids"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(breadcrumb)
		item['created_by'] = 16
		item['county'] = "US"


		return item

	def get_category(self, breadcrumb):
		return "20"

		

