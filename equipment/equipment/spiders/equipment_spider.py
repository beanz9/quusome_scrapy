from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from equipment.items import EquipmentItem
from cStringIO import StringIO
from scrapy.http import Request
from equipment.link_extractor import LinkExtractor
import math
import requests	
import json



class EquipmentSpider(CrawlSpider):
	name = "equipment"
	allow_domains = ["equipmentfr.com"]
	start_urls = [
		# "http://www.equipmentfr.com/shop/sale/betsie-bright-white-peacoat",
		# "http://www.equipmentfr.com/shop/sweaters/taylor-cardigan-with-contrast-ivory-multi-after-dark-plaid"
		"http://www.equipmentfr.com/shop/shirts",
		"http://www.equipmentfr.com/shop/sweaters",
		"http://www.equipmentfr.com/shop/jackets",
		"http://www.equipmentfr.com/shop/dresses",
		"http://www.equipmentfr.com/shop/bottoms",
		"http://www.equipmentfr.com/shop/pajamas",
		"http://www.equipmentfr.com/shop/accessories-1",
		"http://www.equipmentfr.com/shop/sale"

	]

	rules = [
		Rule(LinkExtractor(), follow=False , callback="parse_item" ),
		# Rule(SgmlLinkExtractor(restrict_xpaths=('//div[@class="item-wrapper"]/a')), callback="parse_item" )
  	
  ]

	def parse_item(self, response):
		sel = Selector(response)

		title = sel.xpath('//div[@itemprop="name"]/h1/text()').extract()[0]
		color = sel.xpath('//div[@itemprop="color"]/div/text()').extract()[0]
		original_price = sel.xpath('//span[@class="regular-price"]/span/text()').extract()
		sale_price = sel.xpath('//p[@class="special-price"]/span[@class="price"]/text()').extract()
		image_arr = sel.xpath('//div[@class="product-image"]/img/@src').extract()

		desc = sel.xpath('//div[@itemprop="description"]/node()').extract()
		size_table_html = sel.xpath('//div[@class="size-guide"]/table/node()').extract()
		breadcrumb = sel.xpath('//nav[@class="breadcrumbs"]/a/text()').extract()





		price = {}
		is_sale = "false"

		if original_price:
			original_price = original_price[0].strip()[1:]
			price['original'] = original_price
			price['sale_color'] = {}
		else:
			original_price = sel.xpath('//p[@class="old-price"]/span[@class="price"]/text()').extract()[0].strip()[1:]
			sale_price = sale_price[0].strip()[1:]
			is_sale = "true"
			price['sale_color'] = {color: sale_price}
			price['original'] = original_price


		size_script = sel.xpath('//script[@type="text/javascript"]/text()').extract()


		size_script = size_script[9]
		size_script = size_script.split("Product.Config(")[1]
		size_script = size_script.split(");")[0]

		size_json = json.loads(size_script, strict=False)

		attr_key = size_json["attributes"].keys()[0]


		option_inventory = {}
		options = {}
		options["size"] = []
		options["color"] = [color]


		for option in size_json["attributes"][attr_key]["options"]:

			if "(out of stock)" in option["label"]:
				option_inventory["color*_*" + color + "-_-size*_*" + option["label"].replace('(out of stock)', '').strip().replace('.', '||')] = "unavailable"
			else:
				option_inventory["color*_*" + color + "-_-size*_*" + option["label"].replace('.', '||')] = "available"

			options["size"].append(option["label"].replace('(out of stock)', '').strip())

		images = {}
		images[color] = image_arr

		item = EquipmentItem()

		item['title'] = title
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "equipmentfr.com"
		item['desc'] = "".join(desc) if desc else "" 
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = "EQUIPMENT"
		item['breadcrumb'] = breadcrumb
		item['category'] = self.get_category(response.url)
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		# item['is_soldout'] = "false"
		item['keywords'] = ""
		# item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"



		return item

	def get_category(self, url):
		category = url.split('/')[4]
		print  category

		if "shirts" == category:
			return "women_tops"
		elif "sweaters" == category:
			return "women_sweaters_knits"
		elif "jackets" == category:
			return "women_jackets"
		elif "dresses" == category:
			return "women_dresses"
		elif "bottoms" == category:
			return "women_pants"
		elif "pajamas" == category:
			return "women_loungewear"
		elif "accessories-1" == category:
			return "women_accessories"
		
		