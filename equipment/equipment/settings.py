# Scrapy settings for equipment project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'equipment'

SPIDER_MODULES = ['equipment.spiders']
NEWSPIDER_MODULE = 'equipment.spiders'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
# MONGODB_URI = 'mongodb://mongo_scrapy:beanz9_quusome_mongo@128.199.245.247:27017/scrapy'
MONGODB_DATABASE = 'scrapy'
MONGODB_COLLECTION = 'scrapy_items'
MONGODB_ADD_TIMESTAMP = True

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'equipment (+http://www.yourdomain.com)'
