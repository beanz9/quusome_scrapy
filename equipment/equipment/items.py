# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class EquipmentItem(Item):
	title = Field()
	sub_title = Field()
	url = Field()
	domain = Field()
	desc = Field()
	currency = Field()
	price = Field()
	sale_price = Field()
	options = Field()
	images = Field()
	is_done = Field()
	brand = Field()
	brand_lowercase = Field()
	breadcrumb = Field()
	category = Field()
	created_by = Field()
	county = Field()
	inventory = Field()
	is_sale = Field()
	is_soldout = Field()
	size_table_html = Field()
	keywords = Field()
	sale_rate = Field()
	is_crawled = Field()
 	store_location = Field()