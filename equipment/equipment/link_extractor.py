from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.link import Link
import json
import requests	


class LinkExtractor(SgmlLinkExtractor):

	def _extract_links(self, response_text, response_url, response_encoding, base_url=None):

		ret = []
		if base_url is None:
			base_url = urljoin(response_url, self.base_url) if self.base_url else response_url

		obj = response_text

		# print "!!!!!!!!!!!!!!"
		# print base_url
		# print obj

		sel = Selector(text=response_text)

		script = sel.xpath('//script[not(contains(@type, "text/javascript"))]/text()').extract()[0]
		script = script.split('var pageCount = ')[1]
		script = script.split(';')[0]

		for num in range(0, int(script)):

			link = Link(''.join([base_url, '?p=', str(num + 1)]))
			print "!!!!!!!!"
			url = ''.join([base_url, '?p=', str(num + 1)])
			html = requests.get(url).text

			sel2 = Selector(text=html)
			links = sel2.xpath('//div[@class="item-wrapper"]/a/@href').extract()
			for l in links:
				ret.append(Link(l))

		return ret


	