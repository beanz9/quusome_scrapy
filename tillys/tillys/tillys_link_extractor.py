from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.link import Link
import json


class TillysLinkExtractor(SgmlLinkExtractor):
	def _extract_links(self, response_text, response_url, response_encoding, base_url=None):

		ret = []
		if base_url is None:
			base_url = urljoin(response_url, self.base_url) if self.base_url else response_url

		obj = response_text.split("this.currentSort = 'Sort';")[1]
		obj = obj.split('this.products = ')[1]
		obj = obj.split(';')[0]

		products = json.loads(obj)

		for product in products:
			link = Link(''.join(["http://www.tillys.com/tillys/variants.aspx?prod=", str(product["ProductID"]), "&ctlg=", product["CatalogName"], "&cid=", str(product["CategoryID"]), "&source=1&size=" ]))
			ret.append(link)

		return ret


	