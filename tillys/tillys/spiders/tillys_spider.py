from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from tillys.items import TillysItem
from cStringIO import StringIO
from scrapy.http import Request
from tillys.tillys_link_extractor import TillysLinkExtractor
import math


class TillysSpider(CrawlSpider):
	name = "tillys"
	allow_domains = ["tillys.com"]
	start_urls = [
		
		#men
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1179",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1007",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1182",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1008",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1366",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1013",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1012",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1164",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1014",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1292",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1016",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1390",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1419",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1024",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1282",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1025",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1026",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1027",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1040",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1050",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1207",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1255",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1039",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1048",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1038",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1037",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1208",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=010_Guys&cid=1210",

		#women
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1060",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1063",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1335",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1320",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1061",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1064",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1355",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1199",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1284",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1072",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1071",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1069",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1067",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1068",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1220",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1088",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1087",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1090",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1281",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1089",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1086",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1324",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1216",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1076",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1167",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1265",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1213",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1206",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1267",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1096",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1103",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1106",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1170",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1104",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1178",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=020_Girls&cid=1275",

		#boy
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1118",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1181",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1119",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1218",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1120",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1367",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1121",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1122",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1125",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1124",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1123",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1162",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1130",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1298",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1174",

		#girl
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1137",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1135",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1322",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1136",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1138",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1238",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1139",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1217",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1140",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1141",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1142",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1144",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1143",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1176",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1163",
		"http://www.tillys.com/tillys/Categories.aspx?ctlg=030_Kids&cid=1184",


	]


	rules = [
		Rule(TillysLinkExtractor(), callback="parse_item" ),
  ]


	def make_requests_from_url(self, url):
		request = Request(url, 
			cookies=[{'name': 'Intl',
					'value': 'Country=US&Currency=USD',
	        'domain': 'www.tillys.com',
	        'path': '/'}],)

		return request




	def parse_item(self, response):
		sel = Selector(response)

		title = sel.xpath('//span[@class="product_title"]/text()').extract()[0]


		brand = sel.xpath('//a[@id="ctl00_ctl00_ctl00_ctl00_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_allBrand"]/text()').extract()
		
		if brand:
			brand = brand[0].split("view all ")[1]
		else:
			brand_str = title.split()
			num = 0
			for b in reversed(brand_str):
				if b.isupper():
					break
				else:
					num = num + 1

			brand_arr = brand_str[:-num]
			brand = " ".join(map(lambda a: a.capitalize(), brand_arr))

		desc = sel.xpath('//span[@id="ctl00_ctl00_ctl00_ctl00_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_Description"]/text()').extract()[0]
		
		variants = sel.xpath('//script').extract()
		variants_str = ""
		data_str = ""

		for v in variants:
			if "Rollover" in v:
				variants_str = v
			if "var br_data = {};" in v:
				data_str = v

		color = []
		color_str = variants_str.split('var color = "')[1]



		color_str = color_str.split('";')[0]



		color.append(color_str)

		product_name = sel.xpath('//span[@id="ctl00_ctl00_ctl00_ctl00_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_Name"]/text()').extract()[0]
	
		original_price = data_str.split('br_data.price = "')[1]
		original_price = original_price.split('";')[0]

		sale_price = data_str.split('br_data.sale_price = "')[1]
		sale_price = sale_price.split('";')[0]

		if sale_price:
			is_sale = "true"
		else:
			is_sale = "false"

		breadcrumb = sel.xpath('//div[@id="brandBreadCrumbsContainer"]/a[@class="crumbs"]/text()').extract()

		size_options = sel.xpath('//select[@id="ctl00_ctl00_ctl00_ctl00_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_MainContentPlaceHolder_DDSize"]/option/text()').extract()
		size_options.pop(0)

		options = {}
		options['size'] = size_options
		options['color'] = color


		
		option_inventory = {}
		for size_option in size_options:
			size = size_option.replace('.', '||')
			option_inventory["color*_*" + color_str + "-_-size*_*" + size] = "available"


		price = {}
		price['original'] = original_price

		if sale_price:
			price['sale_color'] = {color_str: sale_price}
		else:
			price['sale_color'] = {}

		sale_rate = 0

		if price["sale_color"].values():
			min_sale_price =  min(price["sale_color"].values())
			sale_rate = math.ceil((float(price["original"]) - float(min_sale_price))/float(price["original"])*100)



		images = {}
		
		image_temp_arr = []

		temp_script = variants_str.split('Rollover = new Array(')[1]

		

		temp_script = temp_script.strip().split(');')[0]

		prd_name = variants_str.split('var LargeImg = "')[1]
		prd_name = prd_name.split('.jpg";')[0]

		temp_images = temp_script.replace('"', '').split(',')

		for temp_image in temp_images:
			if prd_name in temp_image:
				image_temp_arr.append("http://www.tillys.com/tillys/images/catalog/Large/" + temp_image)
				

		images[color_str] = image_temp_arr

		category = self.get_category(breadcrumb, title)

		if not category:
			return

		item = TillysItem()
		
		item['title'] = title.strip()
		item['sub_title'] = ""
		item['url'] = response.url 
		item['domain'] = "tillys.com"
		item['desc'] = desc
		item['currency'] = "USD"
		item['price'] = price
		item['options'] = options
		item['images'] = images
		item['is_done'] = "false"
		item['brand'] = brand.strip()
		# item['brand_lowercase'] = brand_lowercase
		item['breadcrumb'] = breadcrumb
		item['category'] = category
		item['created_by'] = ""
		item['county'] = "US"
		item['inventory'] = option_inventory
		item['is_sale'] = is_sale
		# item['size_table_html'] = "".join(size_table_html) if size_table_html else ""
		item['size_table_html'] = ""
		item['is_soldout'] = ""
		item['keywords'] = ""
		item['sale_rate'] = sale_rate
		item['is_crawled'] = "true"
		item['store_location'] = "overseas"

		return item

		
	def get_category(self, breadcrumb, title):
		if "men" in breadcrumb:
			return self.get_men_category(breadcrumb, title)
		elif "women" in breadcrumb:
			return self.get_women_category(breadcrumb, title)
		elif "kids" in breadcrumb:
			return self.get_kids_category(breadcrumb, title)

	def get_kids_category(self, breadcrumb, title):
		if "Boys" in breadcrumb:
			return self.get_kids_boys_category(breadcrumb, title)
		elif "Girls" in breadcrumb:
			return self.get_kids_girls_category(breadcrumb, title)

	def get_kids_girls_category(self, breadcrumb, title):
		if "Graphic Tees & Tanks" in breadcrumb:
			return "kids_girls_tshirts"
		elif "Knit Tops & Tees" in breadcrumb:
			return "kids_girls_tshirts"
		elif "Blouses & Shirts" in breadcrumb:
			return "kids_girls_shirts"
		elif "Tanks & Camis" in breadcrumb:
			return "kids_girls_tanks"
		elif "Sweatshirts & Hoodies" in breadcrumb:
			return "kids_girls_sweatshirts"
		elif "Sweaters & Wraps" in breadcrumb:
			return "kids_girls_sweaters"
		elif "Jackets & Vests" in breadcrumb:
			if "Vest" in title:
				return "kids_girls_vests"
			elif "Jacket" in title:
				return "kids_girls_jackets"
		elif "Flannels" in breadcrumb:
			return "kids_girls_shirts"
		elif "Dresses & Rompers" in breadcrumb:
			if "Romper" in title:
				return "kids_girls_rompers"
			elif "Dress" in title:
				return "kids_girls_dresses"
		elif "Skirts" in breadcrumb:
			return "kids_girls_skirts"
		elif "Shorts" in breadcrumb:
			return "kids_girls_shorts"
		elif "Leggings & Capris" in breadcrumb:
			return "kids_girls_leggings"
		elif "Jeans & Pants" in breadcrumb:
			if "Pants" in title:
				return "kids_girls_pants"
			elif "Jeans" in title:
				return "kids_girls_jeans"
			elif "Jeggings" in title:
				return "kids_girls_jeans"
		elif "Swimwear" in breadcrumb:
			return "kids_girls_swimwear"
		elif "Sneakers" in breadcrumb:
			return "kids_girls_shoes_sneakers"
		elif "Sandals" in breadcrumb:
			return "kids_girls_shoes_sandals"
		elif "Casuals" in breadcrumb:
			return "kids_girls_shoes_casuals"
		elif "Boots" in breadcrumb:
			return "kids_girls_shoes_boots"
		elif "Slippers" in breadcrumb:
			return "kids_girls_shoes_slippers"
		elif "Bags & Backpacks" in breadcrumb:
			return "kids_girls_bags"

	def get_kids_boys_category(self, breadcrumb, title):
		if "Tees" in breadcrumb:
			return "kids_boys_tshirts"
		elif "Tanks" in breadcrumb:
			return "kids_boys_tanks"
		elif "Thermals & L/S Tees" in breadcrumb:
			return "kids_boys_tshirts"
		elif "Shirts & Polos" in breadcrumb:
			return "kids_boys_shirts"
		elif "Flannels" in breadcrumb:
			return "kids_boys_shirts"
		elif "Lightweight Hoodies" in breadcrumb:
			return "kids_boys_tshirts"
		elif "Sweatshirts" in breadcrumb:
			return "kids_boys_tshirts"
		elif "Jackets" in breadcrumb:
			return "kids_boys_jackets"
		elif "Jeans" in breadcrumb:
			return "kids_boys_jeans"
		elif "Pants" in breadcrumb:
			return "kids_boys_pants"
		elif "Shorts" in breadcrumb:
			return "kids_boys_shorts"
		elif "Sneakers" in breadcrumb:
			return "kids_boys_shoes_sneakers"
		elif "Sandals" in breadcrumb:
			return "kids_boys_shoes_sandals"
		elif "Boat Shoes" in breadcrumb:
			return "kids_boys_shoes_boat_shoes"
		elif "Boots" in breadcrumb:
			return "kids_boys_shoes_boots"
		elif "Hats" in breadcrumb:
			return "kids_boys_accessories_hats"
		elif "Beanies" in breadcrumb:
			return "kids_boys_accessories_hats"
		elif "Belts" in breadcrumb:
			return "kids_boys_accessories_belt"


	def get_men_category(self, breadcrumb, title):
		if "Clothing" in breadcrumb:
			return self.get_men_clothing_category(breadcrumb, title)
		elif "Shoes" in breadcrumb:
			return self.get_men_shoes_category(breadcrumb, title)
		elif "Accessories" in breadcrumb:
			return self.get_men_accessories_category(breadcrumb, title)

	def get_women_category(self, breadcrumb, title):
		if "Clothing" in breadcrumb:
			return self.get_women_clothing_category(breadcrumb, title)
		elif "Swim" in breadcrumb:
			return self.get_women_swim_category(breadcrumb, title)
		elif "Shoes" in breadcrumb:
			return self.get_women_shoes_category(breadcrumb, title)
		elif "Accessories" in breadcrumb:
			return self.get_women_accessories_category(breadcrumb, title)


	def get_women_swim_category(self, breadcrumb, title):
		if "Coverups" in breadcrumb:
			return "women_cover_ups"
		elif "Swimsuits" in breadcrumb:
			if "Bikini" in title:
				return "women_bikinis"
			elif "Swimsuit" in title:
				return "women_one_pieces"



	def get_women_accessories_category(self, breadcrumb, title):
		if "Backpacks & Bags" in breadcrumb:
			if "Backpack" in title:
				return "women_bags_backpacks"
			elif "Tote Bag" in title:
				return "women_bags_totes"
		elif "Handbags & Wallets" in breadcrumb:
			if "Wallet" in title:
				return "women_bags_wallets"
		elif "Hats & Beanies" in breadcrumb:
			return "women_accessories_hats"
		elif "Scarves & Gloves" in breadcrumb:
			if "Scarf" in title:
				return "women_accessories_scarves"
			elif "Gloves" in title:
				return "women_accessories_gloves"
		elif "Belts" in breadcrumb:
			return "women_accessories_belts"
		elif "Sunglasses" in breadcrumb:
			return "women_accessories_eyewear"
		elif "Watches" in breadcrumb:
			return "women_accessories_watches"
		elif "Goggles" in breadcrumb:
			return "women_accessories_eyewear"
		elif "Phone Cases" in breadcrumb:
			return "women_accessories_tech"
		elif "Lingerie" in breadcrumb:
			if "Bandeaus & Bralettes" in breadcrumb:
				return "women_lingerie_bras"
			elif "Bras" in breadcrumb:
				return "women_lingerie_bras"
			elif "Panties" in breadcrumb:
				return "women_lingerie_panties"
		elif "Necklaces" in breadcrumb:
			return "women_jewelry_necklaces"
		elif "Bracelets" in breadcrumb:
			return "women_jewelry_bracelets"
		elif "Earrings" in breadcrumb:
			return "women_jewelry_earrings"
		elif "Rings" in breadcrumb:
			return "women_jewelry_rings"
		elif "Anklets" in breadcrumb:
			return "women_jewelry_anklets"
	




	def get_women_shoes_category(self,breadcrumb, title):
		if "Sandals" in breadcrumb:
			return "women_sandals"
		elif "Heels & Wedges" in breadcrumb:
			return "women_pumps"
		elif "Boots" in breadcrumb:
			return "women_boots_booties"
		elif "Casuals" in breadcrumb:
			return "women_shoes_casuals"
		elif "Flats" in breadcrumb:
			return "women_flats"
		elif "Sneakers" in breadcrumb:
			return "women_sneakers"
		elif "Sneaker Wedges" in breadcrumb:
			return "women_sneakers"
		elif "Slippers" in breadcrumb:
			return "women_shoes_slippers"

	def get_women_clothing_category(self, breadcrumb, title):
		if "Tanks & Camis" in breadcrumb:
			return "women_tank_tops"
		elif "Crop Tops" in breadcrumb:
			return "women_crop_tops"
		elif "Tunics" in breadcrumb:
			return "women_tunics"
		elif "Bandeaus & Bralettes" in breadcrumb:
			return "women_bandeaus"
		elif "Graphic Tees & Tanks" in breadcrumb:
			return "women_graphic_tees"
		elif "Sweatshirts & Hoodies" in breadcrumb:
			return "women_sweatshirts_hoodies"
		elif "Cardigans & Wraps" in breadcrumb:
			return "women_cardigans"
		elif "Pullovers" in breadcrumb:
			return "women_sweaters_knits_pullovers"
		elif "Jackets & Vests" in breadcrumb:
			if "Jacket" in title:
				return "women_jackets"
			elif "Vest" in title:
				return "women_vests"
		elif "Jeggings" in breadcrumb:
			return "women_jeans_jeggings"
		elif "Extreme Skinny" in breadcrumb:
			return "women_skinny_jeans"
		elif "Skinny" in breadcrumb:
			return "women_skinny_jeans"
		elif "Bootcut" in breadcrumb:
			return "women_bootcut_jeans"
		elif "Flare" in breadcrumb:
			return "women_flare_wide_jeans"
		elif "Highwaisted" in breadcrumb:
			return "women_high_waisted_jeans"
		elif "Pants" in breadcrumb:
			return "women_pants"
		elif "Leggings" in breadcrumb:
			return "women_leggings"
		elif "Shorts" in breadcrumb:
			return "women_shorts"
		elif "Short Skirts" in breadcrumb:
			return "women_mini_skirts"
		elif "Maxi Skirts" in breadcrumb:
			return "women_maxi_skirts"
		elif "Short Dresses" in breadcrumb:
			return "women_dresses_mini"
		elif "Maxi Dresses" in breadcrumb:
			return "women_dresses_maxi"
		elif "Rompers, Jumpers & Overalls" in breadcrumb:
			return "women_jumpsuit"

	def get_men_clothing_category(self, breadcrumb, title):
		if "T-Shirts" in breadcrumb:
			return "men_clothing_tshirts"
		elif "Tank" in breadcrumb:
			return "men_clothing_tanks"
		elif "Shirts" in breadcrumb:
			return "men_clothing_shirts"
		elif "Thermals" in breadcrumb:
			return "men_clothing_tshirts"
		elif "Lightweight Hoodies" in breadcrumb:
			return "men_clothing_tshirts"
		elif "Sweatshirts" in breadcrumb:
			return "men_clothing_sweatshirts"
		elif "Sweaters" in breadcrumb:
			return "men_clothing_sweaters"
		elif "Vests" in breadcrumb:
			return "men_clothing_vests"
		elif "Jackets" in breadcrumb:
			return "men_clothing_jackets"
		elif "Jeans" in breadcrumb:
			return "men_clothing_jeans"
		elif "Pants" in breadcrumb:
			return "men_clothing_pants"
		elif "Joggers & Sweatpants" in breadcrumb:
			return "men_clothing_pants"
		elif "Shorts" in breadcrumb:
			return "men_clothing_shorts"
		elif "Boardshorts" in breadcrumb:
			return "men_clothing_shorts"

	def get_men_shoes_category(self, breadcrumb, title):
		if "Sneakers" in breadcrumb:
			return "men_shoes_sneakers"
		elif "Casuals" in breadcrumb:
			return "men_shoes_casuals"
		elif "Sandals" in breadcrumb:
			return "men_shoes_sandals"
		elif "Boots" in breadcrumb:
			return "men_shoes_boots"
		elif "Slippers" in breadcrumb:
			return "men_shoes_slippers"

	def get_men_accessories_category(self, breadcrumb, title):
		if "Watches" in breadcrumb:
			return "men_accessories_watches"
		elif "Beanies" in breadcrumb:
			return "men_accessories_hats"
		elif "Hats" in breadcrumb:
			return "men_accessories_hats"
		elif "Backpacks & Bags" in breadcrumb:
			if "Backpack" in title:
				return "men_bags_backpacks"
			else:
				return "men_bags"
		elif "Wallets" in breadcrumb:
			return "men_accessories_wallets"
		elif "Phone Cases" in breadcrumb:
			return "men_accessories_tech"
		elif "Goggles" in breadcrumb:
			return "men_accessories_eyewears"
		elif "Sunglasses" in breadcrumb:
			return "men_accessories_eyewears"
		elif "Belts & Buckles" in breadcrumb:
			return "men_accessories_belts"
		elif "Bracelets" in breadcrumb:
			return "men_jewelry_bracelets"
		elif "Necklaces" in breadcrumb:
			return "men_jewelry_necklaces"
		elif "Rings" in breadcrumb:
			return "men_jewelry_rings"


		
