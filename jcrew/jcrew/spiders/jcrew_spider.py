from scrapy.spider import Spider
from scrapy.selector import Selector
from jcrew.items import JcrewItem
from scrapy.http import Request
# from selenium import selenium

# import time
class JcrewSpider(Spider):
	name = "jcrew"
	allowed_domains = ["jcrew.com"]
	# start_urls = [
	# 	"https://www.jcrew.com/womens_category/sweaters/Pullover/PRDOVR~A1917/A1917.jsp"
	# ]

	# start_urls = [
	# 	"http://localhost:8050/render.html?url=https://www.jcrew.com/mens_feature/NewArrivals/shirts/PRDOVR~A2847/A2847.jsp"
	# ]

	# request_with_cookies = Request(url="http://localhost:8050/render.html?url=https://www.jcrew.com/mens_feature/NewArrivals/shirts/PRDOVR~A2847/A2847.jsp", 
	# 		cookies={'jcrew_country': 'US'})

	# def start_requests(self):
	# 	yield Request(url="http://localhost:8050/render.html?url=https://www.jcrew.com/mens_feature/NewArrivals/shirts/PRDOVR~A2847/A2847.jsp", 
	# 		cookies=[{'name': 'jcrew_country',
	# 			'value': 'US',
 #        'domain': 'www.jcrew.com',
 #        'path': '/'}])

	# def __init__(self):
	# 	Spider.__init__(self)
	# 	# self.verificationErrors = []
	# 	self.selenium = selenium("localhost", 4444, "*chrome", "https://www.jcrew.com")
	# 	self.selenium.start()

	# def __del__(self):
	# 	self.selenium.stop()
 #    # print self.verificationErrors
 #    # Spider.__del__(self)

	def start_requests(self):
			yield Request(url="https://www.jcrew.com/womens_category/sweaters/Pullover/PRDOVR~A1917/A1917.jsp", 
				cookies=[{'name': 'jcrew_country',
					'value': 'US',
	        'domain': 'www.jcrew.com',
	        'path': '/'}],
	   		# headers={'Accept-Language': 'en_US'}
	      # headers={'X-Splash-renders': 'html'},
	      # meta={'proxy': 'http://localhost:8051?proxy=proxy'}
	      )

	def parse(self, response):
		sel = Selector(response)
		title = sel.xpath('//title/text()').extract()
		desc = sel.xpath('//div[@id="prodDtlBody"]/text()').extract()
		desc += sel.xpath('//div[@id="prodDtlBody"]/ul').extract()
		# desc = response.meta['content']
		price = sel.xpath('//div[@class="full-price"]/span/text()').extract()
		top_nav_country = sel.xpath('//div[@id="top_nav_country"]/text()').extract()

		item = JcrewItem()
		item['title'] = title
		item['desc'] = desc
		item['price'] = price
		item['top_nav_country'] = top_nav_country

		# sel = self.selenium
		# sel.open(response.url)
		# time.sleep(2.5)
		# print sel.get_text('//div[@class="full-price"]/span/text()')
		# print sel.find_element_by_xpath('//div[@class="full-price"]/span/text()')

		return item
	
