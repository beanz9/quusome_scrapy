# Scrapy settings for jcrew project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'jcrew'

SPIDER_MODULES = ['jcrew.spiders']
NEWSPIDER_MODULE = 'jcrew.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'jcrew (+http://www.yourdomain.com)'
COOKIES_ENABLED = True
COOKIES_DEBUG = True
AJAXCRAWL_ENABLED = True

# DOWNLOADER_MIDDLEWARES = {
#     'scrapy.contrib.downloadermiddleware.useragent.UserAgentMiddleware': 400,
#     'scrapy.contrib.downloadermiddleware.cookies.CookiesMiddleware':700
# }

DOWNLOADER_MIDDLEWARES = {
	# 'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 1,
	'jcrew.middlewares.JcrewMiddleware': 740,
	# 'jcrew.scrapyjs.WebkitDownloader': 1
} 

# DEFAULT_REQUEST_HEADERS = {
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#     'Accept-Language': 'en-US',
# }

# ITEM_PIPELINES = [
#     'jcrew.pipelines.JcrewPipeline',
# ]